# Inhabit
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

## Description
Based on data from the Socio-Economic Panel (SOEP), INHABIT can be used to model dwelling occupancy, by matching the German population, disaggregated by age, income, household size to the existing dwelling stock, disaggregated by room numbers, single/multi-family houses, renovation status and tenure status. We aim for a differentiation between regions that are growing/shrinking in population and by degree of urbanization. Modelling iterations are yearly and include the steps of a population fraction moving out, generating a vector of dwelling-seeking household types and a vector of freed-up dwelling types, a matching of supply and demand according to an allocation algorithm calibrated on empirical data and resulting new household-dwelling matrix for the next model year. This model substantiates future scenarios of living space development and distribution between households, yielding insights to potentials for reducing heated floor areas.

## Link to documentation
You can find more detailed descriptions and a manual to functions in our [ReadTheDocs](https://inhabit.readthedocs.io/en/latest/). The most relevant and general information can be found in this README.

## Installation
1. Install python on your computer (e.g. with [Miniconda&copy;](https://docs.conda.io/projects/miniconda/en/latest/))
2. Create a virtual python environment, e.g. by typing ``conda create -n inhabit`` into your console and activate the environment with ``conda activate inhabit``
3. Install [git](https://git-scm.com/download/) on your computer
4. Clone this gitlab repository into your local working directory (e.g. ``mod/inhabit``) with ``git clone https://gitlab.com/energy-sufficiency-model/inhabit``
5. Install the packages listed in ``requirements.txt`` e.g. with ``conda install --file requirements.txt``

## Model outline
![Schematic outline of model](docs/inhabit_model_outline.png)  
A description of the model outline will follow.

## Data
To use this model you need the panel data of the German Socio-Economic Panel (SOEP) from the German Institute for Economic Research (DIW Berlin).
Currently used version: "SOEP-Core v39 including area types and planning regions (geocodes)" in ASCII format (CSV files).  
Get the data: Go to the [website of the Research Data Center SOEP](https://www.diw.de/en/diw_01.c.678568.en/research_data_center_soep.html) to apply for a data distribution contract before ordering the SOEP data. More information at the [website of the DIW Berlin](https://www.diw.de/soep).  
Save the CSV files directly in **input/soep/** (including subfolders folders like "raw" or "eu-silc-like-panel")

## Usage
Navigate to the main inhabit repo folder, open a console and run ``python inhabit_matrix.py`` with the following optional flags appended   

The following flags are supported:  
``no flag``: creates inhabit matrices from evidence for all years specified in ``inputs.xlsx``, as well as calculate inhabit matrices to compare them to evidence. Outputs are saved in **output/inhabit/**  
``-d`` or ``--debug``: generates additional output files for debugging  
``-ns`` or ``--new_soep``: use this flag when running ``inhabit_matrix.py`` for the very first time after adding the (new) SOEP data to **data/soep/**. This (re-) calculates intermediate CSVs and thus overwrites the old with the new data

## Inputs
You can adjust general assumptions and the folder structure in the file ``inputs.xlsx``.

## Folder structure
- main folder with ``inhabit_matrix.py``, ``inputs.xlsx``
- folder **scripts/** for all other ``*.py`` scripts  
- folder **data/** for external data input,   
    - subfolder **soep/** for all CSV files from SOEP and **soep_composita**  
- folder **output/** for generated files, figures etc.  

## How does the model work?
- creation of the inhabit matrix from evidence
- calculation of inhabit matrix 

### Creation of the inhabit matrix from evidence
When running ``inhabit_matrix.py`` several steps are perfomed to create the inhabit matrix. They are explained in the following:

1. When running for the first time, the household and dwelling specific variables from SOEP are loaded and combined into extra files that are saved in **data/soep/soep_composita**. They are solely created to reduce loading time of large SOEP-csvs. They contain all needed information to create inhabit matrices for each year. With the help of these soep-composita files, a household and a dwelling vector are created. Each line in the household vector represents one person, each line in the dwelling vector represents one household.  
2. By merging dwelling and household vectors, the yearly inhabit matrix is generated for all years given in ``inputs.xlsx``. It only contains valid SOEP-data (successful survey, valid answers). 

The **household** specific variables disaggregations are represented in the following matrix. Only the oldest person per household is kept as a representative of that household.  

| Variable | SOEP ID | Groupd disaggregation categories | Original disaggregation categories |
|----------|----------|--------------|--------------------------|
| hh_type | hgtyp1hh | with_children | household contains children |
| hh_type | hgtyp1hh | without_children | no children in household |
| hh_size | hhgr | 1, 2, 3, 4, 5+ | exact household size |
| age | gebjahr | <45, >=45 (age of oldest person in household) | exact age year per person|
| income_quintile | hghinc | q1, q2, q3, q4, q5 | household income partioned in quintiles |
| will_move | resmove | moved, not moved | moved, not moved, does not apply |
| region_type | regtyp | urban, rural | urban, rural |
| growth_type | nuts3 | growing, shrinking, neither | NUTS 3 ID |

The **dwelling** specific variable disaggregations are represented in the following matrix:  

| Variable | SOEP ID | Groupd disaggregation categories | Original disaggregation categories |
|----------|----------|------------------------|--------------------------|
| building_type | wum1, hlf0154_h, housing1 | SFH (single family house)| Farm House, 1-2 Family House, 1-2 Fam. Rowhouse |
| building_type | wum1, hlf0154_h, housing1 | MFH (multi family house)| Apt. in 3-4 Unit Bldg., Apt. in 5-8 Unit Bldg., Apt. in 9+ Unit Bldg., high rise, other building|
| ownership | hgowner | private owner | owner |
| ownership | hgowner | private tenant | main tenant, sub-tenant, tenant, living in home or shared accomodation |
| condition | hgcondit | renovated | In a good condition, Some renovations (necessary) |
| condition | hgcondit | not renovated | Full renovations (necessary), Dilapidated |
| rooms | hgroom | 1, 2, 3, 4+ | exact number of rooms | 

3. The households (rows) are grouped by all variables (with the updated disaggregation categories) and during the grouping the household weights are applied. In the resulting grouped inhabit matrix each cell contains the sum of weights of all households with the respective combination of the household and dwelling variable manifestation  
4. The generated inhabit vector and matrix is saved in **output/inhabit**.


## Support
Overall: johannes.thema@wupperinst.org
Code: georg.graser@uni-flensburg.de

## Roadmap
- Differentiate regions into growing/shrinking/neutral and urban/rural/suburban  
- Implement policies and create scenarios for modelling into the future  
- Calculate and analyze future living space development and distribution between households  

## Contributing
Please feel free to contact us under the E-mails given above.

## Authors and acknowledgment
Johannes Thema, scientific assistant at Wuppertal Institute for Climate, Evironment and Energy  
Luisa Cordroch, scientific assistant t Europa-University Flensburg  
Alexander Kling, scientific assistant at Wuppertal Institute for Climate, Evironment and Energy  
Georg Graser, scientific assistant at Europa-University Flensburg  
Frauke Wiese, head of research group, Europa-University Flensburg  
(Johannes Parschau, assistant at Wuppertal Institute for Climate, Evironment and Energy)  

## License
The code in inhabit is released as free software under the [MIT License](https://opensource.org/license/mit/), see ``LICENSE``. However, different licenses and terms of use may apply to the various input data.

## Project status
Weekly updates.
