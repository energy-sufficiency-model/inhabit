"""Combine dwellings and households function to inhabit matrix.

To run this script, you need access to the SOEP Core panel data, v38.
Apply for data access here:
https://www.diw.de/en/diw_01.c.601584.en/data_access.html
"""

import os
import concurrent.futures
import pandas as pd
import argparse
import scripts.misc as misc
import scripts.move_out_rate as move_out_rate
import scripts.allocation as allocate
import scripts.soep_loader as soep_loader
import scripts.inputs as inputs
import scripts.filters as filters


# @misc.timer_func
def household_disagg(ip, df, year):
    """Create dataframe from household data to be included in inhabit matrix.
    Args:
        - ip: dataframe, contains user-input from csv and in-program added vars
        - df: dataframe to be worked on
        - year: current year of observation
        - movers: boolean, used to handle moving households
    Returns:
        - df: household-dataframe
        - cols_used: list, cols that df uses for furhter processing
        - col_weights: string, name of weights-col from df
    """
    cols_used = ["hid"]

    # apply filters
    df = filters.filter_df(df, ip, year)

    df = df[df[ip["col_oldest"]] == 1]
    all_dimensions = []
    # before selecting movers or hh-oldest persons, let the functions run.
    for col, fun in ip["hh"].items():
        cols_used.append(col)
        # apply the functions to dataframe that are defined in ip["col_hh"]
        df, dim = fun(df.copy(), ip, col)
        # if "region_type" not in dim.keys():
        all_dimensions.append(dim)

    # add information to df, who will move this year
    cols_used.append(ip["move"]["col_will_move"])
    cols_used.append(ip["col_weights"])

    df = df[cols_used]
    df = misc.clean_nan(df)

    return df, all_dimensions


# @misc.timer_func
def dwelling_disagg(ip, df, year):
    """Create dataframe from dwelling data to be included in inhabit matrix.
    Args:
        - ip: dataframe, contains user-input from csv and in-program added vars
        - df: dataframe to be worked on
        - year: current year of observation
    Returns:
        - df: dwelling-dataframe
    """
    # apply filters
    df = filters.year_filter(df, set_year=year)
    # remove "hlf0013_h" from list to filter, nan values from this variable are handled
    # seperately in dwelling.owner_type
    clean_cols = [x for x in df.keys() if x != "hlf0013_h"]
    df = misc.clean_nan(df, on_cols=clean_cols)
    misc.check_empty(df, "dwelling")

    cols_used = ["hid"]
    all_dimensions = []
    for col, fun in ip["dwell"].items():
        cols_used.append(col)
        df, dim = fun(df.copy(), ip, col)
        all_dimensions.append(dim)

    df = df[cols_used]
    df = misc.clean_nan(df)

    return df, all_dimensions


@misc.timer_func
def inh_projection(ip, year, inhabit_weights_v, calc_mor=True):
    out_heat = misc.get_save_path(ip["output"], "heatmaps")
    ih_path = os.path.join(ip["output"], ip["alloc_output_path"])
    if not os.path.exists(ih_path):
        os.makedirs(ih_path)

    inhabit_weights_v.to_csv(os.path.join(ih_path, f"inhabit_created_{year}.csv"))

    while calc_mor and year in range(ip["model_start_year"], ip["target_year"] + 1):
        print(f"move out rate and allocation calculation for year {year}")
        # For further calculation use move vector with rates instead of values
        mor_v = move_out_rate.get_mor(ip, year)

        print(f"calculate allocation in year {year} for {year + 1}")
        inhabit_weights_v, unhappy_v = allocate.allocation(
            ip, inhabit_weights_v, pd.DataFrame(mor_v), year
        )
        if ip["debug"]:
            # create heatmaps
            if not ip["reduce_save"]:
                misc.heatmap(mor_v, year, out_heat, ip, heat_name="move_out_rate")
                misc.heatmap(
                    inhabit_weights_v, year, out_heat, ip, heat_name="inhabit_created"
                )
                misc.heatmap(unhappy_v, year, out_heat, ip, heat_name="unhappy")

            misc.debug_messages("\n", ip)
            analysis = "analysis_gen.txt"
            with open(os.path.join(ip["output"], analysis), "a") as file:
                file.write(ip["debug_message"])
            ip["debug_message"] = ""

        year += 1
        # inhabit for next year is the combination of movers and stayers from last year
        inhabit_weights_v_save = inhabit_weights_v.copy()
        if ip["reduce_save"]:
            inhabit_weights_v_save = inhabit_weights_v_save.loc[
                inhabit_weights_v_save[ip["col_weights"]] > 0
            ]
        inhabit_weights_v_save.to_csv(
            os.path.join(
                ip["output"],
                ip["alloc_output_path"],
                f"inhabit_created_{year}.csv",
            )
        )


@misc.timer_func
def yearly_inhabit(ip, df_household, df_dwelling, year, calc_mor):
    """Year-wise processing of inhabit matrix creation.
    Args:
        - ip: dataframe, contains user-input from csv and in-program added vars
        - df_household: household dataframe
        - df_dwelling: dwelling dataframe
        - year: current year of observation
    """
    misc.debug_messages(f"======== year {year} ========", ip)
    with concurrent.futures.ThreadPoolExecutor() as executor:
        hd = executor.submit(household_disagg, ip, df_household, year)
        dd = executor.submit(dwelling_disagg, ip, df_dwelling, year)

        df_hh, all_dims_hh = hd.result()
        df_dwell, all_dims_dwell = dd.result()

    inhabit_v = pd.merge(
        left=df_dwell, right=df_hh, on=["hid"], suffixes=["_dwell", "_hh"]
    )

    # ensure only correct entries
    inhabit_v = inhabit_v.loc[
        inhabit_v["region_type_hh"] == inhabit_v["region_type_dwell"]
    ]
    # rounding
    inhabit_v[ip["col_weights"]] = inhabit_v[ip["col_weights"]].round(decimals=0)

    inhabit_move_v = inhabit_v[inhabit_v[ip["move"]["col_will_move"]] == 1]
    # getting rid of will_move column
    inhabit_move_v = inhabit_move_v.drop(ip["move"]["col_will_move"], axis=1)
    inhabit_stay_v = inhabit_v[inhabit_v[ip["move"]["col_will_move"]] != 1]
    inhabit_stay_v = inhabit_stay_v.drop([ip["move"]["col_will_move"], "hid"], axis=1)

    inhabit_stay_v = inhabit_stay_v.groupby(by=ip["to_group_col"], observed=True).sum(
        numeric_only=True
    )

    inhabit_v = inhabit_v.drop(ip["move"]["col_will_move"], axis=1)

    all_dimensions = [*all_dims_dwell, *all_dims_hh]
    # call to get full dimensional inhabit matrix
    inhabit_all_v = misc.get_full_inhabit(all_dimensions, ip, inhabit_v, "hid")
    inhabit_move_all_v = misc.get_full_inhabit(
        all_dimensions, ip, inhabit_move_v, "hid"
    )
    # absolute = unweighted, drop weights, not needed for counting bare occurences
    inhabit_absolute_v = inhabit_v.drop(ip["col_weights"], axis=1)
    inhabit_absolute_v.set_index("hid", inplace=True)
    inhabit_absolute_v = inhabit_absolute_v.groupby(
        ip["to_group_col"], observed=True
    ).value_counts()

    inhabit_absolute_all_v = inhabit_all_v.drop(ip["col_weights"], axis=1)
    # inhabit_absolute_all_v.set_index("hid", inplace=True)
    inhabit_absolute_all_v = inhabit_absolute_all_v.groupby(
        ip["to_group_col"], observed=True
    ).agg(lambda x: 0)

    # add inhabit_absolute to inhabit_absolute_all_v to get the full dimensions back
    inhabit_absolute_v = inhabit_absolute_v + inhabit_absolute_all_v
    inhabit_absolute_v.rename("count", inplace=True, axis=0)
    inhabit_absolute_v.fillna(0, inplace=True)
    # inhabit_absolute_m = inhabit_absolute_v.unstack(ip["cols_dwell"])

    inhabit_weights_v = inhabit_all_v.groupby(by=ip["to_group_col"], observed=True).sum(
        numeric_only=True
    )

    # inhabit_weights_m = inhabit_weights_v.unstack(ip["cols_dwell"])

    # create output subfolders if it is not yet created
    out_inhabit = misc.get_save_path(ip["evidence_folder"], ip["inhabit_path"])
    out_movers = misc.get_save_path(ip["evidence_folder"], ip["move_out_path"])
    # out_check = misc.get_save_path(ip["output"], ip["alloc_output_path"])

    inhabit_saved = os.path.join(out_inhabit, f"{ip['inhabit_path']}_{year}")
    inhabit_weights_v.to_csv(f"{inhabit_saved}.csv", index=True)
    # inhabit_absolute_v.to_csv(f"{inhabit_saved}_abs.csv", index=True)
    # if not ip["reduce_save"]:
    #    inhabit_absolute_m.to_csv(f"{inhabit_saved}_abs_m.csv", index=True)
    #    inhabit_weights_m.to_csv(f"{inhabit_saved}_weights_m.csv", index=True)

    # teacher data to check against the created data
    # move in want and move in is the same in original inhabit
    # prepare move to be saved
    # inhabit_move_all_v = inhabit_move_all_v.drop("hid", axis=1)
    inhabit_move_all_v = inhabit_move_all_v.groupby(
        ip["to_group_col"], observed=True
    ).sum(numeric_only=True)
    # inhabit_stay_v = inhabit_stay_v.loc[inhabit_stay_v[ip["col_weights"]] > 0]
    stayers_msg = f"empirical stayers: {inhabit_stay_v[ip['col_weights']].sum()}"
    movers_msg = f"empirical movers: {inhabit_move_all_v[ip['col_weights']].sum()}"
    total_inh = f"empirical inhabits: {inhabit_weights_v[ip['col_weights']].sum()}"
    misc.debug_messages(stayers_msg, ip)
    misc.debug_messages(movers_msg, ip)
    misc.debug_messages(total_inh, ip)
    print(stayers_msg)
    print(movers_msg)
    print(total_inh)

    inhabit_weights_v_save = inhabit_weights_v.copy()
    if ip["reduce_save"]:
        inhabit_weights_v_save = inhabit_weights_v_save.loc[
            inhabit_weights_v_save[ip["col_weights"]] > 0
        ]

    inhabit_move_all_v.to_csv(os.path.join(out_movers, f"move_{year}.csv"))
    # inhabit_move_all_v = inhabit_move_all_v.loc[
    #    inhabit_move_all_v[ip["col_weights"]] > 0
    # ]
    # inhabit_move_all_v.to_csv(os.path.join(out_check, f"move_original_{year}.csv"))
    # inhabit_stay_v.to_csv(
    #    os.path.join(out_check, f"stay_original_{year}.csv"), index=True
    # )
    # inhabit_weights_v_save.to_csv(
    #    os.path.join(out_check, f"inhabit_original_{year}.csv")
    # )

    # save original inhabit as created one for easy image creation
    if year == ip["model_start_year"]:
        # call future projection
        inh_projection(ip, year, inhabit_weights_v, calc_mor)

    # path names for calc_mor and ip["debug"] cases, for saving heatmaps
    out_inh = misc.get_save_path(ip["output"], ip["alloc_output_path"])
    if ip["debug"] and not ip["reduce_save"]:
        # create heatmaps
        misc.heatmap(
            inhabit_weights_v,
            year,
            out_inh,
            ip,
            heat_name="inhabit",
        )


def create_helping_csvs(ip):
    """Load soep-csvs and attach all variables needed to dwelling household and wum csv.
    Args:
        - ip: dataframe, contains user-input from csv and in-program added vars
    """
    # create wum1_updated csv
    with concurrent.futures.ThreadPoolExecutor() as executor:
        hh_path = os.path.join(ip["soep_composita_path"], ip["household_data"])
        if (
            not os.path.exists(hh_path.format(ip["empirical_start_year"]))
            or ip["new_soep"]
        ):
            df_hh = executor.submit(soep_loader.csv_gen_households, ip)
            df_hh = df_hh.result()
            for year in list(set(list(df_hh["syear"]))):
                df_hh.loc[df_hh["syear"] == year].to_csv(
                    hh_path.format(year), index=False
                )

        dwelling_path = os.path.join(ip["soep_composita_path"], ip["dwelling_data"])
        if (
            not os.path.exists(dwelling_path.format(ip["empirical_start_year"]))
            or ip["new_soep"]
        ):
            df_dwell = executor.submit(soep_loader.csv_gen_dwellings, ip)
            df_dwell = df_dwell.result()
            for year in list(set(list(df_dwell["syear"]))):
                df_dwell.loc[df_dwell["syear"] == year].to_csv(
                    dwelling_path.format(year), index=False
                )


def calculate_inhabit(ip, create_calibrate=False):
    # save parameters to file
    misc.save_params_to_file(ip.copy())

    calc_inhabit = False
    missing_years = []
    households = {}
    dwellings = {}
    for year in range(ip["empirical_start_year"], ip["empirical_end_year"] + 1):
        with concurrent.futures.ThreadPoolExecutor() as executor:
            df_h = executor.submit(
                pd.read_csv,
                os.path.join(
                    ip["soep_composita_path"], ip["household_data"].format(year)
                ),
            )
            df_d = executor.submit(
                pd.read_csv,
                os.path.join(
                    ip["soep_composita_path"], ip["dwelling_data"].format(year)
                ),
            )
            households[year] = df_h.result()
            dwellings[year] = df_d.result()
        # see whether inhabit and move out matrizes for given range of years exist:
        # if they don't exist, create
        if (
            not os.path.exists(
                os.path.join(
                    ip["evidence_folder"],
                    "inhabit",
                    f"inhabit_{year}.csv",
                )
            )
            or create_calibrate
        ):
            # calculate missing inhabits for given year
            calc_inhabit = True
            missing_years.append(year)
    if calc_inhabit:
        for year in missing_years:
            yearly_inhabit(ip, households[year], dwellings[year], year, calc_mor=False)

    # call inhabit for the starting year of allocation
    yearly_inhabit(
        ip,
        households[ip["model_start_year"]],
        dwellings[ip["model_start_year"]],
        ip["model_start_year"],
        calc_mor=True,
    )


def main(*args, **flags):
    """Combine household and dwelling data to inhabit matrix."""
    # loading inputs to dataframe
    ip = inputs.load_inputs(**flags)
    # set year to start projection
    year = ip["model_start_year"]
    try:
        # load inhabit matrix and start projection
        inhabit_weights_v, _ = misc.load_inhabit_moving(
            "inhabit", year, ip, load_abs=False, alt_path=None, use_weights=True
        )
        # call future projection
        inh_projection(ip, year, inhabit_weights_v)
    # create inhabit matrices for projection if they cannot be loaded
    except FileNotFoundError as ex:
        print(f"file not found: {ex}. Calculate new from evidence.")

        try:
            create_helping_csvs(ip)
            calculate_inhabit(ip)
        except FileNotFoundError as ex:
            print(f"File not found {ex}. Aborting.")


if __name__ == "__main__":
    # first parse optional command line arguments
    parser = argparse.ArgumentParser(
        description="Arguments are optional for enhanced functionality."
    )
    # TODO: insert debug printouts to code
    parser.add_argument(
        "-d,",
        "--debug",
        action="store_true",
        default=False,
        help="debug better with more information.",
    )

    parser.add_argument(
        "-ns,",
        "--new_soep",
        action="store_true",
        default=False,
        help="new calculations of stored soep csvs.",
    )

    parser.add_argument(
        "-gt,",
        "--growth_type",
        action="store_true",
        default=False,
        help="activate growth_type allocation.",
    )

    parser.add_argument(
        "-rt,",
        "--region_type",
        action="store_true",
        default=False,
        help="activate region_type allocation.",
    )

    parser.add_argument(
        "-rs,",
        "--reduce_save",
        action="store_true",
        default=False,
        help="reduce number of saved csvs, saves disk space.",
    )

    args = parser.parse_args()
    flags = {
        "debug": args.debug,
        "new_soep": args.new_soep,
        "growth_type": args.growth_type,
        "reduce_save": args.reduce_save,
        "region_type": args.region_type,
    }
    main(**flags)
