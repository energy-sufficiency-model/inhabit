import pandas as pd

import inhabit_matrix as im
import scripts.inputs as inputs
import scripts.evaluation_plots as ep
import scripts.alloc_calibration as ac
import scripts.misc as misc
import shutil
import os
from matplotlib import pyplot as plt


def scenario_runner(ip, scenarios):
    """Runs through given scenarios and creates plots and data"""
    ip_changes = {
        "default": {
            "scenario_name": "default",
            "scenario_start_year": 2025,
            "scenario_preferences_variant": "current_quintile",
            "scenario_manual_mor_modification": "default",
            "scenario_split_large_dwellings": "dont_split",
            "scenario_prohibit_underoccupation": "no_prohibition",
        },
        "MoR_incr": {
            "scenario_name": "MoR_incr",
            "scenario_start_year": 2025,
            "scenario_preferences_variant": "current_quintile",
            "scenario_manual_mor_modification": "MoR_incr",
            "scenario_split_large_dwellings": "dont_split",
            "scenario_prohibit_underoccupation": "no_prohibition",
        },
        "No_alloc_uo_2": {
            "scenario_name": "No_alloc_uo_2",
            "scenario_start_year": 2025,
            "scenario_preferences_variant": "current_quintile",
            "scenario_manual_mor_modification": "default",
            "scenario_split_large_dwellings": "dont_split",
            "scenario_prohibit_underoccupation": ">2",
        },
        "Nopreferred_uo": {
            "scenario_name": "Nopreferred_uo",
            "scenario_start_year": 2025,
            "scenario_preferences_variant": "no_preferred_underoccupation",
            "scenario_manual_mor_modification": "default",
            "scenario_split_large_dwellings": "dont_split",
            "scenario_prohibit_underoccupation": "no_prohibition",
        },
        "Split_sfh_6": {
            "scenario_name": "Split_sfh_6",
            "scenario_start_year": 2025,
            "scenario_preferences_variant": "current_quintile",
            "scenario_manual_mor_modification": "default",
            "scenario_split_large_dwellings": "6_rooms_2_dwell",
            "scenario_prohibit_underoccupation": "no_prohibition",
        },
        "Red_UO": {
            "scenario_name": "Red_UO",
            "scenario_start_year": 2025,
            "scenario_preferences_variant": "no_preferred_underoccupation",
            "scenario_manual_mor_modification": "MoR_incr",
            "scenario_split_large_dwellings": "6_rooms_2_dwell",
            "scenario_prohibit_underoccupation": ">2",
        },
        "MoR+No_alloc": {
            "scenario_name": "MoR+No_alloc",
            "scenario_start_year": 2025,
            "scenario_preferences_variant": "current_quintile",
            "scenario_manual_mor_modification": "MoR_incr",
            "scenario_split_large_dwellings": "dont_split",
            "scenario_prohibit_underoccupation": ">2",
        },
        "MoR+Nopreferred": {
            "scenario_name": "MoR+Nopreferred",
            "scenario_start_year": 2025,
            "scenario_preferences_variant": "no_preferred_underoccupation",
            "scenario_manual_mor_modification": "MoR_incr",
            "scenario_split_large_dwellings": "dont_split",
            "scenario_prohibit_underoccupation": "no_prohibition",
        },
        "MoR+Split": {
            "scenario_name": "MoR+Split",
            "scenario_start_year": 2025,
            "scenario_preferences_variant": "current_quintile",
            "scenario_manual_mor_modification": "MoR_incr",
            "scenario_split_large_dwellings": "6_rooms_2_dwell",
            "scenario_prohibit_underoccupation": "no_prohibition",
        },
        "No_alloc+Nopreferred": {
            "scenario_name": "No_alloc+Nopreferred",
            "scenario_start_year": 2025,
            "scenario_preferences_variant": "no_preferred_underoccupation",
            "scenario_manual_mor_modification": "default",
            "scenario_split_large_dwellings": "dont_split",
            "scenario_prohibit_underoccupation": ">2",
        },
        "No_alloc+Split": {
            "scenario_name": "No_alloc+Split",
            "scenario_start_year": 2025,
            "scenario_preferences_variant": "current_quintile",
            "scenario_manual_mor_modification": "default",
            "scenario_split_large_dwellings": "6_rooms_2_dwell",
            "scenario_prohibit_underoccupation": ">2",
        },
        "Nopreferred+Split": {
            "scenario_name": "Nopreferred+Split",
            "scenario_start_year": 2025,
            "scenario_preferences_variant": "no_preferred_underoccupation",
            "scenario_manual_mor_modification": "default",
            "scenario_split_large_dwellings": "6_rooms_2_dwell",
            "scenario_prohibit_underoccupation": "no_prohibition",
        },
        "MoR+No_alloc+Nopreferred": {
            "scenario_name": "MoR+No_alloc+Nopreferred",
            "scenario_start_year": 2025,
            "scenario_preferences_variant": "no_preferred_underoccupation",
            "scenario_manual_mor_modification": "MoR_incr",
            "scenario_split_large_dwellings": "dont_split",
            "scenario_prohibit_underoccupation": ">2",
        },
        "MoR+No_alloc+Split": {
            "scenario_name": "MoR+No_alloc+Split",
            "scenario_start_year": 2025,
            "scenario_preferences_variant": "current_quintile",
            "scenario_manual_mor_modification": "MoR_incr",
            "scenario_split_large_dwellings": "6_rooms_2_dwell",
            "scenario_prohibit_underoccupation": ">2",
        },
        "MoR+Nopreferred+Split": {
            "scenario_name": "MoR+Nopreferred+Split",
            "scenario_start_year": 2025,
            "scenario_preferences_variant": "no_preferred_underoccupation",
            "scenario_manual_mor_modification": "MoR_incr",
            "scenario_split_large_dwellings": "6_rooms_2_dwell",
            "scenario_prohibit_underoccupation": "no_prohibition",
        },
        "No_alloc+Nopreferred+Split": {
            "scenario_name": "No_alloc+Nopreferred+Split",
            "scenario_start_year": 2025,
            "scenario_preferences_variant": "no_preferred_underoccupation",
            "scenario_manual_mor_modification": "default",
            "scenario_split_large_dwellings": "6_rooms_2_dwell",
            "scenario_prohibit_underoccupation": ">2",
        },
    }
    for k, v in ip_changes.items():
        if k in scenarios:
            print(f"Starting with {k} - scenario.")
            ip_new = ip.copy()
            # set scenario settings
            for kk, vv in v.items():
                ip_new[kk] = vv
            # call inhabit

            ip_new["output"] = misc.get_save_path(ip_new["output_folder"], ip_new["scenario_name"])
            get_inhabit(ip_new, create_calibrate=False)


def get_inhabit(ip_new, create_calibrate):
    # if needed, create helping csvs
    use_soep = True
    try:
        im.create_helping_csvs(ip_new)
    except Exception:
        print("no original soep-data available - continue with precalculated inhabits")
        use_soep = False
    # calculate inhabit and images and rmse for new ip
    if use_soep:
        im.calculate_inhabit(ip_new, create_calibrate)
    else:
        ms_year = ip_new["model_start_year"]
        try:
            inh_w, _ = misc.load_inhabit_moving("inhabit", ms_year, ip_new)
        except FileNotFoundError as fnfe:
            print(f"Fiel not found: {fnfe}. No SOEP-data available?")
            print("Try not to change max_rooms, max_hh_size or age. Aborting.")
            exit(0)
        im.inh_projection(ip_new, ms_year, inh_w)


def main(delete_files=True, scen_run=False, *args, **flags):
    # loading inputs to dataframe
    ip = inputs.load_inputs(**flags)

    flags = {
        "debug": False,
        "new_soep": False,
        "growth_type": False,
        "reduce_save": False,
        "region_type": False,
    }
    ip = {**ip, **flags}

    if scen_run:
        scenarios = ["MoR+No_alloc",
        "MoR+Nopreferred",
        "MoR+Split",
        "No_alloc+Nopreferred",
        "No_alloc+Split",
        "Nopreferred+Split",
        "MoR+No_alloc+Nopreferred",
        "MoR+No_alloc+Split",
        "MoR+Nopreferred+Split",
        "No_alloc+Nopreferred+Split"
        ]
        # call all scenarios one after the other
        scenario_runner(ip, scenarios)
        print("done with the last sceanrio, exiting.")
        exit(1)

    # update inputs according to needs
    ip_changes = {
        "max_rooms": [4, 5, 6, 7],
        "max_hh_size": [4, 5, 6],

        "preferences_variant": ["current_quintile", "q4_aspiration", "quintile_above"],
        "mor_disagg_weight": ["summed_diff_score", "equal", "best"],
        "min_mor_SFH": [0.01, 0.02, 0.025, 0.03, 0.04, 0.06],
        "min_mor_MFH": [0.02, 0.03, 0.04, 0.06, 0.08],

        "alloc_rate_iteration": [
            "50, 50",
            "20, 80",
            "80, 20",
            "20, 20, 20, 20, 20",
           "100, 0",
        ],

        "alloc_dwell_prio": [

            # ["con_oth", "bui_oth", "rom_lar", "own_oth"],
            # ["bui_oth", "con_oth", "rom_lar", "own_oth"],
            # ["bui_oth", "con_oth", "rom_low", "own_oth"],
            ["own_oth", "bui_oth", "rom_low", "con_oth"],
            # ["own_oth", "bui_oth", "rom_lar", "con_oth"],
            # ["rom_lar", "con_oth", "own_oth", "bui_oth"],
            ["rom_low", "con_oth", "own_oth", "bui_oth"],
            ["rom_low", "con_oth", "bui_oth", "own_oth"],
            ["rom_low", "own_oth", "con_oth", "bui_oth"],
            ["rom_low", "own_oth", "bui_oth", "con_oth"],
            ["rom_low", "bui_oth", "con_oth", "own_oth"],
            ["rom_low", "bui_oth", "own_oth", "con_oth"],

            ["con_oth", "bui_oth", "rom_low", "own_oth"],
            ["con_oth", "bui_oth", "own_oth", "rom_low"],
            ["con_oth", "rom_low", "own_oth", "bui_oth"],
            ["con_oth", "rom_low", "bui_oth", "own_oth"],
            ["con_oth", "own_oth", "bui_oth", "rom_low"],
            ["con_oth", "own_oth", "rom_low", "bui_oth"],
            # ["con_oth", "own_oth", "rom_lar", "bui_oth"],

            # ["con_oth", "own_oth", "bui_oth", "rom_lar"],
            # ["con_oth", "rom_lar", "own_oth", "bui_oth"],
        ],

        "alloc_hh_prio": [
            ["inq_lar", "typ_yes", "siz_sma", "age_old"],
            ["inq_lar", "typ_yes", "siz_sma", "age_you"],
            ["inq_lar", "typ_yes", "siz_lar", "age_old"],
            ["inq_lar", "typ_yes", "siz_lar", "age_you"],
            ["inq_lar", "typ_no", "siz_sma", "age_old"],
            ["inq_lar", "typ_no", "siz_sma", "age_you"],
            ["inq_lar", "typ_no", "siz_lar", "age_old"],
            ["inq_lar", "typ_no", "siz_lar", "age_you"],
            ["inq_lar", "age_old", "siz_lar", "typ_no"],

            ["inq_low", "typ_no", "siz_lar", "age_you"],
            ["typ_yes", "inq_lar", "siz_sma", "age_old"],
            ["typ_no", "inq_low", "siz_lar", "age_you"],
            ["siz_sma", "typ_yes", "inq_lar", "age_old"],
            ["siz_lar", "typ_no", "inq_low", "age_you"],

            ["age_old", "typ_yes", "siz_sma", "inq_lar"],
            ["age_old", "typ_no", "siz_sma", "inq_lar"],
            ["age_old", "inq_lar", "siz_lar", "typ_no"],
            ["age_old", "inq_lar", "siz_sma", "typ_no"],

            ["age_old", "inq_lar", "siz_lar", "typ_yes"],
            ["age_old", "inq_lar", "siz_sma", "typ_yes"],

            ["age_you", "typ_no", "siz_lar", "inq_low"],
        ],

        "model_start_year": [2011, 2015],
        "target_year": ["last_available_data", 2030, 2050],
    }

    alloc_hh_dwell = {
        # alloc vary order hh
        "inq_lar": ["income_quintile", "largest_first"],
        "inq_low": ["income_quintile", "lowest_first"],
        "typ_yes": ["hh_type", "with_children"],
        "typ_no": ["hh_type", "without_children"],
        "siz_sma": ["hh_size", "smallest_first"],
        "siz_lar": ["hh_size", "largest_first"],
        "age_old": ["age", "oldest_first"],
        "age_you": ["age", "youngest_first"],
        # alloc vary order dwell
        "con_oth": ["condition", "other"],
        "rom_lar": ["rooms", "oscillate_larger"],
        "rom_low": ["rooms", "oscillate_lower"],
        "own_oth": ["ownership", "other"],
        "bui_oth": ["building_type", "other"],
    }

    # iterate through all different combinations. Let only images and rmse files there
    # delete the files that can not be reused
    for k, v in ip_changes.items():
        # copy ip to always start from default. Shallow copy suffices
        for value in v:
            ip_new = ip.copy()
            changed_params = []
            # check for alloc order hh or dwell
            if isinstance(value, list):
                for idx, val in enumerate(value):
                    ip_new[f"{k}_{idx+1}_feature"] = alloc_hh_dwell[val][0]
                    ip_new[f"{k}_{idx+1}_feature_order"] = alloc_hh_dwell[val][1]
            elif value == "last_available_data":
                ip_new["target_year"] = ip_new["empirical_end_year"]
            else:
                ip_new[k] = value

            changed_params.append({k: value})

            misc.debug_messages(f"changed_parameters: {changed_params}", ip_new)
            # define new output folder which is named after the changes
            ip_new["output"] = misc.get_save_path(ip_new["output"], f"{k}_{value}")

            new_alloc = os.path.join(ip_new["output"], ip_new["alloc_output_path"])

            # can't use files with wrong age. Must be computed completely new
            create_calibrate = False
            if k in ["age_limiter", "max_rooms", "max_hh_size"]:
                create_calibrate = True
                ip_new["evidence_folder"] = os.path.join(
                    ip_new["output"], "evidence_age_rooms"
                )
            get_inhabit(ip_new, create_calibrate)
            print("Creating images.")
            ep.check_allocation(ip_new)

            print("Ensure default run available for aggregation plots to test against.")
            new_default = False
            def_output_fol = misc.get_save_path(
                os.path.join("output", "default"), "alloc_out"
            )
            files = [f for f in os.listdir(def_output_fol) if f.endswith(".csv")]
            check_for = ["inhabit_created", "move_in_created", "move_in_want_created"]
            for check in check_for:
                fnames = [
                    f"{check}_{x}.csv"
                    for x in range(ip_new["model_start_year"], ip_new["target_year"])
                ]
                for fname in fnames:
                    if fname not in files:
                        new_default = True
                        break

            if new_default:
                print("create default inhabit for aggregation plots.")
                ip_def = ip.copy()
                ip_def["scenario_name"] = "default"
                ip_def["scenario_start_year"] = 2025
                ip_def["scenario_preferences_variant"] = "current_quintile"
                ip_def["scenario_manual_mor_modification"] = "default"
                ip_def["scenario_split_large_dwellings"] = "dont_split"
                ip_def["scenario_prohibit_underoccupation"] = "no_prohibition"
                get_inhabit(ip_def, create_calibrate)
            ep.aggregation_plots(ip_new, "alloc_out")
            ep.check_allocation(ip_new)
            ep.aggregation_plots(ip_new, "alloc_out")

            print("Creating rmse.")
            ac.main(ip_new)
            if delete_files:
                print("delete unneeded files")

                for file in os.listdir(new_alloc):
                    if file.endswith(".csv"):
                        os.remove(os.path.join(new_alloc, file))
                shutil.rmtree(
                    os.path.join(ip_new["output"], ip_new["move_out_rate_path"])
                )

    collect_metrics(ip_changes, ip)
    #aggregated_plots_parameter_variation(ip_changes, ip, "alloc_out")


def collect_metrics(ip_changes, ip, show_plots=False):
    comparison_list = [
        "inhabit_vs_inhabit_created",
        "move_vs_move_in_created",
        "move_vs_move_in_want_created",
    ]

    result_dict = {}
    for element in comparison_list:
        result_dict[element] = pd.DataFrame()

    for parameter, variation_list in ip_changes.items():
        for variation in variation_list:
            # folder_name = f"{parameter}_{variation}"
            for comparison in comparison_list:
                filename = os.path.join(
                    ip["output"],
                    f"{parameter}_{variation}",
                    f"metrics_{comparison}.csv",
                )
                metrics = pd.read_csv(filename, index_col=0)["nrmse"].round(8)
                result_dict[comparison][f"{parameter}: {variation}"] = pd.concat(
                    [metrics, metrics.describe()]
                )

        # plot for every parameter
        fig, ax = plt.subplots()
        column_names = [f"{parameter}: {variation}" for variation in variation_list]
        fig, axes = plt.subplots(3, 1, sharex=True, figsize=(8, 12))
        fig.suptitle(f"Variation of {parameter}")
        fig.subplots_adjust(right=0.58, left=0.1, hspace=0.2)
        for n, name in enumerate(comparison_list):
            result_dict[name][column_names].iloc[
                : ip["target_year"] - ip["model_start_year"]
            ].plot(ax=axes[n], title=name, legend=False, grid=True)
        axes[0].legend(loc="upper right", bbox_to_anchor=(1.85, 1))
        plt.savefig(os.path.join(ip["output"], f"metrics_plot_{parameter}.png"))
        if show_plots:
            plt.show()
        # result_dict[comparison_list[2]]

    with pd.ExcelWriter(os.path.join(ip["output"], "all_metrics.xlsx")) as writer:
        for name, Dataframe in result_dict.items():
            Dataframe.to_excel(writer, sheet_name=name)


def aggregated_plots_parameter_variation(
    ip_changes, ip, default_foldername, show_plots=False
):
    room_nos_list = ep.arange_room_nos_list(ip["max_rooms"])
    for parameter, variation_list in ip_changes.items():
        print(parameter)

        # hh type dwell type
        n_cols = len(variation_list) + 1
        fig, axes = plt.subplots(2, n_cols, figsize=[6.5 + 0.9 * n_cols, 7])
        fig.suptitle(f"{parameter} Variation")
        fig.subplots_adjust(right=0.8, left=0.05, hspace=0.5, top=0.85)

        # add default results
        for k, hh_type in enumerate(["with_children", "without_children"]):
            data = pd.read_excel(
                os.path.join(
                    ip["output"],
                    default_foldername,
                    "Aggregated_data_hh_type-dwell_type.xlsx",
                ),
                index_col=0,
                sheet_name=hh_type,
            )
            data[
                [f"{dwelling_type} empirical" for dwelling_type in ["SFH", "MFH"]]
            ].plot(
                ax=axes[k, 0],
                title=hh_type,
                color=ep.COLOR_LIST,
                linestyle="-.",
                legend=False,
                grid=True,
            )
            data[[f"{dwelling_type} modeled" for dwelling_type in ["SFH", "MFH"]]].plot(
                ax=axes[k, 0],
                title=hh_type,
                color=ep.COLOR_LIST,
                linestyle="-",
                legend=False,
                grid=True,
            )
        # loop over parameter variations
        for i, variation in enumerate(variation_list):
            if parameter == "max_rooms":
                room_nos_list = ep.arange_room_nos_list(variation)
            for k, hh_type in enumerate(["with_children", "without_children"]):
                data = pd.read_excel(
                    os.path.join(
                        ip["output"],
                        f"{parameter}_{variation}",
                        "Aggregated_data_hh_type-dwell_type.xlsx",
                    ),
                    index_col=0,
                    sheet_name=hh_type,
                )
                data[
                    [f"{dwelling_type} empirical" for dwelling_type in ["SFH", "MFH"]]
                ].plot(
                    ax=axes[k, i + 1],
                    title=hh_type,
                    color=ep.COLOR_LIST,
                    linestyle="-.",
                    legend=False,
                    grid=True,
                )
                data[
                    [f"{dwelling_type} modeled" for dwelling_type in ["SFH", "MFH"]]
                ].plot(
                    ax=axes[k, i + 1],
                    title=hh_type,
                    color=ep.COLOR_LIST,
                    linestyle="-",
                    legend=False,
                    grid=True,
                )
        # customize plot
        rot = 0
        if parameter in ["alloc_order_hh", "alloc_vary_order_dwell"]:
            rot = 15
        for column in range(n_cols):
            axes[0, column].set_title(
                f'{(["default"]+variation_list)[column]}\nwith children', rotation=rot
            )
        axes[0, -1].legend(loc="upper right", bbox_to_anchor=(1.9, 1))
        plt.savefig(
            os.path.join(
                ip["output"], f"plot_{parameter}_aggregated_hh-type-dwell_type.png"
            )
        )
        if show_plots:
            plt.show()

        # hh type no_of_rooms
        n_cols = len(variation_list) + 1
        fig, axes = plt.subplots(2, n_cols, figsize=[6.5 + 0.9 * n_cols, 7])
        fig.suptitle(f"{parameter} Variation")
        fig.subplots_adjust(right=0.8, left=0.05, hspace=0.5, top=0.85)

        for k, hh_type in enumerate(["with_children", "without_children"]):
            data = pd.read_excel(
                os.path.join(
                    ip["output"],
                    default_foldername,
                    "Aggregated_data_hh_type-room_no.xlsx",
                ),
                index_col=0,
                sheet_name=hh_type,
            )
            data[[f"{no_of_rooms} empirical" for no_of_rooms in room_nos_list]].plot(
                ax=axes[k, 0],
                title=hh_type,
                color=ep.COLOR_LIST,
                linestyle="-.",
                legend=False,
                grid=True,
            )
            data[[f"{no_of_rooms} modeled" for no_of_rooms in room_nos_list]].plot(
                ax=axes[k, 0],
                title=hh_type,
                color=ep.COLOR_LIST,
                linestyle="-",
                legend=False,
                grid=True,
            )

        for i, variation in enumerate(variation_list):
            if parameter == "max_rooms":
                room_nos_list = ep.arange_room_nos_list(variation)
            # fig.text(
            # s=f'{variation}', x=0.5*(i+2)/len(variation_list), y=0.91, fontsize=11)
            for k, hh_type in enumerate(["with_children", "without_children"]):
                data = pd.read_excel(
                    os.path.join(
                        ip["output"],
                        f"{parameter}_{variation}",
                        "Aggregated_data_hh_type-room_no.xlsx",
                    ),
                    index_col=0,
                    sheet_name=hh_type,
                )
                data[
                    [f"{no_of_rooms} empirical" for no_of_rooms in room_nos_list]
                ].plot(
                    ax=axes[k, i + 1],
                    title=hh_type,
                    color=ep.COLOR_LIST,
                    linestyle="-.",
                    legend=False,
                    grid=True,
                )
                data[[f"{no_of_rooms} modeled" for no_of_rooms in room_nos_list]].plot(
                    ax=axes[k, i + 1],
                    title=hh_type,
                    color=ep.COLOR_LIST,
                    linestyle="-",
                    legend=False,
                    grid=True,
                )
        rot = 0
        if parameter in ["alloc_order_hh", "alloc_vary_order_dwell"]:
            rot = 15
        for column in range(n_cols):
            axes[0, column].set_title(
                f'{(["default"] + variation_list)[column]}\nwith children', rotation=rot
            )

        axes[0, -1].legend(loc="upper right", bbox_to_anchor=(1.9, 1))
        plt.savefig(
            os.path.join(
                ip["output"], f"plot_{parameter}_aggregated_hh-type-room_no.png"
            )
        )
        if show_plots:
            plt.show()

        # ------ quintile vs. dwelling type--------
        filename = "Aggregated_data_quintile-dwell_type.xlsx"
        n_cols = len(variation_list) + 1
        fig, axes = plt.subplots(
            len(ep.QUINTILES), n_cols, figsize=[6.5 + 0.9 * n_cols, 10]
        )
        fig.suptitle(f"{parameter} Variation")
        fig.subplots_adjust(right=0.8, left=0.05, hspace=0.5, top=0.85)

        # load default data and add to plot
        for k, quintile in enumerate(ep.QUINTILES):
            data = pd.read_excel(
                os.path.join(ip["output"], default_foldername, filename),
                index_col=0,
                sheet_name=quintile,
            )
            data[[f"{dw_type} empirical" for dw_type in ep.DWELLING_TYPES]].plot(
                ax=axes[k, 0],
                title=quintile,
                color=ep.COLOR_LIST,
                linestyle="-.",
                legend=False,
                grid=True,
            )
            data[[f"{dw_type} modeled" for dw_type in ep.DWELLING_TYPES]].plot(
                ax=axes[k, 0],
                title=quintile,
                color=ep.COLOR_LIST,
                linestyle="-",
                legend=False,
                grid=True,
            )
        # loop over variations, load data and add to plot
        for i, variation in enumerate(variation_list):
            if parameter == "max_rooms":
                room_nos_list = ep.arange_room_nos_list(variation)
            for k, quintile in enumerate(ep.QUINTILES):
                data = pd.read_excel(
                    os.path.join(ip["output"], f"{parameter}_{variation}", filename),
                    index_col=0,
                    sheet_name=quintile,
                )
                data[[f"{dw_type} empirical" for dw_type in ep.DWELLING_TYPES]].plot(
                    ax=axes[k, i + 1],
                    title=quintile,
                    color=ep.COLOR_LIST,
                    linestyle="-.",
                    legend=False,
                    grid=True,
                )
                data[[f"{dw_type} modeled" for dw_type in ep.DWELLING_TYPES]].plot(
                    ax=axes[k, i + 1],
                    title=quintile,
                    color=ep.COLOR_LIST,
                    linestyle="-",
                    legend=False,
                    grid=True,
                )
        # customize plot
        rot = 0
        if parameter in ["alloc_order_hh", "alloc_vary_order_dwell"]:
            rot = 15
        for column in range(n_cols):
            axes[0, column].set_title(
                f'{(["default"] + variation_list)[column]}\nq1', rotation=rot
            )
        axes[0, -1].legend(loc="upper right", bbox_to_anchor=(1.9, 1))
        plt.savefig(
            os.path.join(
                ip["output"], f"plot_{parameter}_aggregated_quintile-dwelling_type.png"
            )
        )
        if show_plots:
            plt.show()

        # ------ quintile no_of_rooms--------
        filename = "Aggregated_data_quintile-room_no.xlsx"
        n_cols = len(variation_list) + 1
        fig, axes = plt.subplots(
            len(ep.QUINTILES), n_cols, figsize=[6.5 + 0.9 * n_cols, 10]
        )
        fig.suptitle(f"{parameter} Variation")
        fig.subplots_adjust(right=0.8, left=0.05, hspace=0.5, top=0.85)

        # load default data and add to plot
        for k, quintile in enumerate(ep.QUINTILES):
            data = pd.read_excel(
                os.path.join(ip["output"], default_foldername, filename),
                index_col=0,
                sheet_name=quintile,
            )
            data[[f"{no_of_rooms} empirical" for no_of_rooms in room_nos_list]].plot(
                ax=axes[k, 0],
                title=quintile,
                color=ep.COLOR_LIST,
                linestyle="-.",
                legend=False,
                grid=True,
            )
            data[[f"{no_of_rooms} modeled" for no_of_rooms in room_nos_list]].plot(
                ax=axes[k, 0],
                title=quintile,
                color=ep.COLOR_LIST,
                linestyle="-",
                legend=False,
                grid=True,
            )
        # loop over variations, load data and add to plot
        for i, variation in enumerate(variation_list):
            if parameter == "max_rooms":
                room_nos_list = ep.arange_room_nos_list(variation)
            for k, quintile in enumerate(ep.QUINTILES):
                data = pd.read_excel(
                    os.path.join(ip["output"], f"{parameter}_{variation}", filename),
                    index_col=0,
                    sheet_name=quintile,
                )
                data[
                    [f"{no_of_rooms} empirical" for no_of_rooms in room_nos_list]
                ].plot(
                    ax=axes[k, i + 1],
                    title=quintile,
                    color=ep.COLOR_LIST,
                    linestyle="-.",
                    legend=False,
                    grid=True,
                )
                data[[f"{no_of_rooms} modeled" for no_of_rooms in room_nos_list]].plot(
                    ax=axes[k, i + 1],
                    title=quintile,
                    color=ep.COLOR_LIST,
                    linestyle="-",
                    legend=False,
                    grid=True,
                )
        # customize plot
        rot = 0
        if parameter in ["alloc_order_hh", "alloc_vary_order_dwell"]:
            rot = 15
        for column in range(n_cols):
            axes[0, column].set_title(
                f'{(["default"] + variation_list)[column]}\nq1', rotation=rot
            )
        axes[0, -1].legend(loc="upper right", bbox_to_anchor=(1.9, 1))

        plt.savefig(
            os.path.join(
                ip["output"], f"plot_{parameter}_aggregated_quintile-room_no.png"
            )
        )
        if show_plots:
            plt.show()


if __name__ == "__main__":
    main(delete_files=True, scen_run=False)
