Usage
=====

.. _installation:

Installation
------------

Ensure that you have *python >= 3.9.13* installed. To see your python version, open your terminal and type:

.. code-block:: console

  $ python --version


Data
----

soep explanations and weblinks
bbsr explanations and weblinks


Running the model
-----------------

- Run the model by opening the terminal, navigating to the project folder and executing:
  
  .. code-block:: console
  
    $ python3 inhabit_table.py

