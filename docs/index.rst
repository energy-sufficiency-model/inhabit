.. Inhabit documentation master file, created by
   sphinx-quickstart on Mon Aug  7 15:44:24 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Inhabit's documentation!
===================================

.. toctree::

   intro
   usage
   code
   api




