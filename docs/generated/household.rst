﻿household
=========

.. automodule:: household

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      age_class
      growth_type
      household_types
      income_quintiles
      load_data
      region_type
      size_class
   
   

   
   
   

   
   
   



