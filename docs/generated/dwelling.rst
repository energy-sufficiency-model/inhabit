﻿dwelling
========

.. automodule:: dwelling

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      house_condition
      load_data
      owner_type
      room_num
      tabula_building_types
   
   

   
   
   

   
   
   



