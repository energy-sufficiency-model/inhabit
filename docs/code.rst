Understanding the code
======================

Code from inhabit_table.py
--------------------------
.. automodule:: inhabit_table
  :members:
  :undoc-members:  
  :noindex:

Code from calibrate.py
--------------------------
.. automodule:: calibrate
  :members:
  :undoc-members:  
  :noindex:

Code from allocation.py
----------------------
.. automodule:: scripts.allocation
  :members:
  :undoc-members:
  :noindex:

Code from alloc_calibration.py
---------------------

.. automodule:: scripts.alloc_calibration
  :members:
  :undoc-members:
  :noindex:

Code from check_values_labels.py
-----------------

.. automodule:: scripts.check_values_labels
  :members:
  :undoc-members:
  :noindex:
  
Code from dwelling.py
-------------------------------

.. automodule:: scripts.dwelling
  :members:
  :undoc-members:
  :noindex:
  
 
Code from evaluate_missings.py
--------------------------
.. automodule:: scripts.evaluate_missings
  :members:
  :undoc-members:  
  :noindex:

Code from filters.py
-----------------

.. automodule:: scripts.filters
  :members:
  :undoc-members:
  :noindex:
  
Code from household.py
-------------------------------

.. automodule:: scripts.household
  :members:
  :undoc-members:
  :noindex:
 
Code from inputs.py
--------------------------
.. automodule:: scripts.inputs
  :members:
  :undoc-members:  
  :noindex:

  
Code from misc.py
-------------------------------

.. automodule:: scripts.misc
  :members:
  :undoc-members:
  :noindex:
 
Code from move_out_rate.py
--------------------------
.. automodule:: scripts.move_out_rate
  :members:
  :undoc-members:  
  :noindex:

  
Code from occupation_charts.py
-------------------------------

.. automodule:: scripts.occupation_charts
  :members:
  :undoc-members:
  :noindex:
 
Code from soep_loader.py
--------------------------
.. automodule:: scripts.soep_loader
  :members:
  :undoc-members:  
  :noindex:

