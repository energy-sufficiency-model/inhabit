Structure of Inhabit project
============================

- starting point is the ``inhabit.py`` file.
- ``calibrate.py`` file is only used for parameter calibration.
- folder scripts/ for all other \*.py scripts
- ``scripts.occupation_charts.py`` for generating figures about under/overoccupation. Minor adaptions in the code are needed for that.

