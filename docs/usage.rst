Usage
=====

.. _installation:

Installation
------------

Ensure that you have *python >= 3.11.9* installed. To see your python version, open your terminal and type:

.. code-block:: console

  $ python --version


Data
----

soep information: https://companion.soep.de/index.html
In this version, no extra soep is needed. We have aggregated soep data to inhabit and move out matrices (to be found in data/evidence/) which are used as a basis for future projections.


Running the model
-----------------

- To run the model, open a terminal, navigate to the project folder and execute:
  
  .. code-block:: console
  
    $ pip3 install -r requirements.txt 
    

- After all requirements are installed, you can run the model by calling:
 
  .. code-block:: console
  
    $ python3 inhabit_table.py


- Don't forget to play with the inputs in the inputs.xlsx
