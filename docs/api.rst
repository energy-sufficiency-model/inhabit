API
===

.. autosummary::
  :toctree: generated

  inhabit_table
  calibrate
  allocation
  alloc_calibration
  check_values_label
  dwelling
  evaluate_missings
  evaluation_plots
  filters
  household
  inputs
  misc
  move_out_rate
  occupation_charts
  soep_loader
