"""Analyse inhabit table or empirical data of variables from SOEP Core."""

import pandas as pd
import numpy as np
import os
import seaborn as sns
import matplotlib.pyplot as plt

import misc
import dwelling

import household


@misc.timer_func
def load_df_raw(soep_path, output_path, wum_path, vars_datasets):
    """Load all csv data that is needed to one df."""
    df_raw = pd.read_csv(
        os.path.join(soep_path, "ppathl.csv"),
        usecols=[
            "pid",  # Never Changing Person ID
            "hid",  # household ID
            "syear",  # survery year
            "gebjahr",  # birth year, 4 digit
            "netto",  # personal survey status
        ],
    )
    # Attach from hpathl:
    # hnetto: succefull interview?
    df_raw = misc.attachVariable(
        soep_path=soep_path,
        add_vars=["hnetto"],
        merge_from="hpathl",
        merge_to=df_raw,
        match_keys=["hid", "syear"],
    )

    # load all other variables
    for var, dataset in vars_datasets.items():
        path = soep_path
        match_keys = ["hid", "syear"]
        if var in ["resmove", "will_move"]:
            match_keys = ["pid", "syear"]
        elif var == "wum1_updated":
            dataset = var
            path = wum_path
        if var == "will_move" and not os.path.exists(
            os.path.join(output_path, "will_move.csv")
        ):
            _, _ = household.movers(df_raw, soep_path, ["hid"])
        if var == "hgowner":
            # adding additional variable for owner status
            df_raw = misc.attachVariable(
                soep_path=path,
                add_vars=["hlf0013_h"],
                merge_from="hl",
                merge_to=df_raw,
                match_keys=match_keys,
            )
        try:
            df_raw = misc.attachVariable(
                soep_path=path,
                add_vars=[var],
                merge_from=dataset,
                merge_to=df_raw,
                match_keys=match_keys,
            )
        except Exception as e:
            print(f"Exception {e} occured.")
            print(f"Dataset: {dataset}, variable: {var}, path: {path}")
            print(f"match_keys: {match_keys}")
            exit(1)

    return df_raw


@misc.timer_func
def check_missings_per_var(var, dataset, file, df_raw, ip):
    """Analyse if there is data for all years for the specific variable var (str)."""
    print(f"Loading data for variable {var}")

    print(f"Starting analysis for {var}")
    file.writelines(f"===Evaluation of variable {var}===\n")
    file.writelines(f"Observations total: {len(df_raw.index)} \n")
    output_path = ip.loc["output"].values.item()
    soep_path = ip.loc["soep"].values.item()
    start_year = ip.loc["start_year"].values.item()
    end_year = ip.loc["empirical_data_end_year"].values.item()

    total_obs = pd.DataFrame(columns=[var], index=range(start_year, end_year + 1))
    dfs_grouped = {}

    # Calculate availability for every year
    years = [i for i in range(start_year, end_year + 1)]
    for yr in years:
        # Calculate building type for 2019,20,21
        if (
            var == "wum1_updated"
            and (not os.path.exists(os.path.join(output_path, "wum1_updated.csv")))
            and (yr in [2019, 2020, 2021])
        ):
            cols_used = ["hid"]
            df_raw, cols_used = dwelling.building_type(df_raw, soep_path, cols_used, yr)

        # Apply filters
        df_yr = df_raw.copy()
        df_yr = misc.year_filter(df_yr, set_year=yr)
        # change to "pnetto_filter()" for analysis on pid level
        df_yr = misc.netto_filter(df_yr)
        df_yr = misc.households_filter(df_yr)  # disable for analysis on pid level
        df_yr = misc.age_filter(
            df_yr,
            min_age=int(ip.loc["min_age"].values.item()),
            max_age=int(ip.loc["max_age"].values.item()),
        )

        if var == "hid" or var == "pid":
            df_yr = df_yr[[var]]
            total_obs[var][yr] = len(df_yr.index)
            continue

        elif var == "hgowner":
            # Fill ownership column with data from hlf0013_h and hgowner
            # change to ["pid"] analysis on hid level
            df_yr, _ = dwelling.owner_type(df_yr.copy(), ip, ["hid"], dm="")

        # Count the values in each possible answer group except for var == "hid"
        # (also the group NaN)
        # TODO problem for var == wum1_updated and var == wum1_hbrutt
        # TODO variables not as column in df returned by dwelling.building_type().
        # Throws error below
        try:
            dfs_grouped[yr] = df_yr[var].value_counts()
            df_yr = df_yr[[var]]

            # Clean NaNs except for var == "hid" or "pid" (every household has an hid)
            df_yr = misc.clean_nan(df_yr)
            total_obs[var][yr] = len(df_yr.index)
        except Exception as e:
            print(f"Exception f{e} occured in year {yr} from var {var} from {dataset}.")
            raise
    dfs_grouped = pd.DataFrame(dfs_grouped)
    file.writelines(dfs_grouped.to_string() + "\n")

    print("Finished analysis")
    return total_obs


@misc.timer_func
def create_heatmap(df, order, save_path):
    """Create a heatmap for total observations.

    red = low amount of observations
    green = high amount of observations

    Return:
    - Saves heatmap to visualize total obs. per var and yr
    in "save_path/total_obs_vars.png"
    """
    df = df.fillna(0)
    df = df.apply(pd.to_numeric)

    df = df[order]

    plt.figure(figsize=(10, 6))
    sns.heatmap(df, cmap="RdYlGn", annot=True, fmt="d")
    plt.title("Total observations for each variable and year")
    plt.xlabel("Variables")
    plt.ylabel("Years")

    plt.savefig(os.path.join(save_path, "total_obs_vars.png"))
    plt.show()


@misc.timer_func
def check_missings(vars_datasets):
    """Calculate data data availability for list_of_vars.

    Parameters:
    - list_of_vars: dict, {var: dataset}, e.g. {"hid": "hgen", ...}

    Return:
    - df_res: total observations for every variable and year
    also saved as CSV in "save_path/total_obs_vars.csv"
    - Saves per variable observations for every catergory and year
    in "save_path/evaluate_missing.txt"
    """
    ip = misc.load_inputs()
    output_path = ip.loc["output"].values.item()
    soep_path = ip.loc["soep"].values.item()
    wum_path = ip.loc["composita_path"].values.item()
    analysis_path = os.path.join(output_path, "analysis")
    start_year = ip.loc["start_year"].values.item()
    end_year = ip.loc["empirical_data_end_year"].values.item()

    # Create empty df, one col per var
    columns = vars_datasets.keys()
    df_res = pd.DataFrame(columns=columns, index=np.arange(start_year, end_year))

    if not os.path.exists(analysis_path):
        os.makedirs(analysis_path)

    df_raw = load_df_raw(soep_path, output_path, wum_path, vars_datasets)
    # Calculate data availabiliy for every variable, print results each to
    # "evaluate_missings.txt" and save total obs in "df_res" one col per var
    with open(os.path.join(analysis_path, "evaluate_missings.txt"), "w") as file:
        for var, dataset in vars_datasets.items():
            try:
                df_res[var] = check_missings_per_var(
                    var, dataset, file, df_raw.copy(), ip
                )
            except Exception as e:
                print(f"Error {e} occured on variable {var} from dataset {dataset}.")

    # Save table as CSV
    df_res.to_csv(os.path.join(analysis_path, "total_obs_vars.csv"))

    return df_res, analysis_path


def main():
    """Check if in variables there is enough data for every year."""
    vars_datasets = {
        "hid": "hgen",  # household id
        # "pid": "pgen",  # person id
        "hgcondit": "hgen",  # dwelling condition
        "hgroom": "hgen",  # number of rooms per dwelling
        "hgowner": "hgen",  # tenant/owner
        "wum1_updated": "wum1_updated",  # building type
        "wum1": "hbrutto",
        "hlf0154_h": "hl",  # building type old
        "housing1": "raw/housing2021",
        "hgtyp1hh": "hgen",  # household type
        "hhgr": "hbrutto",  # household size
        "hghinc": "hgen",  # household income
        # "resmove": "raw/movedist",
        # "will_move": "will_move",  # calculated from raw/movedist/resmove
    }

    # Option 1: Calculate total observations per var
    df_res, analysis_path = check_missings(vars_datasets)

    # Option 2: Only generate heatmap
    # csv_path = os.path.join(save_path, "total_obs_vars.csv")
    # df_res = pd.read_csv(csv_path, index_col=0)
    # order = ["hid", "hgcondit", "hlf0154_h", "hlf0013_h"]
    order = vars_datasets.keys()
    create_heatmap(df_res, order, analysis_path)


if __name__ == "__main__":
    main()
