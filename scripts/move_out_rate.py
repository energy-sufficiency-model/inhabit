"""Evaluate the inhabit matrices and move out matrices created by inhabit.py."""

import pandas as pd
import numpy as np
import os
import concurrent.futures
from itertools import pairwise, product
from sklearn.linear_model import LinearRegression
from scipy.special import softmax
from sklearn.metrics import r2_score
import copy

try:
    import misc
except ModuleNotFoundError:
    import scripts.misc as misc


def interpolate(year_1, year_2, factors):
    """Interpolate from x to y on a yearly basis.
    First create years between x and y, then check factors values for years
    x and y and interpolate the values for the years in between.

    Args:
        year_1 (str): year based on columns from factors dataframe.
        year_2 (str): year based on columns from factors dataframe.
        factors (df): loaded factors for each column and year for margins.

    Returns:
        factors: updated with values for the yeas between x and y
    """

    # get interpolation between x and y
    def get_interpolation(row_y1, row_y2, years_diff):
        if row_y1 is np.nan or row_y2 is np.nan or row_y1 == row_y2:
            return [row_y1 for _ in range(years_diff)]
        linear_factor = (row_y2 - row_y1) / years_diff
        linear = [row_y1]
        for _ in range(years_diff - 1):
            linear.append(linear[-1] + linear_factor)
        linear = [x for x in linear if x != row_y1]

        return linear

    years_diff = int(year_2) - int(year_1)
    interpol = [int(year_1) + offset for offset in range(1, years_diff)]
    # fac = factors.copy()
    # for index, row in fac.iterrows():
    interpolation = get_interpolation(
        factors[year_1].iloc[0], factors[year_2].iloc[0], years_diff
    )
    for value, year in zip(interpolation, interpol):
        factors[int(year)] = value
    return factors


def load_factors(ip, scen_def):
    """Load factors from move_out_rate_input.xlsx file and apply yearly interpolation.

    Args:
        ip (dict): contains user-input from xlsx and in-program added variables.

    Returns:
        factors (df): loaded and interpolated factors to apply to yearly move_out_rates.
    """

    def handle_factors(factor):
        original_columns = factor.columns
        for a, b in list(pairwise(original_columns[2:])):
            if int(b) - int(a) not in [0, 1]:
                factor = interpolate(a, b, factor)
        new_index = list(factor.columns[:2])
        new_index = new_index + sorted(factor.columns[2:])
        return factor.reindex(new_index, axis=1)

    input_path = os.path.join(
        ip["mor_modification_folder"],
        f"{scen_def}.xlsx",
    )
    factors1 = pd.read_excel(
        input_path,
        nrows=12,
        header=1,
    )
    factors1 = handle_factors(factors1)
    factors2 = pd.read_excel(
        input_path,
        skiprows=15,
        nrows=12,
        header=1,
    )
    factors2 = handle_factors(factors2)

    return factors1, factors2

def apply_factors(mor, ip, year, factors1, factors2):
    mor = mor.reset_index()  # Reset index for easier merging

    for factors in [factors1, factors2]:
        year_factor = factors[year].iloc[0]
        if year_factor != 1.0:
            for _, row in factors.iterrows():  # Iterate through rows for more control
                col = row["column name"]
                param = row["parameter"]

                if col != "choose column" and not pd.isna(param):
                    if col == "rooms" and param == ip["max_rooms"]:
                        param = f'{ip["max_rooms"]}+'
                    elif col == "hh_size" and param == ip["max_hh_size"]:
                        param = f'{ip["max_hh_size"]}+'

                    # Create a boolean mask for the current condition
                    mask = mor[col].astype(str) == str(param)  # Consistent string comparison

                    # Apply the factor using the mask
                    mor.loc[mask, ip["col_move_per_pop"]] *= year_factor

    return mor

"""def apply_factors(mor, ip, year, factors1, factors2):
    mor = mor.reset_index()

    for factors in [factors1, factors2]:
        year_factor = factors[year].iloc[0]  # zero line factor is sufficcient
        if year_factor != 1.0:
            col_to_param = {
                col: []
                for col in factors["column name"]
                if not (col == "choose column")
            }
            for col, param in zip(factors["column name"], factors["parameter"]):
                if not ((col == "choose column") or (param == np.nan)):
                    if col == "rooms" and param == ip["max_rooms"]:
                        param = f'{ip["max_rooms"]}+'
                    elif col == "hh_size" and param == ip["max_hh_size"]:
                        param = f'{ip["max_hh_size"]}+'
                    col_to_param[col].append(str(param))

            mor_map = mor.copy()
            for k, v in col_to_param.items():
                mor_map = mor_map.loc[mor[k].isin(v)]
            # print(mor_map, year, year_factor)
            # apply mor_map to mor and multiply with year_factor
            mor.loc[mor_map.index, ip["col_move_per_pop"]] *= year_factor

    return mor"""


def linear_regression(x_regression, y_regression, x_new):
    # apply linear regression here
    # 𝑓(𝑥) = 𝑏₀ + 𝑏₁𝑥
    # x_regression: list of years, y_regression: list of weights
    model = LinearRegression().fit(x_regression, y_regression)
    return model.predict(x_new)


@misc.timer_func
def x_years_rate_regression(current_year, ip, dis):
    all_years = [
        year for year in range(ip["empirical_start_year"], ip["target_year"] + 1)
    ]
    passed_years = [year for year in range(ip["empirical_start_year"], current_year)]
    mor_dis = None
    for idx, year in enumerate(all_years):
        # only load data until empirical end year
        if year <= ip["empirical_end_year"]:
            # load absolute inhabit:
            inh_w, _ = misc.load_inhabit_moving(
                "inhabit", year, ip, load_abs=False, alt_path=None, use_weights=True
            )
            move_w, _ = misc.load_inhabit_moving(
                "move", year, ip, load_abs=False, alt_path=None, use_weights=True
            )
            inh_w = inh_w.merge(
                move_w, on=ip["to_group_col"], suffixes=["_inhabits", "_movers"]
            )
            inh_w = inh_w.groupby(dis).sum()
            inh_w[f"{ip['col_move_per_pop']}_{year}"] = (
                inh_w["weights_movers"] / inh_w["weights_inhabits"]
            )
            inh_w = inh_w.drop(["weights_movers", "weights_inhabits"], axis=1)

            if idx == 0:
                mor_dis = inh_w
            else:
                mor_dis = mor_dis.merge(inh_w, on=dis)

        # create empty columns to be filled by the following operations

        mor_dis[f"{ip['col_move_per_pop']}_{year}_reg"] = 0.0

    # x_new are all available years
    x_new = np.array(all_years).reshape((-1, 1))

    r2 = []
    for row in mor_dis.itertuples():
        y = np.array(
            [getattr(row, f'{ip["col_move_per_pop"]}_{year}') for year in passed_years]
        )
        x_reg, y_true = [], []

        for year, col_move_per_pop in zip(passed_years, y):
            x_reg.append(year)
            y_true.append(col_move_per_pop)

        x_reg = np.array(x_reg).reshape((-1, 1))
        y_pred = linear_regression(x_reg, y_true, x_new)
        r2.append(r2_score(y_true, y_pred[: len(passed_years)]))
        # ensure that no negative numbers are inserted for the weights in the future
        for xx, yy in zip(x_new, y_pred):
            mor_dis.loc[row.Index, f"{ip['col_move_per_pop']}_{xx[0]}_reg"] = max(
                0.0, yy
            )
    # r2 score mean from all disaggreagtions (e.g. from <45 and >=45)
    r2_sc = sum(r2) / len(mor_dis)
    # filter mor_dis to only take regression entries into account.
    # mor_dis entries: move_per_pop_2000, move_per_pop_2000_reg, ..., ..._2019_reg
    # take every second row, begin with ...2000_reg, end with ..._2019_reg (iloc[1::2])
    mor_reg = mor_dis.transpose().iloc[1::2]
    # sorted list of indizes from mean values of each disaggregation
    min_max_index = mor_reg.mean().sort_values(kind="mergesort").index
    # disaggregation with largest mean minus disaggregation with smallest mean
    mor_reg["difference"] = mor_reg[min_max_index[-1]] - mor_reg[min_max_index[0]]
    # sum must always be positive because of the mean entries of the difference
    sum_differences = mor_reg["difference"].sum()
    return mor_dis, r2_sc, sum_differences


def create_mor(
    ip,
    mor,
    year,
    all_disaggs,
    mors_factors,
    mors_disaggs,
    keys,
    mor_path,
    factors1_def,
    factors2_def,
    factors1_scen,
    factors2_scen,
):
    print(f"create move out rate for year {year}")
    mor.loc[mor["building_type"] == "SFH", [ip["col_move_per_pop"]]] = ip["min_mor_SFH"]
    mor.loc[mor["building_type"] == "MFH", [ip["col_move_per_pop"]]] = ip["min_mor_MFH"]
    mov_year = f'{ip["col_move_per_pop"]}_{year}_reg'
    for a, bt, size, iq, typ, rt in product(
        all_disaggs["age"],
        all_disaggs["building_type"],
        all_disaggs["hh_size"],
        all_disaggs["income_quintile"],
        all_disaggs["hh_type"],
        all_disaggs["region_type_dwell"],
    ):
        min_mor = ip["min_mor_SFH"] if bt == "SFH" else ip["min_mor_MFH"]
        calc_mor = sum(
            [
                (
                    mors_factors[k]  # parameter given factor (e.g. 1/6)
                    * mors_disaggs[k]
                    .loc[mors_disaggs[k].index == v, mov_year]
                    .to_numpy()[0]
                )
                for k, v in zip(keys, [a, bt, size, iq, typ, rt])
            ]
        )

        mor.loc[
            (
                (mor["age"] == a)
                & (mor["building_type"] == bt)
                & (mor["hh_size"] == size)
                & (mor["income_quintile"] == iq)
                & (mor["hh_type"] == typ)
                & (mor["region_type_dwell"] == rt)
            ),
            ip["col_move_per_pop"],
        ] = max(min_mor, calc_mor)

    # print(f"{year} minimum mor: {mor['move_per_pop'].min()}")
    # save yearly mor to file
    save_mor = mor.copy().set_index(ip["to_group_col"])
    if save_mor[ip["col_move_per_pop"]].min() < min(
        ip["min_mor_SFH"], ip["min_mor_MFH"]
    ):
        print(f"error with mor creation in year {year}")
        print(save_mor)
        exit(1)

    if year < int(ip["scenario_start_year"]):
        save_mor = apply_factors(save_mor, ip, year, factors1_def, factors2_def)
    else:
        save_mor = apply_factors(save_mor, ip, year, factors1_scen, factors2_scen)

    # print(f'{year}: after factors: {save_mor["move_per_pop"].sum()}')
    save_mor.to_csv(os.path.join(mor_path, f"mor_{year}.csv"))


@misc.timer_func
def get_mor(ip, current_year):
    # load variables
    mor_path = misc.get_save_path(ip["output"], ip["move_out_rate_path"])

    full_mor_path = os.path.join(mor_path, f"mor_{current_year}.csv")

    if os.path.exists(full_mor_path):
        mor = pd.read_csv(full_mor_path, index_col=ip["to_group_col"])
        return mor
    # else case needs no else

    # age_classes = misc.get_age_classes(ip)
    # age_classes = list(set(age_classes.values()))
    age_classes = [f"<{ip['age_limiter']}", f">={ip['age_limiter']}"]

    # create mor directly from regression using these disaggregations
    all_disaggs = {
        "age": age_classes,
        "building_type": ["SFH", "MFH"],
        "hh_size": [f"{x}" for x in range(1, ip["max_hh_size"])],
        "income_quintile": ["q1", "q2", "q3", "q4", "q5"],
        "hh_type": ["with_children", "without_children"],
        "region_type_dwell": ["urban", "rural"],
    }
    all_disaggs["hh_size"].append(f"{ip['max_hh_size']}+")
    mors_disaggs = {}
    mors_r2 = {}
    mors_summed_differences = {}
    for dis in all_disaggs.keys():
        mors_disaggs[dis], mors_r2[dis], mors_summed_differences[dis] = (
            x_years_rate_regression(current_year, ip, dis)
        )
        mors_disaggs[dis].to_csv(
            os.path.join(
                mor_path,
                f"mor_{ip['empirical_start_year']}_{ip['target_year']}_{dis}.csv",
            )
        )
    # export mor regression metrics
    df_differences = pd.DataFrame.from_dict(
        mors_summed_differences, orient="index", columns=["summed difference"]
    )
    df_r2 = pd.DataFrame.from_dict(mors_r2, orient="index", columns=["R2"])
    df_mor_reg_metrics = pd.concat([df_r2, df_differences], axis=1)
    df_mor_reg_metrics.to_excel(os.path.join(mor_path, "mor_reg_metrics.xlsx"))
    # load inhabit matrix for inhabit structure. Set it to 0 and change weights name
    mor, _ = misc.load_inhabit_moving("inhabit", current_year, ip)
    mor = mor.reset_index()
    mor = mor.rename(columns={ip["col_weights"]: ip["col_move_per_pop"]})
    keys = list(all_disaggs.keys())
    mors_factors = {}
    if ip["mor_disagg_weight"] == "r2_score":
        to_scale = [mors_r2[k] for k in keys]
        scaled = softmax(np.array(to_scale))
        for enu, k in enumerate(keys):
            mors_factors[k] = scaled[enu]
    elif ip["mor_disagg_weight"] == "summed_diff_score":
        to_scale = [mors_summed_differences[k] for k in keys]
        scaled = softmax(np.array(to_scale))
        for enu, k in enumerate(keys):
            mors_factors[k] = scaled[enu]
    elif ip["mor_disagg_weight"] == "best":
        best_val = max(list(mors_r2.values()))
        for k, v in mors_r2.items():
            mors_factors[k] = 1 if v == best_val else 0
    elif ip["mor_disagg_weight"] == "equal":
        for k in mors_r2.keys():
            mors_factors[k] = 1 / len(keys)

    # load factors here to apply them when needed
    # use default factors until scenario year.
    factors1_def, factors2_def = load_factors(ip, ip["manual_mor_modification"])
    factors1_scen, factors2_scen = load_factors(
        ip, ip["scenario_manual_mor_modification"]
    )

    pd.DataFrame.from_dict(mors_factors, orient='index', columns=["factors"]).to_csv(os.path.join(mor_path, f"mor_projection_factors.csv"))

    '''with concurrent.futures.ThreadPoolExecutor() as executor:
        [
            executor.submit(
                create_mor,
                ip,
                mor,
                year,
                all_disaggs,
                mors_factors,
                mors_disaggs,
                keys,
                mor_path,
                factors1_def,
                factors2_def,
                factors1_scen,
                factors2_scen,
            )
            for year in range(current_year, ip["target_year"] + 1)
        ]
    '''

    with concurrent.futures.ProcessPoolExecutor() as executor:
        futures = [
            executor.submit(
                create_mor,
                ip,
                copy.deepcopy(mor),  # Tiefe Kopie, um Datenkonflikte zu vermeiden
                year,
                copy.deepcopy(all_disaggs),
                copy.deepcopy(mors_factors),
                copy.deepcopy(mors_disaggs),
                copy.deepcopy(keys),
                mor_path,
                copy.deepcopy(factors1_def),
                copy.deepcopy(factors2_def),
                copy.deepcopy(factors1_scen),
                copy.deepcopy(factors2_scen),
            )
            for year in range(current_year, ip["target_year"] + 1)
        ]
    # reload mor from current year to return
    mor = pd.read_csv(
        os.path.join(mor_path, f"mor_{current_year}.csv"),
        index_col=ip["to_group_col"],
    )
    return mor
