"""inputs for the model"""

import pandas as pd

try:
    import scripts.misc as misc
    import scripts.household as household
    import scripts.dwelling as dwelling
    import scripts.soep_loader as soep_loader
except ModuleNotFoundError:
    import misc
    import household
    import dwelling
    import soep_loader
# GLOBALS
INPUT_PATH = "inputs.xlsx"


def load_inputs(**flags):
    """Load input csv and add path to composita file"""
    ip_from_csv_df = pd.read_excel(
        misc.check_absolute_path(INPUT_PATH),
        index_col="Name",
        usecols=["Name", "Value"],
    )

    composita_path = misc.get_save_path(
        ip_from_csv_df.loc["soep"].values.item(), "soep_composita"
    )

    ip_from_csv = ip_from_csv_df.to_dict()["Value"]

    ip_from_code = {
        "data": "data",
        "mor_modification_folder": "mor_modification",
        "soep_composita_path": composita_path,
        "output_folder": "output",
        "evidence_folder": "evidence",
        "inhabit_path": "inhabit",
        "move_out_path": "move",
        "move_out_rate_path": "move_out_rate",
        "alloc_output_path": "alloc_out",
        "dwelling_ownership_short": "true",  # non profit included in private tenant
        "household_classification": "children",  # or types
        "empirical_start_year": 2000,  # 1986
        "empirical_end_year": 2019,
        # cols that are to be added to household and dwelling df
        "hh": {
            "hh_type": household.household_types,
            "hh_size": household.size_class,
            "age": household.age_class,
            "income_quintile": household.income_quintiles,
            "region_type_hh": dwelling.region_type,
            # "growth_type": dwelling.growth_type,
        },
        # TODO: add test to only allow new dwelling options if they have functions
        # everywhere needed (especially at the allocation algorithm)
        "dwell": {
            "building_type": dwelling.building_type,
            "ownership": dwelling.owner_type,
            "condition": dwelling.house_condition,
            "rooms": dwelling.room_num,
            "region_type_dwell": dwelling.region_type,
            # "growth_type": dwelling.growth_type,
        },
        "col_weights": "weights",
        "col_move_per_pop": "move_per_pop",
        "col_oldest": "oldest_person_in_hh",
        "move": {
            "col_will_move": "will_move",  # person that will move
            "col_moved": "moved",  # column for entry who has moved in past year
            "csv_move": "will_move.csv",
            "calc_movers": soep_loader.movers,  # calculate/determine who will move
        },
        "debug_message": "",
        "household_data": "household_data_{}.csv",
        "dwelling_data": "dwelling_data_{}.csv",
        "max_age": 120,
        "min_age": 15,
        "age_limiter": 55,  # insert more ages to get age spans
    }

    ip = {**ip_from_csv, **ip_from_code, **flags}

    ip["cols_dwell"] = list(ip["dwell"].keys())
    ip["cols_hh"] = list(ip["hh"].keys())
    ip["to_group_col"] = [*ip["cols_dwell"], *ip["cols_hh"]]
    ip["default_output"] = misc.get_save_path(ip["output_folder"], "default")
    ip["output"] = misc.get_save_path(ip["output_folder"], ip["scenario_name"])
    if ip["target_year"] == "last_available_data":
        ip["target_year"] = ip["empirical_end_year"]
    ip["evidence_folder"] = misc.get_save_path(ip["data"], "evidence")

    return ip
