"""
This script evaluates the yearly results of the dwelling allocation process.
It therefore compares the inhabit matrices of the allocation output with the respective
empirical inhabit matrix.

As a metric the root-mean-square error (rmse) is calculated for the whole matrix.

"""

import os.path

# imports

import pandas as pd

# import numpy as np
from sklearn.metrics import root_mean_squared_error

try:
    import inputs
except ModuleNotFoundError:
    import scripts.inputs as inputs
try:
    import misc
except ModuleNotFoundError:
    import scripts.misc as misc


def inhabit_metrics(inhabit_1_v, inhabit_2_v):
    """
    Calculates a metric between two inhabit matrices

    Args:
        inhabit_1:
        inhabit_2:

    Returns:

    """
    rmse = root_mean_squared_error(inhabit_1_v, inhabit_2_v)
    nrmse = rmse / inhabit_2_v.sum().iloc[0]

    return round(rmse, 2), nrmse


def main(ip):

    load_original = [
        "move",
        "move",
        # "stay_original",
        "inhabit",
    ]
    load_created = [
        "move_in_want_created",
        "move_in_created",
        # "stay_created",
        "inhabit_created",
    ]

    for original, created in zip(load_original, load_created):
        rmse_all_year = pd.Series(name="rmse")
        nrmse_all_years = pd.Series(name="nrmse")

        for yr in range(
            ip["model_start_year"] + 1,
            ip["empirical_end_year"] + 1,
        ):
            inhabit_empirical_v, _ = misc.load_inhabit_moving(
                original,
                yr,
                ip,
                load_abs=False,
            )

            inhabit_modeled_v, _ = misc.load_inhabit_moving(
                created, yr, ip, alt_path=ip["output"]
            )

            # merge to get same ammount of samples
            inhabit_empirical_v = inhabit_empirical_v.merge(
                inhabit_modeled_v,
                on=ip["to_group_col"],
                how="outer",
                suffixes=(None, "_delete_me"),
            )
            inhabit_empirical_v = inhabit_empirical_v.drop(
                ["weights_delete_me"], axis=1
            )
            inhabit_modeled_v = inhabit_modeled_v.merge(
                inhabit_empirical_v,
                on=ip["to_group_col"],
                how="outer",
                suffixes=(None, "_delete_me"),
            )
            inhabit_modeled_v = inhabit_modeled_v.drop(["weights_delete_me"], axis=1)

            inhabit_modeled_v.fillna(0, inplace=True)
            inhabit_empirical_v.fillna(0, inplace=True)

            rmse_all_year.loc[yr], nrmse_all_years.loc[yr] = inhabit_metrics(
                inhabit_empirical_v, inhabit_modeled_v
            )

        metrics_all_years = pd.concat([rmse_all_year, nrmse_all_years], axis=1)
        metrics_all_years.to_csv(
            os.path.join(ip["output"], f"metrics_{original}_vs_{created}.csv")
        )
        print(metrics_all_years)


if __name__ == "__main__":
    ip = inputs.load_inputs()
    main(ip)
