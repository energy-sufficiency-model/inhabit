"""Load dwelling information from SOEP.

Disaggregation:
House type (EFH, MFH, ...) --> Restoration status
--> House Owner --> Number of Rooms
"""

import pandas as pd
import numpy as np

from math import ceil
from random import choice

try:
    import scripts.misc as misc
except ModuleNotFoundError:
    import misc


# @misc.timer_func
def building_type(df, _, col_name):
    """Return building type namings from tabula.

    SFH = Single Family House 1-2 dwellings
    MFH = Multi Family House, 3-12 dwellings
    AB = Apartment Building with 13+ dwellings
    """
    sfh = "SFH"
    mfh = "MFH"
    tabula = misc.get_negative_dict()
    tabula.update(
        {
            1: sfh,  # Farm House
            2: sfh,  # 1-2 Family House
            3: sfh,  # 1-2 Fam.Rowhouse
            4: mfh,  # Apt in 3-4 Unit Bldg
            5: mfh,  # Apt in 5-8 Unit Bldg
            6: mfh,  # Wohnh,9 u.mehr Wohnungen
            7: mfh,  # Hochhaus
            8: mfh,  # other building
            9: np.nan,
        }
    )

    df.replace({col_name: tabula}, inplace=True)
    all_dimensions = {col_name: (sfh, mfh)}
    return df, all_dimensions


# @misc.timer_func
def owner_type(df, ip, col_name):
    """Translate owning status info from int to string
    from hlf0013_h and hgowner.

    hlf0013_h from hl (low data availability)
    1: Communal Dwelling,
    2: Co-Operative Apt.,
    3: Company Apt.,
    4: Private Owner,
    5: Do Not Know,
    6: Private Company,
    7: Non Profit Organization (Church, Foundation, etc.)
    8: NaN

    hgowner
    1: owner
    2: main tenant
    3: sub-tenant
    4: tenant
    5: living in a home (Heim) or shared accomodation
    """
    # translate encoding for "hlf0013_h"

    # translate encoding of "hgowner"
    own = "private owner"
    ten = "private tenant"

    if ip["dwelling_ownership_short"] == "true":
        own_ten = misc.get_negative_dict()
        own_ten.update({1: own, 2: ten, 3: ten, 4: ten, 5: ten})
        # df.hgowner.replace(own_ten, inplace=True)
        df.replace({"hgowner": own_ten}, inplace=True)
        df.rename({"hgowner": col_name}, axis=1, inplace=True)
        # add combined col for hlf0013_h and hgowner, replace private_dwell with hgowner

        all_dimensions = {col_name: (own, ten)}  # without private_dwell!
    else:
        np_dwell = "non profit dwelling"
        # in the next steps private_dwell won't be used for final inhabit-matrix
        private_dwell = "Private Dwelling"  # owner/tenant

        owner = misc.get_negative_dict()
        owner.update(
            {
                1: np_dwell,
                2: np_dwell,
                3: private_dwell,
                4: private_dwell,
                5: np.nan,
                6: private_dwell,
                7: np_dwell,
                8: np.nan,
            }
        )

        df.replace({"hlf0013_h": owner}, inplace=True)

        own_ten = misc.get_negative_dict()
        own_ten.update({1: own, 2: ten, 3: ten, 4: ten, 5: ten})
        # df.hgowner.replace(own_ten, inplace=True)
        df.replace({"hgowner": own_ten}, inplace=True)

        # add combined col for hlf0013_h and hgowner, replace private_dwell with hgowner
        df[col_name] = np.where(
            # copy entry from hlf0013_h for "non-profit dwellings"
            df["hlf0013_h"] == np_dwell,
            np_dwell,
            np.where(
                # for private dwellings: use disaggregation of hgowner
                df["hlf0013_h"] == private_dwell,
                df["hgowner"],
                # for nan-entries in hlf0013_h use entries from hgowner
                np.where(df["hlf0013_h"] == np.nan, df["hgowner"], df["hgowner"]),
            ),
        )
        df = df.drop("hlf0013_h", axis=1)

        all_dimensions = {col_name: (np_dwell, own, ten)}  # without private_dwell!

    return df, all_dimensions


# @misc.timer_func
def house_condition(df, _, col_name):
    r"""Translate house condition information from int to string.

    1: In a good condition --> renovated
    2: Some renovations --> not renovated
    3: Full renovations --> not renovated
    4: Dilapidated --> not renovated
    """

    ren = "renovated"
    not_ren = "not renovated"
    cond = misc.get_negative_dict()
    cond.update({1: ren, 2: not_ren, 3: not_ren, 4: not_ren})

    df.replace({"hgcondit": cond}, inplace=True)
    df.rename({"hgcondit": col_name}, axis=1, inplace=True)

    all_dimensions = {col_name: (ren, not_ren)}
    return df, all_dimensions


# @misc.timer_func
def room_num(df, ip, col_name):
    r"""Limit the number of rooms to 1, 2, 3 and 4+.

    1: 1
    2: 2
    3: 3
    ...
    ip['max_rooms']: ip['max_rooms']+ (e.g. 4+, or 7+)
    ...
    """
    rooms_neg = misc.get_negative_dict()
    rooms = {x: f"{x}" for x in range(1, ip["max_rooms"])}
    rooms.update({x: f"{ip['max_rooms']}+" for x in range(ip["max_rooms"], 16)})
    room_classes = tuple(set(rooms.values()))
    all_dimensions = {col_name: room_classes}

    # this step comes after updating all_dimensions
    rooms.update({x: np.nan for x in range(16, 200)})
    rooms = {**rooms_neg, **rooms}

    # df.hgroom.replace(rooms, inplace=True)
    df.replace({"hgroom": rooms}, inplace=True)
    df.rename({"hgroom": col_name}, axis=1, inplace=True)
    return df, all_dimensions


def region_type(df, ip, col_name):
    """Assign region type to hids."""

    reg_type = misc.get_negative_dict()
    reg_type.update({1: "urban", 2: "rural"})

    # create real name first
    col = "_".join(col_name.split("_")[0:2])

    df = df.replace({col: reg_type})
    df = df.rename({col: col_name}, axis=1)
    all_dimensions = {col_name: ("urban", "rural")}

    return df, all_dimensions


def growth_type(df, ip, col_name):
    """Assign growth type to hids."""
    df_g = pd.read_csv("data/nuts_grow_shrink_urban_rural_questioned.csv")
    quests = df_g.groupby(["urban_rural", "grow_shrink"]).sum(numeric_only=True)[
        ["people_questioned"]
    ]

    regions = list(df["region_type"].unique())  # urban, rural
    grow_shrink = ["growing", "neither", "shrinking"]
    grow_types = {}

    # find out dissemination for growth_type per region_type
    # divide sum of growth_type by sum(hid) for each region_type
    # use as factor for dissemination
    quests_factor = df_g.groupby(["urban_rural"]).sum(numeric_only=True)[
        ["people_questioned"]
    ]

    df_factor = df.groupby(["region_type"])["hid"].count()
    # regions_factor = {}
    for region in regions:
        sum_region_df_g = quests_factor.loc[region].values[0]
        sum_region_df = df_factor.loc[region]
        regions_factor = sum_region_df_g / sum_region_df
        # divide people_questioned per region_type by regions_factor to get number of
        # hid to distribute per grow_types
        grow_types[region] = {}
        for growth in grow_shrink:
            grow_types[region][growth] = ceil(
                quests.loc[region, growth].values[0] / regions_factor
            )

    # iterate through df and assign each row randomly assigned the grow_type.
    # for each assigned grow_type substract from grow_types

    growth_type_df = []
    for row in df.itertuples():
        region = row.region_type
        growth = choice(list(grow_types[region].keys()))

        grow_types[region][growth] -= 1
        if grow_types[region][growth] == 0.0:
            grow_types[region].pop(growth)
        growth_type_df.append(growth)

    df[col_name] = growth_type_df
    all_dimensions = {col_name: tuple(grow_shrink)}
    return df, all_dimensions
