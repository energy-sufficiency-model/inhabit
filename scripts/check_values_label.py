"""Check soep values."""

import pandas as pd
import os


def lookup_value_col(
    file_name,
    search_variable,
    search_column="variable",
    return_column=("label", "value"),
):
    """Search for a value in soep CSV file based on search criteria in a column.

    Parameters:
        - file (str): File path to the CSV file.
        - search_variable (str): Value to search for in the search_column.
        - search_column (str): Name of the column to search in.
        - return_column (str): Name of the column from which the value should be
            returned.

    Returns:
        - value (Series or None): Series with the found values from the return_column,
            None if no value was found.
    """
    file = os.path.join("data", "soep", f"{file_name}_values.csv")
    print(f"find values for {search_variable} in {file}")

    df = pd.read_csv(file)
    value = df.loc[df[search_column] == search_variable, return_column]
    if value.empty:
        return None
    else:
        return value


def lookup_value_row(file_name, search_variable, search_row=0):
    """Check if a value is present in a specific row, e.g. a column name (=variable)."""
    file = os.path.join("data", "soep", f"{file_name}.csv")
    print(f"Check if {search_variable} is in {file}")

    row = pd.read_csv(file, nrows=search_row).columns
    if search_variable in row:
        print(
            f"Yes! The row number {search_row} in {file_name} DOES contains\
                 {search_variable}."
        )
    else:
        print(
            f"No! The row number {search_row} in {file_name} does NOT contain\
                 {search_variable}."
        )


# change these parameters
def main():
    file_name = "hgen"
    search_variable = "hgcondit"
    row_or_col = "col"

    if row_or_col == "col":
        result = lookup_value_col(file_name, search_variable)
        if result is not None:
            print(result)
        else:
            print("Kein Wert gefunden.")
    elif row_or_col == "row":
        lookup_value_row(file_name, search_variable)


if __name__ == "__main__":
    main()
