"""
Define functions to count households by income, household type, age and household size.

Functions definded in this script:
- Load data on person level
- Define changes in disaggregagtion of different SOEP variables
and replace encoding
- Calculate income quintiles
- Caclulate weighted shares for one variable (e.g. income)

Disaggregation of heouseholds in inhabit matrix:
Region --> Income Quantile --> household type --> age --> number of people

Functions are applied in "inhabit_matrix.py".
"""

import numpy as np
import pandas as pd

try:
    import scripts.misc as misc
except ModuleNotFoundError:
    import misc

# for weighting
# from statsmodels.stats.weightstats import DescrStatsW


def household_types(df, ip, col_name):
    """Replace encoding of household types with own encoding.

    Merge "single parent" (value 3) or "couple" with children" (values 4-6)
    merge "multiple generation-HH" and "other combination"
    1:	1-Pers.-HH	single
    2:  Couple Without Children	couple_no_child
    3:	Single Parent	single_parent
    4:	Couple With Children LE 16	couple_parent
    5:	Couple With Children GT 16	couple_parent
    6:	Couple With Children LE And GT 16   couple_parent
    7:	Multiple Generation-HH	other
    8:	Other Combination	other
    """
    if ip["household_classification"] == "children":
        children = "with_children"
        no_children = "without_children"
        other = "other"
        to_replace = misc.get_negative_dict()
        to_replace.update(
            {
                1: no_children,  # single,
                2: no_children,  # cnc,
                3: children,  # single_parent,
                4: children,  # couple,
                5: children,  # couple,
                6: children,  # couple,
                7: no_children,  # other,
                8: no_children,  # other
            }
        )
        all_dimensions = {col_name: (children, no_children)}
    else:
        if ip["household_classification"] != "types":
            print(
                "Invalid entry in Input sheet 'household_classification'. \
            Using household_type by default"
            )
        single = "single"
        single_parent = "single_parent"
        cnc = "couple_no_children"
        couple = "couple_parent"
        other = "other"
        to_replace = misc.get_negative_dict()
        to_replace.update(
            {
                1: single,
                2: cnc,
                3: single_parent,
                4: couple,
                5: couple,
                6: couple,
                7: other,
                8: other,
            }
        )
        all_dimensions = {col_name: (single, single_parent, cnc, couple, other)}

    # df[col_name].replace(to_replace, inplace=True)
    df.replace({col_name: to_replace}, inplace=True)

    # all_dimensions = {col_name: (single, single_parent, cnc, couple, other)}

    return df, all_dimensions


def size_class(df, ip, col_name):
    """Assign Household size class to every person."""
    # Attach from hbrutto
    # hhgr: household size
    hh_classes_neg = misc.get_negative_dict()
    hh_classes = {}
    all_dimensions = {col_name: [-1.0]}
    hh_classes.update({x: f"{x}" for x in range(1, ip['max_hh_size'])})
    hh_classes.update({x: f"{ip['max_hh_size']}+" for x in range(ip['max_hh_size'], 100)})

    dim_classes = tuple(set(hh_classes.values()))
    all_dimensions = {col_name: dim_classes}

    hh_classes = {**hh_classes_neg, **hh_classes}
    # df[col_name].replace(hh_classes, inplace=True)
    df.replace({col_name: hh_classes}, inplace=True)

    return df, all_dimensions


def age_class(df, ip, col_name):
    """Assign age classes to every person."""

    age_classes_neg = misc.get_negative_dict()
    age_classes = misc.get_age_classes(ip)
    dim_classes = tuple(set(age_classes.values()))
    all_dimensions = {col_name: dim_classes}
    age_classes = {**age_classes_neg, **age_classes}

    # df["sage"].replace(age_classes, inplace=True)
    df.replace({"sage": age_classes}, inplace=True)
    df.rename({"sage": col_name}, axis=1, inplace=True)

    return df, all_dimensions


def income_quintiles(df, ip, col_name):
    r"""Calculate income quintiles for df and add them in \"income_quintile\" column.

    - q1: the quintil with least income
    - q2
    - q3
    - q4
    - q5: the quintil with highest income
    """
    misc.check_empty(df, "hh_income_quintiles")
    # Calculate of quintiles by sorting inhabits by income and divide the number of
    # inhabits by 5
    df = df.sort_values(by=["hghinc"], kind="mergesort", axis=0, ascending=True)

    def weighted_qcut(values, weights, q):
        "Return weighted quantile cuts from a given series, values."
        # 0-0.2, 0.2-0.4, 0.4-0.6, 0.6-0.8, 0.8-1 --> five segments
        quantiles = np.linspace(0, 1, q + 1)
        labels = ["q1", "q2", "q3", "q4", "q5"]
        # order: cumulative sum of weights over sorted income
        order = weights.iloc[values.argsort()].cumsum()
        # order/order[-1] is the percentage of each weight sorted by income
        # sorts the orders to the previously defined quantiles and labels them.
        bins = pd.cut(order / order.iloc[-1], quantiles, labels=labels)
        return bins.sort_index()

    df[col_name] = weighted_qcut(df["hghinc"], df["weights"], 5)

    all_dimensions = {col_name: ("q5", "q4", "q3", "q2", "q1")}
    return df, all_dimensions
