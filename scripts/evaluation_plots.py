import os
import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

try:
    import inputs
except ModuleNotFoundError:
    import scripts.inputs as inputs

GREY_LIST = ["silver", "darkgrey", "grey", "dimgrey"]  # 'gainsboro', 'lightgrey',
COLOR_LIST = [
    "lightsalmon",
    "yellowgreen",
    "darkcyan",
    "darkviolet",
    "mediumspringgreen",
    "firebrick",
    "navajowhite",
]
# NO_OF_ROOMS_LIST = ["1", "2", "3", "4", "5", "6", "7+"]
QUINTILES = [f"q{n+1}" for n in range(5)]
DWELLING_TYPES = ["SFH", "MFH"]


def arange_room_nos_list(max_rooms):
    list = [f"{i+1}" for i in range(max_rooms - 1)]
    list.append(f"{max_rooms}+")
    return list


def load_inhabit_matrix_from_csv(file, ip, empirical=False):
    length = len(ip["cols_dwell"] + ip["cols_hh"])
    length += 2 if empirical else 0

    inhabit = pd.read_csv(os.path.join(file), index_col=[i for i in range(length)])
    return inhabit


def check_folder(folder_name):
    folder_path = os.path.join(ip["output"], folder_name)

    if os.path.exists(folder_path) and os.path.isdir(folder_path):
        if os.path.exists(os.path.join(folder_path, "mor_2002_2019.csv")):
            return 1
    else:
        fol = ip["output"]
        print(
            f"Fehler: Der Ordner '{folder_name}' existiert nicht im Basisordner \
                '{fol}'."
        )
        return 0


def single_plots(
    large_movers_df, base_folder, folder_name, years_empirical, years_model, years
):

    filtered_df = large_movers_df[large_movers_df["what"] == "regression"]
    for row_no in range(len(filtered_df.iloc[0:40])):
        row = pd.Series(filtered_df.iloc[row_no])

        df_row_movers = pd.DataFrame(index=years)
        # for loop empirical
        for year in range(years_empirical[0], years_empirical[1] + 1):
            # leave out zero-parts to better understand regression curves
            # if row[f"weights_{year}"] > 0:
            df_row_movers.loc[year, "empirical"] = row[f"move_per_pop_{year}"]

        # for loop modeled
        # changed years_model with years_empirical +1
        for year in range(years_empirical[0], years_model[1]):
            df_row_movers.loc[year, "modeled"] = row[f"move_per_pop_{year}_reg"]
            # if row[f"weights_{year}"] > 0:
            df_row_movers.loc[year, "empirical"] = row[f"move_per_pop_{year}"]
        df_row_movers.plot(title=str(row.name))
        plt.savefig(os.path.join(base_folder, folder_name, f"row_plot_{str(row.name)}"))
        plt.close()
    return


def single_plots_dis(
    large_movers_df, base_folder, folder_name, dis, years_empirical, years_model, years
):
    for idx, row in enumerate(large_movers_df.itertuples()):
        df_row_movers = pd.DataFrame(index=years)
        # for loop empirical
        for year in range(years_empirical[0], years_empirical[1] + 1):
            # leave out zero-parts to better understand regression curves
            # if row[f"weights_{year}"] > 0:
            df_row_movers.loc[year, "empirical"] = getattr(
                row, f'{ip["col_move_per_pop"]}_{year}'
            )

        # for loop modeled
        # changed years_model with years_empirical +1
        for year in range(years_empirical[0], years_model[1]):
            df_row_movers.loc[year, "modeled"] = getattr(
                row, f'{ip["col_move_per_pop"]}_{year}_reg'
            )
            # if row[f"weights_{year}"] > 0:
            df_row_movers.loc[year, "empirical"] = getattr(
                row, f'{ip["col_move_per_pop"]}_{year}'
            )
        df_row_movers.plot(title=f"{dis}_{getattr(row, dis)}")
        plt.savefig(os.path.join(base_folder, folder_name, f"row_plot_{dis}_{idx}"))
        plt.close()
    return


def summed_plots(
    large_movers_df, base_folder, folder_name, years_empirical, years_model, years
):
    df_summed_movers = pd.DataFrame(index=years)
    # for loop empirical
    # for year in range(years_empirical[0], years_empirical[1] + 1):
    #    df_summed_movers.loc[year, "empirical"] = large_movers_df[
    #        f"weights_{year}"
    #    ].sum()

    # for loop modeled
    # changed years_model with years_empirical +1
    for year in range(years_empirical[0], years_model[1] + 1):
        df_summed_movers.loc[year, "modeled_all"] = large_movers_df[
            f"move_per_pop_{year}_reg"
        ].sum()
        df_summed_movers.loc[year, "modeled_regression"] = large_movers_df.loc[
            large_movers_df["what"] == "regression", f"move_per_pop_{year}_reg"
        ].sum()
        df_summed_movers.loc[year, "modeled_not_enogh_samples"] = large_movers_df.loc[
            large_movers_df["what"] == "not_enogh_samples", f"move_per_pop_{year}_reg"
        ].sum()
        df_summed_movers.loc[year, "modeled_not_enough_obs"] = large_movers_df.loc[
            large_movers_df["what"] == "not_enough_obs", f"move_per_pop_{year}_reg"
        ].sum()
        df_summed_movers.loc[year, "empirical"] = large_movers_df[
            f"move_per_pop_{year}"
        ].sum()
    df_summed_movers.plot(title="summed move_out_rates", ylim=(0, 500))
    plt.savefig(os.path.join(base_folder, folder_name, "summed_mors.png"))
    plt.close()


def summed_plots_dis(
    large_movers_df, base_folder, folder_name, dis, years_empirical, years_model, years
):
    df_summed_movers = pd.DataFrame(index=years)
    for year in range(years_empirical[0], years_model[1] + 1):

        df_summed_movers.loc[year, "modeled_all"] = large_movers_df[
            f"move_per_pop_{year}_reg"
        ].sum()
        df_summed_movers.loc[year, "empirical"] = large_movers_df[
            f"move_per_pop_{year}"
        ].sum()
    df_summed_movers.plot(title=f"summed move_out_rates_{dis}", ylim=(0, 0.4))
    plt.savefig(os.path.join(base_folder, folder_name, f"summed_mors_{dis}.png"))
    plt.close()


def plot_from_various_files(ip, folder_name, years_empirical, years_model, years):
    # load empirical and calculated files into one
    all_mor = None
    for idx, year in enumerate(years):
        emp_move = os.path.join(
            ip["output"], folder_name, f"empirical_movers_{year}.csv"
        )
        reg_mor = os.path.join(ip["output"], folder_name, f"mor_{year}.csv")
        inh_before = os.path.join(
            ip["evidence_folder"], "inhabit", f"inhabit_{year-1}.csv"
        )
        if idx == 0:
            all_mor = pd.read_csv(emp_move)
        elif year >= years_model[0]:
            all_mor = all_mor.merge(
                pd.read_csv(emp_move),
                on=ip["to_group_col"],
            )
            all_mor = all_mor.merge(
                pd.read_csv(reg_mor),
                on=ip["to_group_col"],
            )
            all_mor = all_mor.merge(
                pd.read_csv(inh_before),
                on=ip["to_group_col"],
            )
            all_mor[f"weights_{year}_reg"] = (
                all_mor["weights"] * all_mor["move_per_pop"]
            )
            all_mor = all_mor.drop(["weights", "move_per_pop"], axis=1)
        else:
            all_mor = all_mor.merge(
                pd.read_csv(emp_move),
                on=ip["to_group_col"],
            )

    all_mor.rename(
        columns={ip["col_move_per_pop"]: f'{ip["col_move_per_pop"]}_{years[0]}'},
        inplace=True,
    )
    df_summed_movers = pd.DataFrame(index=years)
    for year in range(years_empirical[0], years_model[1] + 1):
        # all_mor.loc[
        #    all_mor[f'{ip["col_move_per_pop"]}_{year}'] < ip["minimal_move_out_rate"]
        # ] = ip["minimal_move_out_rate"]

        if year >= years_model[0]:
            df_summed_movers.loc[year, "regression"] = all_mor[
                f"weights_{year}_reg"
            ].sum()
        df_summed_movers.loc[year, "empirical"] = all_mor[f"weights_{year}"].sum()
    df_summed_movers.plot(title="summed movers")
    plt.savefig(os.path.join(ip["output"], folder_name, "summed_movers_all.png"))
    plt.close()


def check_allocation(ip):
    check = [
        ("inhabit", "inhabit"),
        ("move", "move_in_want"),
        ("move", "move_in"),
        # ("stay", "stay"),
    ]

    for a, b in check:
        df_ori, df_crea = None, None
        for year in range(ip["empirical_start_year"], ip["target_year"] + 1):
            original = os.path.join(ip["evidence_folder"], a, f"{a}_{year}.csv")
            created = os.path.join(
                ip["output"], ip["alloc_output_path"], f"{b}_created_{year}.csv"
            )
            if year == ip["empirical_start_year"]:
                df_ori = pd.read_csv(original, index_col=ip["to_group_col"])
            elif year <= ip["empirical_end_year"]:
                df_ori = df_ori.merge(
                    pd.read_csv(original, index_col=ip["to_group_col"]),
                    on=ip["to_group_col"],
                    suffixes=[None, f"_{year}"],
                    how="outer",
                )
            if year == ip["model_start_year"] + 1:
                df_crea = pd.read_csv(created, index_col=ip["to_group_col"])
            elif year > ip["model_start_year"] + 1:
                df_crea = df_crea.merge(
                    pd.read_csv(created, index_col=ip["to_group_col"]),
                    on=ip["to_group_col"],
                    suffixes=[None, f"_{year}"],
                    how="outer",
                )

        df_ori.rename(
            columns={
                ip["col_weights"]: f'{ip["col_weights"]}_{ip["empirical_start_year"]}'
            },
            inplace=True,
        )
        df_crea.rename(
            columns={
                ip["col_weights"]: f'{ip["col_weights"]}_{ip["model_start_year"]+1}'
            },
            inplace=True,
        )

        years = np.arange(ip["empirical_start_year"], ip["target_year"] + 1)
        df_summed_movers = pd.DataFrame(index=years)
        for year in range(ip["empirical_start_year"], ip["target_year"] + 1):
            if year <= ip["empirical_end_year"]:
                df_summed_movers.loc[year, "empirical"] = df_ori[
                    f"{ip['col_weights']}_{year}"
                ].sum()
            if year >= ip["model_start_year"] + 1:
                df_summed_movers.loc[year, "created"] = df_crea[
                    f"{ip['col_weights']}_{year}"
                ].sum()

        df_summed_movers.plot(title=f"summed allocations_{b}")
        plt.savefig(
            os.path.join(
                ip["output"], ip["alloc_output_path"], f"summed_allocations_{b}.png"
            )
        )
        plt.close()


def aggregation_plots(ip, default_output_folder):
    # plt.style.use('bmh')
    ip_default = inputs.load_inputs()
    years_dict = {
        "original": (2002, ip["target_year"]),
        "created": (ip["model_start_year"], ip["target_year"]),
    }
    plot_years = 2002, ip["target_year"]
    room_nos_list = arange_room_nos_list(ip["max_rooms"])
    room_nos_list_default = arange_room_nos_list((ip_default["max_rooms"]))
    hh_types = ["with_children", "without_children"]
    # create year vector
    years = np.arange(years_dict["original"][0], years_dict["original"][1] + 1)
    test = False
    if test:
        base_folder = "/home/pool/mod/inhabit/"
    else:
        base_folder = os.path.join(ip["output"], ip["alloc_output_path"])

    default_output_folder = os.path.join("output/default", default_output_folder)

    # df_summed_movers = pd.DataFrame(index=years)

    # --------aggregate results---------
    def aggregate_results(base_folder, ip, default=False):
        results = {
            "original": {},
            "created": {},
        }
        room_nos_list = arange_room_nos_list(ip["max_rooms"])
        for result_type in ["original", "created"]:
            if result_type == "created" and default:
                years = np.arange(
                    2011, years_dict[result_type][1] + 1
                )  # TODO besser lösen hier nur für schneller Debugging
            else:
                years = np.arange(
                    years_dict[result_type][0], years_dict[result_type][1] + 1
                )

            for year in years:
                if result_type == "original":
                    inhabit_matrix = load_inhabit_matrix_from_csv(
                        os.path.join(
                            ip["evidence_folder"],
                            ip["inhabit_path"],
                            f"inhabit_{year}.csv",
                        ),
                        ip,
                    )
                else:
                    inhabit_matrix = load_inhabit_matrix_from_csv(
                        os.path.join(base_folder, f"inhabit_{result_type}_{year}.csv"),
                        ip,
                    )
                results[result_type][year] = {}
                for quintile in range(1, 6):

                    quin_aggregation = inhabit_matrix.xs(f"q{quintile}", level=8)
                    # results['empirical']['']
                    results[result_type][year][quintile] = {}
                    for dwell_type in ["MFH", "SFH"]:
                        quin_dwell_aggregation = quin_aggregation.xs(
                            dwell_type, level=0
                        )
                        results[result_type][year][quintile][
                            dwell_type
                        ] = quin_dwell_aggregation.sum()
                    for no_of_rooms in room_nos_list:
                        quin_room_aggregation = quin_aggregation.xs(
                            no_of_rooms, level=3
                        )
                        results[result_type][year][quintile][
                            no_of_rooms
                        ] = quin_room_aggregation.sum()
                for hh_type in hh_types:
                    type_aggregation = inhabit_matrix.xs(hh_type, level=5)
                    results[result_type][year][hh_type] = {}
                    for dwell_type in ["MFH", "SFH"]:
                        results[result_type][year][hh_type][dwell_type] = (
                            type_aggregation.xs(dwell_type, level=0).sum()
                        )
                    for no_of_rooms in room_nos_list:
                        results[result_type][year][hh_type][no_of_rooms] = (
                            type_aggregation.xs(no_of_rooms, level=3).sum()
                        )
        return results

    results = aggregate_results(base_folder, ip)
    results_default = aggregate_results(default_output_folder, ip_default, default=True)
    # ---------Plots---------

    result_type_rename = {"original": "empirical", "created": "modeled"}

    # - - - - - - - HH Type aggregation plots - - - - - - -
    # ------ HH type vs. dwell_type -------
    fig_hh_type, axes_hh_type = plt.subplots(2, 2, figsize=(18, 6))
    fig_hh_type.subplots_adjust(right=0.55, top=0.8)
    fig_hh_type.suptitle("Household type vs. Dwelling type", x=0.35, y=0.98, size=20)
    fig_hh_type.text(s="Default Setting", x=0.18, y=0.88, fontsize=15)
    fig_hh_type.text(s="Changed Setting", x=0.4, y=0.88, fontsize=15)
    excel_writer = pd.ExcelWriter(
        os.path.join(ip["output"], "Aggregated_data_hh_type-dwell_type.xlsx")
    )

    for number, hh_type in enumerate(hh_types):
        years = range(
            min(years_dict["original"][0], years_dict["original"][0]),
            max(years_dict["original"][1], years_dict["original"][1]),
        )
        dwell_data_dict = {
            "original": pd.DataFrame(
                index=range(years_dict["original"][0], years_dict["original"][1])
            ),
            "created": pd.DataFrame(
                index=range(years_dict["original"][0], years_dict["original"][1])
            ),
        }
        dwell_data_dict_default = {
            "original": pd.DataFrame(
                index=range(years_dict["original"][0], years_dict["original"][1])
            ),
            "created": pd.DataFrame(
                index=range(years_dict["original"][0], years_dict["original"][1])
            ),
        }
        for result_type in ["original", "created"]:
            years = np.arange(
                years_dict[result_type][0], years_dict[result_type][1] + 1
            )
            for year in years:
                # print(f'processing:{year}')
                for dwell_type in ["MFH", "SFH"]:
                    if result_type == "original" or year >= 2011 and "created":
                        dwell_data_dict_default[result_type].loc[
                            year, dwell_type + f" {result_type_rename[result_type]}"
                        ] = results_default[result_type][year][hh_type][
                            dwell_type
                        ].iloc[
                            0
                        ]

                    dwell_data_dict[result_type].loc[
                        year, dwell_type + f" {result_type_rename[result_type]}"
                    ] = results[result_type][year][hh_type][dwell_type].iloc[0]
        pd.concat(
            [dwell_data_dict["original"], dwell_data_dict["created"]], axis=1
        ).to_excel(excel_writer=excel_writer, sheet_name=f"{hh_type}")

        dwell_data_dict["original"].plot(
            ax=axes_hh_type[number, 1],
            legend=False,
            linestyle="-.",
            sharex=True,
            title=hh_type,
            color=COLOR_LIST,
        )
        dwell_data_dict["created"].plot(
            ax=axes_hh_type[number, 1], legend=False, linestyle="-", color=COLOR_LIST
        )
        dwell_data_dict_default["original"].plot(
            ax=axes_hh_type[number, 0],
            legend=False,
            linestyle="-.",
            sharex=True,
            title=f"{hh_type}",
            color=COLOR_LIST,
        )
        dwell_data_dict_default["created"].plot(
            ax=axes_hh_type[number, 0], legend=False, linestyle="-", color=COLOR_LIST
        )

        axes_hh_type[0, 1].legend(loc="upper right", bbox_to_anchor=(1.5, 1))
        for column in range(2):
            axes_hh_type[number, column].set_xticks(
                [i for i in range(plot_years[0], plot_years[1], 3)]
            )
            axes_hh_type[number, column].set_xlim(plot_years)
            axes_hh_type[number, column].grid(visible=True)

    excel_writer.close()
    plt.savefig(
        os.path.join(
            ip["output"],
            ip["alloc_output_path"],
            "plot_household-dwelling_aggregation.png",
        ),
        bbox_inches="tight",
    )
    plt.close()

    # ------ HH type vs. room_no ------
    fig_hh_type, axes_hh_type = plt.subplots(2, 2, figsize=(18, 6))
    fig_hh_type.subplots_adjust(right=0.55, top=0.8)
    fig_hh_type.suptitle("Household type vs. No of rooms", x=0.35, y=0.98, size=20)
    fig_hh_type.text(s="Default Setting", x=0.18, y=0.88, fontsize=15)
    fig_hh_type.text(s="Changed Setting", x=0.4, y=0.88, fontsize=15)
    excel_writer = pd.ExcelWriter(
        os.path.join(ip["output"], "Aggregated_data_hh_type-room_no.xlsx")
    )

    for number, hh_type in enumerate(hh_types):
        years = range(
            min(years_dict["original"][0], years_dict["original"][0]),
            max(years_dict["original"][1], years_dict["original"][1]),
        )
        room_data_dict = {
            "original": pd.DataFrame(
                index=range(years_dict["original"][0], years_dict["original"][1])
            ),
            "created": pd.DataFrame(
                index=range(years_dict["original"][0], years_dict["original"][1])
            ),
        }
        room_data_dict_default = {
            "original": pd.DataFrame(
                index=range(years_dict["original"][0], years_dict["original"][1])
            ),
            "created": pd.DataFrame(
                index=range(years_dict["original"][0], years_dict["original"][1])
            ),
        }
        for result_type in ["original", "created"]:
            years = np.arange(
                years_dict[result_type][0], years_dict[result_type][1] + 1
            )
            for year in years:
                for no_of_rooms in room_nos_list:
                    room_data_dict[result_type].loc[
                        year, no_of_rooms + f" {result_type_rename[result_type]}"
                    ] = results[result_type][year][hh_type][no_of_rooms].iloc[0]
                    room_data_dict_default[result_type].loc[
                        year, no_of_rooms + f" {result_type_rename[result_type]}"
                    ] = results[result_type][year][hh_type][no_of_rooms].iloc[0]
        pd.concat(
            [room_data_dict["original"], room_data_dict["created"]], axis=1
        ).to_excel(excel_writer=excel_writer, sheet_name=f"{hh_type}")
        room_data_dict_default["original"].plot(
            legend=False,
            ax=axes_hh_type[number, 0],
            linestyle="-.",
            sharex=True,
            title=hh_type,
            color=COLOR_LIST,
        )
        room_data_dict_default["created"].plot(
            legend=False, ax=axes_hh_type[number, 0], linestyle="-", color=COLOR_LIST
        )
        room_data_dict["original"].plot(
            legend=False,
            ax=axes_hh_type[number, 1],
            linestyle="-.",
            sharex=True,
            title=hh_type,
            color=COLOR_LIST,
        )
        room_data_dict["created"].plot(
            legend=False, ax=axes_hh_type[number, 1], linestyle="-", color=COLOR_LIST
        )

        axes_hh_type[0, 1].legend(loc="upper right", bbox_to_anchor=(1.5, 1))
        for column in range(2):
            axes_hh_type[number, column].set_xlim(plot_years)
            axes_hh_type[number, column].set_xticks(
                [i for i in range(plot_years[0], plot_years[1], 3)]
            )
            axes_hh_type[number, column].grid(visible=True)

    excel_writer.close()
    plt.savefig(
        os.path.join(
            ip["output"],
            ip["alloc_output_path"],
            "plot_household-rooms_aggregation.png",
        ),
        bbox_inches="tight",
    )
    plt.close()

    # - - - - - - - Quintile aggregation plot - - - - - - -
    # ------ quintile vs dwelling type plots ------
    fig_quintile, axes_quintile = plt.subplots(5, 2, figsize=(18, 12))
    fig_quintile.subplots_adjust(right=0.55, top=0.9)
    fig_quintile.suptitle("Quintile vs. Dwelling type", x=0.35, y=0.98, size=20)
    fig_quintile.text(s="Default Setting", x=0.18, y=0.93, fontsize=15)
    fig_quintile.text(s="Changed Setting", x=0.4, y=0.93, fontsize=15)
    excel_writer = pd.ExcelWriter(
        os.path.join(ip["output"], "Aggregated_data_quintile-dwell_type.xlsx")
    )
    for quintile in range(1, 6):
        dwell_data_dict_default = {
            "original": pd.DataFrame(
                index=range(years_dict["original"][0], years_dict["original"][1])
            ),
            "created": pd.DataFrame(
                index=range(years_dict["original"][0], years_dict["original"][1])
            ),
        }
        dwell_data_dict = {
            "original": pd.DataFrame(
                index=range(years_dict["original"][0], years_dict["original"][1])
            ),
            "created": pd.DataFrame(
                index=range(years_dict["original"][0], years_dict["original"][1])
            ),
        }
        for result_type in ["original", "created"]:
            years = np.arange(
                years_dict[result_type][0], years_dict[result_type][1] + 1
            )
            for year in years:
                for dwell_type in ["MFH", "SFH"]:
                    if result_type == "original" or year >= 2011 and "created":
                        dwell_data_dict_default[result_type].loc[
                            year, dwell_type + f" {result_type_rename[result_type]}"
                        ] = results_default[result_type][year][quintile][
                            dwell_type
                        ].iloc[
                            0
                        ]
                    dwell_data_dict[result_type].loc[
                        year, dwell_type + f" {result_type_rename[result_type]}"
                    ] = results[result_type][year][quintile][dwell_type].iloc[0]
        pd.concat(
            [dwell_data_dict["original"], dwell_data_dict["created"]], axis=1
        ).to_excel(excel_writer=excel_writer, sheet_name=f"q{quintile}")
        dwell_data_dict_default["original"].plot(
            ax=axes_quintile[quintile - 1, 0],
            legend=False,
            linestyle="-.",
            sharex=True,
            title=f"Quintile q{quintile}",
            color=COLOR_LIST,
        )
        dwell_data_dict_default["created"].plot(
            ax=axes_quintile[quintile - 1, 0],
            legend=False,
            linestyle="-",
            color=COLOR_LIST,
        )
        dwell_data_dict["original"].plot(
            ax=axes_quintile[quintile - 1, 1],
            legend=False,
            linestyle="-.",
            sharex=True,
            title=f"Quintile q{quintile}",
            color=COLOR_LIST,
        )
        dwell_data_dict["created"].plot(
            ax=axes_quintile[quintile - 1, 1],
            legend=False,
            linestyle="-",
            color=COLOR_LIST,
        )

        axes_quintile[0, 1].legend(loc="upper right", bbox_to_anchor=(1.5, 1))
        for column in range(2):
            axes_quintile[quintile - 1, column].set_xlim(plot_years)
            axes_quintile[quintile - 1, column].set_xticks(
                [i for i in range(plot_years[0], plot_years[1], 3)]
            )
            axes_quintile[quintile - 1, column].grid(visible=True)
    excel_writer.close()
    plt.savefig(
        os.path.join(
            ip["output"],
            ip["alloc_output_path"],
            "plot_quintile-dwelling_aggregation.png",
        ),
        bbox_inches="tight",
    )
    plt.close()

    # ------ Quintile vs rooms ------
    # quintile vs plots
    fig_quintile, axes_quintile = plt.subplots(5, 2, figsize=(18, 12))
    fig_quintile.subplots_adjust(right=0.55, top=0.9)
    fig_quintile.suptitle("Quintile vs. Dwelling type", x=0.35, y=0.98, size=20)
    fig_quintile.text(s="Default Setting", x=0.18, y=0.93, fontsize=15)
    fig_quintile.text(s="Changed Setting", x=0.4, y=0.93, fontsize=15)
    excel_writer = pd.ExcelWriter(
        os.path.join(ip["output"], "Aggregated_data_quintile-room_no.xlsx")
    )

    for quintile in range(1, 6):
        years = range(
            min(years_dict["original"][0], years_dict["original"][0]),
            max(years_dict["original"][1], years_dict["original"][1]),
        )
        room_data_dict_default = {
            "original": pd.DataFrame(
                index=range(years_dict["original"][0], years_dict["original"][1])
            ),
            "created": pd.DataFrame(
                index=range(years_dict["original"][0], years_dict["original"][1])
            ),
        }
        room_data_dict = {
            "original": pd.DataFrame(
                index=range(years_dict["original"][0], years_dict["original"][1])
            ),
            "created": pd.DataFrame(
                index=range(years_dict["original"][0], years_dict["original"][1])
            ),
        }
        for result_type in ["original", "created"]:
            years = np.arange(
                years_dict[result_type][0], years_dict[result_type][1] + 1
            )
            for year in years:
                # print(f'processing:{year}')
                if result_type == "original" or year >= 2011 and "created":
                    for no_of_rooms in room_nos_list_default:
                        room_data_dict_default[result_type].loc[
                            year, no_of_rooms + f" {result_type_rename[result_type]}"
                        ] = results_default[result_type][year][quintile][
                            no_of_rooms
                        ].iloc[
                            0
                        ]
                for no_of_rooms in room_nos_list:
                    room_data_dict[result_type].loc[
                        year, no_of_rooms + f" {result_type_rename[result_type]}"
                    ] = results[result_type][year][quintile][no_of_rooms].iloc[0]

        pd.concat(
            [room_data_dict["original"], room_data_dict["created"]], axis=1
        ).to_excel(excel_writer=excel_writer, sheet_name=f"q{quintile}")
        room_data_dict_default["original"].plot(
            legend=False,
            ax=axes_quintile[quintile - 1, 0],
            linestyle="-.",
            sharex=True,
            title=f"Quintile q{quintile}",
            color=COLOR_LIST,
        )
        room_data_dict_default["created"].plot(
            legend=False,
            ax=axes_quintile[quintile - 1, 0],
            linestyle="-",
            color=COLOR_LIST,
        )

        room_data_dict["original"].plot(
            legend=False,
            ax=axes_quintile[quintile - 1, 1],
            linestyle="-.",
            sharex=True,
            title=f"Quintile q{quintile}",
            color=COLOR_LIST,
        )
        room_data_dict["created"].plot(
            legend=False,
            ax=axes_quintile[quintile - 1, 1],
            linestyle="-",
            color=COLOR_LIST,
        )

        axes_quintile[0, 1].legend(loc="upper right", bbox_to_anchor=(1.5, 1))
        for column in range(2):
            axes_quintile[quintile - 1, column].set_xlim(plot_years)
            axes_quintile[quintile - 1, column].set_xticks(
                [i for i in range(plot_years[0], plot_years[1], 3)]
            )
            axes_quintile[quintile - 1, column].grid(visible=True)

    excel_writer.close()
    plt.savefig(
        os.path.join(
            ip["output"],
            ip["alloc_output_path"],
            "plot_quintile-rooms_aggregation.png",
        ),
        bbox_inches="tight",
    )
    plt.close()
    # TODO's:
    """
    """


if __name__ == "__main__":
    # Prüfen, ob das Skript mit einem Argument aufgerufen wurde
    test = True
    if test:
        folder_name = "move_out_rate/"  # "figure_test"
    else:
        if len(sys.argv) != 2:
            print("Fehler: Bitte geben Sie einen Ordnernamen als Argument an.")
            sys.exit(1)

        folder_name = sys.argv[1]

    ip = inputs.load_inputs()

    # check_allocation(ip)

    # exit(1)
    ip["alloc_output_path"] = "alloc_rate_iteration_80, 20/alloc_out"
    aggregation_plots(ip, "alloc_out_default")
    # aggregation_plots(ip)

    """disaggs = [
        "age",
        "building_type",
        "hh_size",
        "income_quintile",
        "hh_size",
        "region_type_dwell",
    ]
    # define start/stop years as tuples
    years_empirical = ip["empirical_start_year"], ip["model_start_year"] - 1
    years_model = ip["model_start_year"], ip["target_year"]

    # create year vector
    years = np.arange(years_empirical[0], years_model[1] + 1)
    for dis in disaggs:
        large_movers_file = os.path.join(
            ip["output"],
            folder_name,
            f"mor_{years_empirical[0]}_{years_model[-1]}_{dis}.csv",
        )
        large_movers_df = pd.read_csv(large_movers_file)
        summed_plots_dis(
            large_movers_df,
            folder_name,
            dis,
            years_empirical,
            years_model,
            years,
        )
        single_plots_dis(
            large_movers_df,
            folder_name,
            dis,
            years_empirical,
            years_model,
            years,
        )

    plot_from_various_files(ip, folder_name, years_empirical, years_model, years)
    # large_movers_df = load_inhabit_matrix_from_csv(large_movers_file, ip)

    # summed_plots(large_movers_df, folder_name)
    # single_plots(large_movers_df, folder_name)"""
