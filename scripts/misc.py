"""Define several global variables, paths and basic functions."""

import os
import numpy as np
import pandas as pd
from time import time
import seaborn as sns
import matplotlib.pyplot as plt
import pprint


def save_params_to_file(ip):
    file = os.path.join(ip["output"], "input_parameters.txt")
    with open(file, "w") as f:
        f.write(pprint.pformat(ip, indent=4, sort_dicts=False))


def check_absolute_path(path):
    """Checks if the current working directory is inhabit. If yes Path is kept,
    if not '../' is added to the path"""
    if "inhabit" in os.getcwd().split("/")[-1]:
        return path
    else:
        return os.path.join("../" + path)


def get_save_path(folder1, folder2):
    save_path = check_absolute_path(os.path.join(folder1, folder2))
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    return save_path


def get_negative_dict():
    """Create NaN entries for range(-8, 0) entries."""
    return {i: np.nan for i in range(-8, 1)}


def heatmap(data, year, save_path, ip, heat_name="move_out_rate"):
    """Create heatmaps of move_out_rates."""
    # first prepare dataframe by dropping double columns
    delete_col = "region_type_hh"
    group_col = [x for x in ip["to_group_col"] if x != delete_col]
    data = data.reset_index()
    data = data.drop(delete_col, axis=1)
    data = data.groupby(by=group_col).sum(numeric_only=True)
    # unstack to get matrix-form
    data = data.unstack(level=ip["cols_dwell"])
    plt.figure(figsize=(20, 16))
    # TODO: change in a way that impossible configurations are not to be seen
    x_axis_labels = data.columns  # labels for x-axis
    y_axis_labels = data.index  # labels for y-axis
    sns.set(font_scale=0.5)
    sns.heatmap(
        data,
        annot=False,
        fmt=".1f",
        cbar=True,
        # cmap=sns.cubehelix_palette(as_cmap=True)
        cmap="crest",
        xticklabels=x_axis_labels,
        yticklabels=y_axis_labels,
        # cmap="RdYlGn",
    )  # vmin=0, vmax=1
    plt.title(f"Move out rate for {year}")
    plt.xlabel("Dwelling")
    plt.ylabel("Household")
    plt.savefig(os.path.join(save_path, f"{heat_name}_{year}.png"), bbox_inches="tight")
    plt.close()

    # print(f"Heatmap created and saved in the {save_path} directory.")


def clean_nan(df, on_cols=None):
    """Remove rows which contain NaN value in specific columns from df.

    Args:
    - df: pandas.DataFrame to be cleaned
    - on_cols: list of strings containing the column names to consider
    if on_cols=None (default value), all columns of the df are considered

    Returns:
    - cleaned dataframe
    """
    # Replace values which indicate insufficient answers in selected columns with NaN
    df = df.replace(to_replace=list(range(-8, 0)), value=np.nan)
    # Filter rows which contain nan value in the column passed
    df = df.dropna(axis=0, subset=on_cols)
    return df


def timer_func(func):
    """Decorator for adding time measurements."""

    def wrap_func(*args, **kwargs):
        print(f"Calling function {func.__name__}.")
        t1 = time()
        result = func(*args, **kwargs)
        t2 = time()
        print(f"{func.__name__} executed in {(t2-t1):.2f}s")
        return result

    return wrap_func


def check_empty(df, source):
    if df.empty:
        raise ValueError(f"The {source}-df is empty. No further calculations possible.")


def debug_messages(message, ip):
    """prints debugging information. If flag is set, save them to string and return."""
    if ip["debug"]:
        ip["debug_message"] += message + "\n"


def get_full_inhabit(all_dimensions, ip, df_all_v, index_var):
    # Create full inhabit matrix as default for dimensions
    dfs = []
    for dim in all_dimensions:
        dfs.append(pd.DataFrame.from_dict(dim))

    default_inhabit = dfs[0]
    for df in dfs[1:]:
        default_inhabit = default_inhabit.merge(df, how="cross")

    # delete entries that will not occur
    # impossibles = default_inhabit.loc[
    #    default_inhabit["region_type_hh"] != default_inhabit["region_type_dwell"]
    # ]
    # default_inhabit = default_inhabit.drop(index=impossibles.index)

    df_all_v = df_all_v.merge(
        default_inhabit, how="right", on=ip["to_group_col"], validate="m:1"
    )
    df_all_v = df_all_v.loc[df_all_v["region_type_hh"] == df_all_v["region_type_dwell"]]
    df_all_v = df_all_v.set_index(index_var)

    return df_all_v


def load_inhabit_moving(
    inhabit_or_move, year, ip, load_abs=False, alt_path=None, use_weights=True
):
    """Load the data from inhabit_{year}.csv or move_out_{year}.csv to dataframes.

    Columns in matrices: hid, building_type, ownership, condition, rooms,
    region_type, growth_type, income_quintile, hh_type,
    hh_size, age, weights
    Data loaded with one household per row, then grouped.

    Args:
    - inhabit_or_move_out = "inhabit" or "move_out" (subfolder in output)
    - year: year for which to load the data

    Return:
    - df_grouped: grouped and weighted dataframe, but not unstacked
    - df_absolute: grouped and unweighted dataframe, but not unstacked
    """
    inh_move_path = inhabit_or_move
    if alt_path:
        inh_move_path = alt_path

    # Load ungrouped data and save as dataframe
    if inhabit_or_move.split("_")[-1] == "created":
        path_ungrouped = check_absolute_path(
            os.path.join(
                inh_move_path, ip["alloc_output_path"], f"{inhabit_or_move}_{year}.csv"
            )
        )
    else:
        path_ungrouped = check_absolute_path(
            os.path.join(
                ip["evidence_folder"], inh_move_path, f"{inhabit_or_move}_{year}.csv"
            )
        )

    if load_abs:
        usecols = ip["to_group_col"] + [ip["col_weights"]]
    elif use_weights:
        usecols = ip["to_group_col"] + [ip["col_weights"]]
    else:
        usecols = ip["to_group_col"] + [ip["col_move_per_pop"]]

    df_grouped = pd.read_csv(path_ungrouped, usecols=usecols)
    df_grouped = df_grouped.astype({"rooms": "string"})
    df_grouped.set_index(ip["to_group_col"], inplace=True)
    df_absolute = None
    if load_abs:
        usecols = ip["to_group_col"] + ["count"]
        df_absolute = pd.read_csv(
            os.path.join(
                ip["evidence_folder"],
                inh_move_path,
                f"{inhabit_or_move}_{year}_abs.csv",
            ),
            usecols=usecols,
        )
        df_absolute.set_index(ip["to_group_col"], inplace=True)

    return df_grouped, df_absolute


def add_unhappyness(unhappy_final, unhappy_index):
    """Add/Create unhappyness from unhappy_index to unhappy_final.
    For all configurations in unhappy_index:
        Get total sum of moving people: sum(people).
        Get sum of moving people that are happy: sum(people, unhappyness=0).
        Get sum of moving people that are unhappy: sum(people*unhappyness).
        unhappyness_percentage of specific configuration:
            1 - (happy+unhappy)/total

        Add/Create entry for unhappy_final as follows:
            configuration: [total, unhappyness_percentage]

    :param unhappy_final: Add list with entries: {configuration: [moved, unhappyness]}
    :type unhappy_final: dict
    :param unhappy_index: {configuration: [moved, unhappyness]} for all configurations.
    :type unhappy_index: dict
    :return: unhappy_final, updated from unhappy_index.
    :rtype: dict
    """
    # entries in unhappy_index look like this:
    # ('MFH', 'private owner', 'renovated', '4+', 'rural', 'couple_no_children', '2',
    # '>=40', 'q3', 'rural'): [[1.7869510568342093, 1], [2.567647951063737, 0]]

    for config, unhappy in unhappy_index.items():
        sum_total = sum([x[0] for x in unhappy])
        sum_happy = sum(x[0] for x in unhappy if x[1] == 0)
        sum_unhappy = sum([x[1] * x[0] for x in unhappy])
        percentage = 1 - (sum_happy + sum_unhappy) / sum_total

        new_unhappy = [sum_total, percentage]
        if config not in unhappy_final.keys():
            unhappy_final[config] = [new_unhappy]
        else:
            unhappy_final[config].append(new_unhappy)

    return unhappy_final


def get_bsi(pref_v, ip, hh_search_v, dwell_free_v, year):
    bsi_all_years = pd.DataFrame()
    bsi_dwell_by_rooms_all_years = pd.DataFrame()
    bsi_aggregated_all_years = pd.DataFrame()
    # BSI calculation
    pref_m = pref_v.unstack(ip["cols_dwell"])
    move_in_want_bsi_m = pref_m.mul(hh_search_v["search"], axis="index")

    pref_v["move_in_want"] = move_in_want_bsi_m.stack(
        ip["cols_dwell"], future_stack=True
    )

    pref_v.dropna(inplace=True)

    (
        bsi_rel,
        bsi_abs,
        bsi_dwell_by_rooms_rel,
        bsi_dwell_by_rooms_abs,
        bsi_rel_aggregated,
        bsi_abs_aggregated,
    ) = calculate_building_shortage_index(
        ip["cols_dwell"], dwell_free_v["free_dwell"], pref_v["move_in_want"]
    )

    bsi_all_years[f"BSI_rel_{year}"] = bsi_rel
    bsi_all_years[f"BSI_abs_{year}"] = bsi_abs
    bsi_dwell_by_rooms_all_years[f"BSI_dwell_rel_{year}"] = bsi_dwell_by_rooms_rel
    bsi_dwell_by_rooms_all_years[f"BSI_dwell_abs_{year}"] = bsi_dwell_by_rooms_abs
    bsi_aggregated_all_years[f"BSI_rel_agg_{year}"] = bsi_rel_aggregated
    bsi_aggregated_all_years[f"BSI_abs_agg_{year}"] = bsi_abs_aggregated


def order_move_in_want(move_in_want_v, ip):
    """Allocate searching percentiles of households to free dwellings according to their
    preferences. The order of allocation is saved in ip.
    Use mapping a, b, c, d, e, ... for hh_type in the order of which it is given.
    map back after sorting
    """

    def get_order_attributes(sort_orders):
        sort_keys = []
        asc_desc = []
        change_names_dict = {}
        for idx in range(1, 5):
            hh_var = ip[f"alloc_hh_prio_{idx}_feature"]
            hh_order = ip[f"alloc_hh_prio_{idx}_feature_order"]

            if (hh_var == "hh_type") and (
                hh_order.split(", ")[0]
                in [
                    "single",
                    "couple_no_children",
                    "couple_parent",
                    "single_parent",
                    "other",
                ]
            ):
                order = hh_order.split(", ")  # ["single", "other", ...]
                change_names_dict = {x: "abcde"[y] for y, x in enumerate(order)}
                asc_desc.append(True)  # always true as always ascending
            elif (hh_var == "hh_type") and (
                hh_order in ["with_children", "without_children"]
            ):
                change_names_dict = {"with_children": "a", "without_children": "b"}
                asc_desc.append(sort_orders[hh_var][hh_order])
            else:
                asc_desc.append(sort_orders[hh_var][hh_order])

            sort_keys.append(hh_var)  # stays the same for all cases
        return sort_keys, asc_desc, change_names_dict

    def change_names_sort(move_in_want_v, change_names_dict, sort_keys, asc_desc):
        # first change names in a way that can be sorted ascending or desceing
        change_back = {}
        for key, val in change_names_dict.items():
            # already create inverted dict to return the original names after sorting
            change_back[val] = key
            move_in_want_v.loc[move_in_want_v["hh_type"] == key, ["hh_type"]] = val

        # then sort
        move_in_want_v.sort_values(
            by=sort_keys, axis=0, ascending=asc_desc, kind="mergesort", inplace=True
        )

        # then change back the original names
        for key, val in change_back.items():
            move_in_want_v.loc[move_in_want_v["hh_type"] == key, ["hh_type"]] = val

        return move_in_want_v

    # input sheet possible sort orders per category
    sort_orders = {
        "income_quintile": {"largest_first": False, "lowest_first": True},
        "age": {"oldest_first": False, "youngest_first": True},
        "hh_size": {"smallest_first": True, "largest_first": False},
    }

    move_in_want_v.reset_index(inplace=True)

    if ip["household_classification"] == "children":
        sort_orders["hh_type"] = {
            "with_children": True,
            "without_children": False,
        }
    sort_keys, asc_desc, change_names_dict = get_order_attributes(sort_orders)

    # sort
    move_in_want_sorted_v = change_names_sort(
        move_in_want_v, change_names_dict, sort_keys, asc_desc
    )
    move_in_want_sorted_v.set_index([*ip["cols_hh"], *ip["cols_dwell"]], inplace=True)

    return move_in_want_sorted_v


# TODO: Update to current behaviour with flexible number of rooms
def get_unhappyness(ip, original, changed):
    """Find out unhappyness that the changed configuration will create.
    Each change in the configuration from original to changed induces unhappyness.
    Maximal unhappyness per configuration sums up to 1.

    Args:
    - original: configuration where no space is left.
    - changed: configuration with enough free space.
    returns: minimum of (1, unhappyness)
    """
    # keys: original, keys from values: changed
    set_unhappyness = {
        "MFH": {"MFH": 0, "SFH": 0.5},
        "SFH": {"MFH": 0.5, "SFH": 0},
        "renovated": {"renovated": 0, "not renovated": 0.5},
        "not renovated": {"renovated": 0.5, "not renovated": 0},
        "1": {"1": 0, "2": 0.25, "3": 0.75, "4+": 1},
        "2": {"1": 0.25, "2": 0, "3": 0.25, "4+": 0.75},
        "3": {"1": 0.75, "2": 0.25, "3": 0, "4+": 0.25},
        "4+": {"1": 1, "2": 0.5, "3": 0.25, "4+": 0},
        "urban": {"urban": 0, "rural": 0.75},
        "rural": {"urban": 0.75, "rural": 0},
        "growing": {"growing": 0, "neither": 0.5, "shrinking": 0.75},
        "neither": {"growing": 0.5, "neither": 0, "shrinking": 0.5},
        "shrinking": {"growing": 0.75, "neither": 0.5, "shrinking": 0},
    }
    if ip["dwelling_ownership_short"] == "true":
        set_unhappyness.update(
            {
                "private owner": {
                    "private owner": 0,
                    "private tenant": 0.5,
                },
                "private tenant": {
                    "private owner": 0.5,
                    "private tenant": 0,
                },
            }
        )
    elif ip["dwelling_ownership_short"] == "false":
        set_unhappyness.update(
            {
                "non profit dwelling": {
                    "non profit dwelling": 0,
                    "private owner": 0.5,
                    "private tenant": 0.5,
                },
                "private owner": {
                    "non profit dwelling": 0.5,
                    "private owner": 0,
                    "private tenant": 0.5,
                },
                "private tenant": {
                    "non profit dwelling": 0.5,
                    "private owner": 0.5,
                    "private tenant": 0,
                },
            }
        )

    unhappyness = 0
    # for o, c in zip(original, changed):
    #    unhappyness += set_unhappyness[o][c]
    return unhappyness  # min(1, unhappyness)


def calculate_building_shortage_index(
    cols_dwell, freed_dwellings, move_in_want_v: pd.DataFrame
):
    """
    Function that calculates the building shortage index for 1 year
    Args:
        cols_dwell:
        freed_dwellings:
        move_in_want_v:

    Returns:

    """
    move_in_want_m = move_in_want_v.unstack(cols_dwell)
    desired_dwellings = move_in_want_m.sum(axis=0)

    availiable_dwellings = freed_dwellings  # + new_dwellings - demolition ----
    # TODO Neubau und Abriss ergänzen

    # highly aggregated Version, so that only room categories remain
    room_categories = ["1", "2", "3", "4+"]  # TODO generalize input
    bsi_dwellings_by_rooms_relative = pd.Series(index=room_categories)
    bsi_dwellings_by_rooms_absolute = pd.Series(index=room_categories)
    for room_cat in room_categories:
        total_dwellings_by_rooms_desired = desired_dwellings.xs(room_cat, level=3).sum()
        total_dwellings_by_rooms_freed = availiable_dwellings.xs(
            room_cat, level=3
        ).sum()
        bsi_dwellings_by_rooms_relative[room_cat] = (
            total_dwellings_by_rooms_desired / total_dwellings_by_rooms_freed
        )
        bsi_dwellings_by_rooms_absolute[room_cat] = (
            total_dwellings_by_rooms_desired - total_dwellings_by_rooms_freed
        )

    # medium aggregation, without condition
    """desired_dwellings_agg1 = desired_dwellings.groupby(['building_type',
    'ownership', 'rooms']).sum()
    grouped = desired_dwellings_agg1.groupby(['building_type', 'rooms'])
    #desired_dwellings_agg1
    desired_dwellings_agg2 = pd.Series(index=[np.array(['MFH', 'MFH','MFH','MFH','MFH',
                                                        'MFH','MFH','MFH','SFH','SFH',
                                                        'SFH','SFH','SFH','SFH','SFH',
                                                        'SFH',]),
                                              np.array(['private owner','private owner',
                                              'private owner','private owner',
                                                        'aggregated tenant',
                                                        'aggregated tenant',
                                                        'aggregated tenant',
                                                        'aggregated tenant',
                                                        'private owner','private owner',
                                                        'private owner','private owner',
                                                        'aggregated tenant',
                                                        'aggregated tenant',
                                                        'aggregated tenant',
                                                        'aggregated tenant']),
                                              np.array(['1', '2', '3', '4+', '1', '2',
                                                        '3', '4+', '1', '2', '3', '4+',
                                                        '1', '2', '3', '4+'])])

    for name, group in grouped:
        desired_dwellings_agg2.loc[name[0], 'aggregated tenant', name[1]] = \
             desired_dwellings_agg1.loc[name[0], ['non profit dwelling',
             'private tenant'], name[1]].sum()
        desired_dwellings_agg2.loc[name[0], 'private owner', name[1]] = \
            desired_dwellings_agg1.loc[name[0], ['private owner'], name[1]].sum()"""

    def aggregate_dwellings(dwellings_vector):
        dwellings_agg1 = dwellings_vector.groupby(
            ["building_type", "ownership", "rooms"]
        ).sum()
        grouped = dwellings_agg1.groupby(["building_type", "rooms"])
        # desired_dwellings_agg1
        dwellings_agg2 = pd.Series(
            index=[
                np.array(
                    [
                        "MFH",
                        "MFH",
                        "MFH",
                        "MFH",
                        "MFH",
                        "MFH",
                        "MFH",
                        "MFH",
                        "SFH",
                        "SFH",
                        "SFH",
                        "SFH",
                        "SFH",
                        "SFH",
                        "SFH",
                        "SFH",
                    ]
                ),
                np.array(  # TODO adapt to new matrix reduction configuration options
                    [
                        "private owner",
                        "private owner",
                        "private owner",
                        "private owner",
                        "aggregated tenant",
                        "aggregated tenant",
                        "aggregated tenant",
                        "aggregated tenant",
                        "private owner",
                        "private owner",
                        "private owner",
                        "private owner",
                        "aggregated tenant",
                        "aggregated tenant",
                        "aggregated tenant",
                        "aggregated tenant",
                    ]
                ),
                np.array(
                    [
                        "1",
                        "2",
                        "3",
                        "4+",
                        "1",
                        "2",
                        "3",
                        "4+",
                        "1",
                        "2",
                        "3",
                        "4+",
                        "1",
                        "2",
                        "3",
                        "4+",
                    ]
                ),
            ]
        )

        for name, group in grouped:
            dwellings_agg2.loc[name[0], "aggregated tenant", name[1]] = (
                dwellings_agg1.loc[
                    name[0], ["non profit dwelling", "private tenant"], name[1]
                ].sum()
            )
            dwellings_agg2.loc[name[0], "private owner", name[1]] = dwellings_agg1.loc[
                name[0], ["private owner"], name[1]
            ].sum()
        return dwellings_agg2

    desired_dwellings_aggregated = aggregate_dwellings(desired_dwellings)
    availiable_dwellings_aggregated = aggregate_dwellings(availiable_dwellings)
    bsi_relative_aggregated = (
        desired_dwellings_aggregated / availiable_dwellings_aggregated
    )
    bsi_absolute_aggregated = (
        desired_dwellings_aggregated - availiable_dwellings_aggregated
    )

    # no aggregation, all dwelling categories
    bsi_relative = desired_dwellings / availiable_dwellings
    bsi_absolute = desired_dwellings - availiable_dwellings
    return (
        bsi_relative,
        bsi_absolute,
        bsi_dwellings_by_rooms_relative,
        bsi_dwellings_by_rooms_absolute,
        bsi_relative_aggregated,
        bsi_absolute_aggregated,
    )


def get_age_classes(ip):
    age_classes = {}
    if ", " in str(ip["age_limiter"]):
        all_ages = ip["age_limiter"].split(", ")
        all_ages = [int(x) for x in all_ages]
        for idx, age in enumerate(all_ages):
            if idx == 0:
                age_classes.update({x: f"{ip['min_age']}-{age-1}" for x in range(age)})
            else:
                age_classes.update(
                    {
                        x: f"{all_ages[idx-1]}-{age-1}"
                        for x in range(all_ages[idx - 1], age)
                    }
                )
        age_classes.update(
            {
                x: f"{all_ages[-1]}-{ip['max_age']}"
                for x in range(all_ages[-1], ip["max_age"] + 1)
            }
        )

    else:
        age_limiter = int(ip["age_limiter"])
        age_classes.update({x: f"<{age_limiter}" for x in range(age_limiter)})
        age_classes.update(
            {x: f">={age_limiter}" for x in range(age_limiter, ip["max_age"])}
        )
    return age_classes
