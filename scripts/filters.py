"""Define filters for soep dataframes"""

import numpy as np
import pandas as pd

try:
    import scripts.misc as misc
except ModuleNotFoundError:
    import misc


# @timer_func
def age_filter(df, ip):
    r"""Add column \"sage\" to dataframe. min_age <= age <= max_age.

    Parameters:
        - df: Dataframe, loaded soep csv.
        - min_age: int, minimum age.
        - max_age: int, maximal age.
    """

    # sage: age at time of survey
    df2 = df.copy()
    df2["sage"] = df["syear"] - df["gebjahr"]
    # limit age to min_age <= age <= max_age
    age = (df2["sage"] >= ip["min_age"]) & (df2["sage"] <= ip["max_age"])
    return df2[age]


# @timer_func
def netto_filter(df):
    r"""Limit hh interviews to those that were conducted successfully.

    Parameters:
        - df: Dataframe, loaded soep csv.
    """
    hnetto = df["hnetto"] == 1
    return df[hnetto]


# @timer_func
def year_filter(df, set_year=2019):
    r"""Select only interview years of certain year.

    Parameters:
        - df: Dataframe, loaded soep csv.
        - set_year: int, specific year to be used.
    """
    #
    year = df["syear"] == set_year
    return df[year]


# @timer_func
def households_filter(df, ip):
    r"""Keep the first non NaN row per household for all variables
    that are not gebjahr, hid or syear.
    As we group by households (and syear) and there are only household variables
    in df except for gebjahr, the values in the other columns are the same
    for all people (rows) in the same household. Keep all entries, add column and mark
    the entries with True at the oldest person in Household, False for the rest.

    Parameters:
        - df: Dataframe, loaded soep csv.
        - ip:
    """

    def first_non_nan(series):
        for value in series:
            if not pd.isna(value):
                return value
        return np.nan  # If all values are NaN, return None

    # Apply the first_non_nan function to each column
    # (except "gebjahr", "hid", and "syear")
    # within each group defined by "hid" and "syear"
    # TODO: seems not to work correctly. Keeps not always only oldest person
    oldest_hid = df.groupby(["hid", "syear"], as_index=False).agg(
        {
            # only keep the oldest person per household and survey year
            "gebjahr": "min",  # Minimum gebjahr value
            **{
                col: first_non_nan
                for col in df.columns
                if col not in ["gebjahr", "hid", "syear"]
            },
        }
    )
    # adding column with True entries at the oldest person in household
    df[ip["col_oldest"]] = df["pid"].isin(oldest_hid["pid"])
    # cols_used.append(ip["col_oldest"])

    return df


def filter_df(df, ip, set_year):
    # df = year_filter(df, set_year=set_year)
    df = netto_filter(df)
    df = age_filter(df, ip)
    df = households_filter(df, ip)
    df = misc.clean_nan(df)
    misc.check_empty(df, "households")
    return df
