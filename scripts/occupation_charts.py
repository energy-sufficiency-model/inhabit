"""Create charts to evaluate all inhabit tables over time."""

import pandas as pd
import os
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from cycler import cycler

try:
    import inputs
    import misc
except ModuleNotFoundError:
    import scripts.inputs as inputs
    import scripts.misc as misc


def underoccup_col(ip, df, average_disagg):
    df.loc[df["rooms"] == f"{ip['max_rooms']}+", "rooms"] = ip["max_rooms"]
    df.loc[df["hh_size"] == f"{ip['max_hh_size']}+", "hh_size"] = ip["max_hh_size"]
    df["rooms"] = pd.to_numeric(df["rooms"])
    df["hh_size"] = pd.to_numeric(df["hh_size"])

    if average_disagg == "underoccupation":
        # Calculate the occupation indicator for each hh
        df["underoccupation"] = df["rooms"] - df["hh_size"]
    return df


def underoccup(
    ip, inh_all, disagg, start_year, end_year, save_path, average_disagg, plot_year
):
    """Calculate the underoccupation over the years for different disaggregations"""
    # Initialize a global dataframe to store the results for all years
    plot_line_df = None
    global_results = pd.DataFrame()
    global_results["year"] = 0
    if average_disagg == "underoccupation":
        global_results["underoccupation"] = 0

    for yr in range(start_year, end_year + 1):
        df = inh_all[yr].copy()
        df = underoccup_col(ip, df, average_disagg)

        if yr == plot_year and average_disagg == "underoccupation":
            plot_line_df = df.copy()
        # print(df[disagg])
        # print(df[average_disagg])
        # exit(1)
        result = (
            df.groupby(disagg)
            .apply(
                lambda x: np.average(x[average_disagg], weights=x["weights"]),
                include_groups=False,
            )
            .reset_index()
        )
        result.rename(columns={0: average_disagg}, inplace=True)

        # Add year column to the result dataframe
        result["year"] = yr

        # Append the result to the global dataframe
        global_results = pd.concat([global_results, result])

    return global_results, plot_line_df


def plot_all_yrs(
    df_all,
    avg_disagg,
    disagg,
    start_year,
    model_start_year,
    end_year,
    save_path,
    inh_scen,
    scen,
):
    """
    Plots the average value of "avg_disagg" by "disagg" for all years.

    Params:
    - df: weighted DataFrame with columns year, "avg_disagg, "disagg"
    - avg_disagg: str, main avg_disagg (e.g. "underoccupation")
    - disagg: list of str, disaggregation avg_disagg, e.g. ["income_group"]
    - start_year: int
    - end_year: int
    - save_path: str
    - multilelvel: bool, if True: multiple groupby avg_disagg can be passed
    as a list of strings, if only one str is passed in the list:
    leave out multilevel parameter (defaults to False)
    - share: bool, if True: title is called "Share of underoccupation => 1" resp. ">=2"

    Return:
    Plots a line graph with
    - x-axis: years
    - y-axis: main variable (weighted)
    - lines for each disaggregation (groups of groupby variable)
    """
    # Plotting the line graph
    fig, ax = plt.subplots(figsize=(8, 8))

    color = plt.cm.Blues(
        np.linspace(0, 1, len(df_all.groupby(disagg).groups.keys()) + 1)
    )
    # skip first color
    ax.set_prop_cycle(cycler(color=color[1:]) * cycler(linestyle=["--"]))

    # Iterate over each group and plot a line
    for group, data in df_all.groupby(disagg):
        if type(group) is float:
            group = int(group)
        ax.plot(data["year"], data[avg_disagg], label=group)

    # plot line where the model starts
    y0 = 0
    y1 = 3
    if avg_disagg == "rooms":
        y0 = 2
        y1 = 6
    ax.set_ylim([y0, y1])
    ax.axvline(x=ms_year, color="lightgray", label="Model start")
    ax.axvline(
        x=int(ip["scenario_start_year"]),
        color="darkgray",
        label="Scenario start",
    )

    ylab = avg_disagg
    if avg_disagg == "underoccupation":
        ax.axhline(y=1, color="dimgray", label="uo threshold", linestyle="dashed")
        ylab = "occupation: No rooms - No persons"
    # Set plotting parameters
    ax.set_xticks(range(start_year, end_year + 1, 10))
    plt.xticks(rotation=90)
    ax.set_xlabel("Year")
    ax.set_ylabel(ylab)
    # Shrink current axis by 20% and put legend on the right of the axis
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    # Check if any groupby variables exist for labeling the legend
    legend_title = disagg
    ax.legend(title=legend_title, loc="center left", bbox_to_anchor=(1, 0.5))
    save_name = f"{avg_disagg}_by_{disagg}.{ip['file_end']}"
    # title = f'{scen}: {save_name[0:-4].replace("_", " ")}'
    # plt.title(title)
    plt.savefig(os.path.join(save_path, save_name), bbox_inches="tight")
    plt.close()


def plot_line(df, df_d, avg_disagg, disagg, plot_year, save_path, scen):
    """
    Return:
    Plots a line graph with
    - x-axis: years
    - y-axis: main variable (weighted)
    - lines
    """
    # Aggregate the weights for each group
    df_agg = df.groupby([disagg, avg_disagg])["weights"].sum().reset_index()
    # Aggregate the weights for each group
    df_agg_d = df_d.groupby([disagg, avg_disagg])["weights"].sum().reset_index()
    paper_disaggs = {
        "building_type": "a",
        "age": "b",
        "income_quintile": "c",
        "hh_size": "d",
        "hh_type": "e",
        "region_type_dwell": "f",
    }
    # Create a line plot
    fig, ax = plt.subplots(figsize=(7, 7))
    sns.lineplot(
        data=df_agg,
        x=avg_disagg,
        y="weights",
        hue=df_agg[disagg].apply(str),
        palette="Blues",
        # marker="o",
        markersize=8,
        linestyle="-",
    )

    sns.lineplot(
        data=df_agg_d,
        x=avg_disagg,
        y="weights",
        hue=df_agg_d[disagg].apply(str),
        palette="Blues",
        # marker="o",
        markersize=8,
        linestyle="--",
    )


    handles, labels = ax.get_legend_handles_labels()
    labels_df = [f"{labels[x]} {scen}" for x in range(len(labels) // 2)]
    labels_df_d = [f"{labels[x]} default" for x in range(len(labels) // 2, len(labels))]
    labels_all = labels_df + labels_df_d
    ax.axvline(x=1, color="lightgray", label="adequate occ", linestyle="--",)
    ax.legend(
        handles,
        labels_all,
        title=disagg,
        loc="upper right",
        fontsize=16,
        title_fontsize=16,
    )

    for item in (
        [ax.xaxis.label, ax.yaxis.label, ax.title]
        + ax.get_xticklabels()
        + ax.get_yticklabels()
    ):
        item.set_fontsize(16)
    ax.set_xlim([-4, 6])
    ax.set_ylim([0, 16e6])
    # Set plotting parameters
    # plt.title(f"{avg_disagg} by {disagg} for {year}")
    plt.xticks(
        range(int(df_agg[avg_disagg].min()), int(df_agg[avg_disagg].max()) + 1, 2)
    )
    plt.xlabel(f"{avg_disagg}")
    plt.ylabel("Number of households")
    # plt.legend(title=disagg, loc="upper right")  # , bbox_to_anchor=(1, 0.5))
    paper_save = "fig10{}_underoccupation_by_{}_line_{}.{}".format(
        paper_disaggs[disagg], disagg, plot_year, ip["file_end"]
    )
    plt.savefig(os.path.join(save_path, paper_save))
    plt.close()


def plot_all_scenarios(dfs, s_year, ms_year, t_year, save_path, all_avg_disaggs):
    loosely_dashdotted = (0, (3, 10, 1, 10, 1, 10))
    plot_to_latex = {}
    linestyles = ["--", ":", loosely_dashdotted, "-.", "-"]
    colors = ["r", "g", "b", "y", "c", "m"]
    defined_scens = [
        "MoR_incr",
        "Nopreferred_uo",
        "No_alloc_uo_2",
        "Split_sfh_6",
        "Red_UO",
        "default",
    ]
    paper_disaggs = {
        "building_type": "a",
        "age": "b",
        "income_quintile": "c",
        "hh_size": "d",
        "hh_type": "e",
        "region_type_dwell": "f",
    }
    for avg_disagg in all_avg_disaggs:
        plot_to_latex[avg_disagg] = {}
        y0 = 0
        y1 = 2.6
        paper_save = "fig9{}_underoccupation_by_{}.{}"
        if avg_disagg == "rooms":
            y0 = 2
            y1 = 6  # ip["max_rooms"]
            paper_save = "fig8{}_rooms_by_{}.{}"
        elif avg_disagg == "hh_size":
            y0 = 0
            y1 = ip["max_hh_size"] + 0.25
        for disagg in dfs[avg_disagg].keys():
            plot_to_latex[avg_disagg][disagg] = {}

            fig, ax = plt.subplots(figsize=(10, 7))
            scenario = ip["compare_scenarios"][0]
            len_disagg = (
                len(dfs[avg_disagg][disagg][scenario].groupby(disagg).groups.keys()) + 1
            )
            cycler_colors = {
                0: ["orange", plt.cm.Oranges(np.linspace(0, 1, len_disagg))],
                1: ["blue", plt.cm.Blues(np.linspace(0, 1, len_disagg))],
                2: ["green", plt.cm.Greens(np.linspace(0, 1, len_disagg))],
                3: ["grey", plt.cm.Greys(np.linspace(0, 1, len_disagg))],
                4: ["purple", plt.cm.Purples(np.linspace(0, 1, len_disagg))],
                5: ["red", plt.cm.Reds(np.linspace(0, 1, len_disagg))],
            }

            for i, scen in enumerate(ip["compare_scenarios"]):
                plot_to_latex[avg_disagg][disagg][scen] = {}
                # Draw the lines of the scenario
                if scen not in defined_scens:
                    # skip first color
                    ax.set_prop_cycle(
                        cycler(color=cycler_colors[i][1][1:])
                        * cycler(linestyle=linestyles)
                    )
                elif scen == "default":
                    ax.set_prop_cycle(
                        cycler(color=["black"]) * cycler(linestyle=linestyles)
                    )
                else:
                    ax.set_prop_cycle(
                        cycler(color=[colors[i]]) * cycler(linestyle=linestyles)
                    )

                for group, data in dfs[avg_disagg][disagg][scen].groupby(disagg):
                    plot_to_latex[avg_disagg][disagg][scen][group] = {}
                    if type(group) is float:
                        group = int(group)
                    ax.plot(
                        data["year"],
                        data[avg_disagg],
                        label=f"{group} - {scen}",
                    )
                    to_plot = data.loc[data["year"] == 2050, [avg_disagg]].to_numpy()[
                        0
                    ][0]
                    # print(f"{avg_disagg}, disagg {disagg}, scenario {scen}")

                    plot_to_latex[avg_disagg][disagg][scen][group] = round(to_plot, 2)

            for item in (
                [ax.title, ax.xaxis.label, ax.yaxis.label]
                + ax.get_xticklabels()
                + ax.get_yticklabels()
            ):
                item.set_fontsize(16)
            ax.set_ylim([y0, y1])
            # plot vertical line where the model starts
            ax.axvline(x=ms_year, color="lightgray", label="Model start")
            ax.axvline(
                x=int(ip["scenario_start_year"]),
                color="darkgray",
                label="Scenario start",
            )

            ylab = avg_disagg
            if avg_disagg == "underoccupation":
                ax.axhline(
                    y=1,
                    color="dimgray",
                    label="uo threshold",
                    linestyle="dashed",
                )
                ylab = "occupation: No rooms - No persons"
            handles, labels = ax.get_legend_handles_labels()
            # sort both labels and handles by labels
            labels, handles = zip(
                *sorted(zip(labels, handles), key=lambda t: t[0].split(" ")[0])
            )
            # Set plotting parameters
            ax.set_xticks(range(s_year, t_year + 1, 10))
            plt.xticks(rotation=90)
            ax.set_xlabel("Year")
            ylabel = ylab
            ax.set_ylabel(ylabel)
            # Shrink current axis by 20% and put legend on the right of the axis
            box = ax.get_position()
            ax.set_position((box.x0, box.y0, box.width * 0.8, box.height))
            legend_title = disagg
            ax.legend(
                handles,
                labels,
                loc="center left",
                fontsize=16,
                title=legend_title,
                title_fontsize=16,
                bbox_to_anchor=(1, 0.5),
            )
            if ip["compare_scenarios"] == defined_scens:
                # save_name = f"{avg_disagg}_by_{disagg}_all_scen.{ip['file_end']}"
                s_me = "fig11{}_underoccupation_by_{}.{}".format(
                    paper_disaggs[disagg], disagg, ip["file_end"]
                )
            else:
                s_me = paper_save.format(paper_disaggs[disagg], disagg, ip["file_end"])
                # save_name = f"{avg_disagg}_by_{disagg}.{ip['file_end']}"
            # fig_title = save_name[0:-4].replace("_", " ")
            # plt.title(f'{"_vs_".join(ip["compare_scenarios"])}: {fig_title}')
            plt.savefig(os.path.join(save_path, s_me), bbox_inches="tight")
            plt.close()
    print_for_table = True

    if print_for_table:
        print(plot_to_latex)
        for avg_disagg, disaggs in plot_to_latex.items():
            print(avg_disagg)
            all_scens = {scen : f"{scen} & " for scen in ip["compare_scenarios"]}
            all_disaggs = ""
            for disagg, scens in disaggs.items():
                all_disaggs = all_disaggs + " & " + disagg
                for scen, dis in scens.items():
                    all_scens[scen] +=" & ".join("{:.2f}".format(x) for x in dis.values())
                    all_scens[scen] +=" & "
            all_disaggs += r" \\"
            print(all_disaggs.lstrip(" & "))
            for v in all_scens.values():
                print(v.rstrip(" & ") + r" \\")


def get_inh_all(inh_path, ms_year, t_year, s_year):
    inh_created = {
        y: pd.read_csv(os.path.join(inh_path, f"inhabit_created_{y}.csv"))
        for y in range(ms_year + 1, t_year + 1)
    }
    # add inhabit original for last evidence year for better visualizations
    inh_created[ms_year] = pd.read_csv(
        os.path.join(
            ip["evidence_folder"], ip["inhabit_path"], f"inhabit_{ms_year}.csv"
        )
    )
    inh_original = {
        y: pd.read_csv(
            os.path.join(ip["evidence_folder"], ip["inhabit_path"], f"inhabit_{y}.csv")
        )
        for y in range(s_year, ms_year + 1)
    }
    inhabits = {**inh_original, **inh_created}
    # change column content from hh type
    for y in range(s_year, t_year + 1):
        inhabits[y].loc[inhabits[y]["hh_type"] == "with_children", ["hh_type"]] = (
            "children"
        )
        inhabits[y].loc[inhabits[y]["hh_type"] == "without_children", ["hh_type"]] = (
            "no children"
        )

    return inhabits


# TODO: nicht nur über empirie sondern über alle Jahre
def create_charts(ip, plot_year, ms_year, t_year, s_year):
    all_disaggs = [
        "income_quintile",
        "building_type",
        "age",
        "hh_size",
        "hh_type",
        "region_type_dwell",
    ]
    all_avg_disaggs = ["underoccupation", "rooms"]
    inh_all, scen_paths, scens = [], [], []
    dfs = {}

    for scen in ip["compare_scenarios"]:
        scen_path = misc.get_save_path(ip["output_folder"], scen)
        inh_scen = get_inh_all(
            os.path.join(scen_path, ip["alloc_output_path"]), ms_year, t_year, s_year
        )
        inh_all.append(inh_scen)
        scen_paths.append(scen_path)
        scens.append(scen)

    for avg_disagg in all_avg_disaggs:
        dfs[avg_disagg] = {}
        for disagg in all_disaggs:
            if avg_disagg != disagg:
                dfs[avg_disagg][disagg] = {}
                for scen in scens:
                    dfs[avg_disagg][disagg].update({scen: pd.DataFrame})

    plot_line_default = None
    plot_line_dfs = {}
    for inh_scen, scen_path, scen in zip(inh_all, scen_paths, scens):
        save_path = misc.get_save_path(
            ip["output_folder"], f"Plots-Single: {scen_path}"
        )
        # underoccupation indicator by income quintile, building type, hh_size, age
        # underoccup = nr. of rooms - hh size
        for avg_disagg in all_avg_disaggs:
            for disagg in all_disaggs:
                if avg_disagg != disagg:
                    # create general plots for 2015 and all years
                    df_all, plot_line_df = underoccup(
                        ip,
                        inh_scen,
                        disagg,
                        s_year,
                        t_year,
                        save_path,
                        avg_disagg,
                        plot_year,
                    )
                    if plot_line_df is not None:
                        if scen == "default":
                            plot_line_default = plot_line_df
                        else:
                            plot_line_dfs[scen] = plot_line_df

                    plot_all_yrs(
                        df_all,
                        avg_disagg,
                        disagg,
                        s_year,
                        ms_year,
                        t_year,
                        save_path,
                        inh_scen,
                        scen,
                    )

                    dfs[avg_disagg][disagg][scen] = df_all

    if len(ip["compare_scenarios"]) > 1:
        save_folder = f"Plots-{'_vs_'.join(ip['compare_scenarios'])}"
        save_path = misc.get_save_path(ip["output_folder"], save_folder)
        if "default" in ip["compare_scenarios"]:
            # Create line plot for the year plot_yr for underoccupation
            for disagg in all_disaggs:
                for scen, plot_line_df in plot_line_dfs.items():
                    plot_line(
                        plot_line_df,
                        plot_line_default,
                        "underoccupation",
                        disagg,
                        plot_year,
                        save_path,
                        scen,
                    )

        plot_all_scenarios(dfs, s_year, ms_year, t_year, save_path, all_avg_disaggs)


def plot_forced_moves(ip, sc_year, t_year, scen):
    scen_path = os.path.join(
        misc.get_save_path(ip["output_folder"], scen), ip["alloc_output_path"]
    )
    fm_all = {
        y: pd.read_csv(os.path.join(scen_path, f"forced_move_in_{y}.csv"))
        for y in range(sc_year, t_year + 1)
    }
    fm_max_all = {
        y: fm_all[y].iloc[fm_all[y]["weights"].idxmax()]
        for y in range(sc_year, t_year + 1)
    }
    fm_max_all_str = {
        y: [fm_max_all[y][x] for x in fm_max_all[y].index]
        for y in range(sc_year, t_year + 1)
    }

    # print(fm_max_all_str)
    fig, ax = plt.subplots(figsize=(8, 8))

    # Iterate over each group and plot a line
    for year, data in fm_max_all_str.items():
        ax.plot(year, data[-1], marker="o", label=data[:-1])

    # ax.set_xticks(range(sc_year, t_year + 1, 10))
    plt.xticks(rotation=90)
    ax.set_xlabel("Year")
    # ax.set_ylabel(ylab)
    # Shrink current axis by 20% and put legend on the right of the axis
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    # Check if any groupby variables exist for labeling the legend
    legend_title = "test"
    ax.legend(title=legend_title, loc="center left", bbox_to_anchor=(1, 0.5))
    save_name = f"forced_move_ins.{ip['file_end']}"
    # title = f'{scen}: {save_name[0:-4].replace("_", " ")}'
    # plt.title(title)
    plt.savefig(os.path.join(scen_path, save_name), bbox_inches="tight")
    plt.close()


def plot_mor(ip):
    all_disaggs = {
        "age": "fig12b_mor_by_age",
        "building_type": "fig12a_mor_by_building_type",
        "hh_size": "fig12d_mor_by_hh_size",
        "income_quintile": "fig12c_mor_by_income_quintile",
        "hh_type": "fig12e_mor_by_hh_type",
        "region_type_dwell": "fig12f_mor_by_region_type",
    }
    for scen in ip["compare_scenarios"]:
        scen_path = os.path.join(
            misc.get_save_path(ip["output_folder"], scen), ip["move_out_rate_path"]
        )
        save_folder = f"Plots-{'_vs_'.join(ip['compare_scenarios'])}"
        save_path = misc.get_save_path(ip["output_folder"], save_folder)
        for dis, name in all_disaggs.items():
            mor = pd.read_csv(
                os.path.join(
                    scen_path,
                    f"mor_{ip['empirical_start_year']}_{ip['target_year']}_{dis}.csv",
                ),
                index_col=dis,
            )
            fig, ax = plt.subplots(figsize=(8, 8))
            df_regulated = mor.loc[
                :, [col for col in mor.columns if "_reg" in col]
            ].copy()
            df_regular = mor.loc[
                :, [col for col in mor.columns if "_reg" not in col]
            ].copy()
            # Extract years and convert to numeric
            df_regulated.columns = pd.to_numeric(
                df_regulated.columns.str.extract(r"(\d{4})").iloc[:, 0]
            )
            df_regular.columns = pd.to_numeric(
                df_regular.columns.str.extract(r"(\d{4})").iloc[:, 0]
            )

            # Combine: regulated before ms_year, regular after ms_year
            df_combined = pd.concat(
                [
                    df_regulated.loc[:, df_regulated.columns >= ip["model_start_year"]],
                    df_regular.loc[:, df_regular.columns < ip["model_start_year"]],
                ],
                axis=1,
            )

            # Sort columns (years) to ensure correct order
            df_combined = df_combined.reindex(columns=sorted(df_combined.columns))
            df_combined.T.plot(ax=ax)
            plt.title(f"Move out Rate {dis}")
            plt.xlabel("Year")
            plt.ylabel("Move out Rate")
            # plot vertical line where the model starts
            ax.axvline(x=ip["model_start_year"], color="lightgray", label="Model start")
            ax.axvline(
                x=int(ip["scenario_start_year"]),
                color="darkgray",
                label="Scenario start",
            )
            for item in (
                [ax.title, ax.xaxis.label, ax.yaxis.label]
                + ax.get_xticklabels()
                + ax.get_yticklabels()
            ):
                item.set_fontsize(16)
            plt.legend(title=dis, fontsize=16, title_fontsize=16)
            plt.savefig(
                os.path.join(save_path, f"{name}.{ip['file_end']}"),
                bbox_inches="tight",
            )
            plt.close()


if __name__ == "__main__":
    ip = inputs.load_inputs()
    ip["compare_scenarios"] = [
        # "MoR_incr",
        # "Nopreferred_uo",
        # "No_alloc_uo_2",
        # "Split_sfh_6",
        # "Red_UO",
        #"MoR+No_alloc",
        #"MoR+Nopreferred",
        #"MoR+Split",
        #"No_alloc+Nopreferred",
        #"No_alloc+Split",
        # "Nopreferred+Split",
        "MoR+No_alloc+Nopreferred",
        "MoR+No_alloc+Split",
        "MoR+Nopreferred+Split",
        "No_alloc+Nopreferred+Split",
        # "default",
    ]
    ip["file_end"] = "pdf"
    plot_year = ip["target_year"]
    ms_year = ip["model_start_year"]
    t_year = ip["target_year"]
    s_year = ip["empirical_start_year"]
    sc_year = ip["scenario_start_year"]
    # ms_year = ip["empirical_start_year"]
    # t_year = ip["model_start_year"]
    # plot_forced_moves(ip, sc_year, t_year, "Red_UO")
    plot_mor(ip)
    create_charts(ip, plot_year, ms_year, t_year, s_year)
    # plot_underocc_after_move(ip, s_year, ms_year, t_year)
