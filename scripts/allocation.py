"""Create a matrix with preferences where to move in to per household category.

The marix will have the same dimensions as the inhabit matrix.
Each row in the matrix represents one household category.
The columns represent the household categories.
The cells in each row are filled with values summing up to 1.
A value in a cell means the following:
The probability that the hosehold with the given configuration (the row) wants to move
to the dwelling type of the respective column.

From the input sheet "inputs.csv" a variant for the generation of the preference matrix
must be chosen.

Currently available variant:
- "current_quintile": the preferences of the household category
  are equal to the distribution of households in the inhabit matrix
  in the same household category (including the same quintile)

Variants in the future:
- "quintile_above": the preference of the household category in quintile qx
  is equal to the distribution of households in the inhabit matrix
  of the same household category but in the above quintile (q{x+1})
- "highest_quintile": the preference of the household category in quintile x
  is equal to the distribution of households in the inhabit matrix
  of the same household category but in the highest quintile (q5)
- "avg_highest_current_quintile": the preference of the household category
  in quintile qx is equal to the average of
  a) the distribution of hh in the inhabit matrix of the same hh category
  and b) the distribution of hh in the inhabit matrix
  in the same hh category but in the highest quintile ((qx+q5)/2)
"""

import os
import math
import pandas as pd
import concurrent.futures


# import os

try:
    import misc
except ModuleNotFoundError:
    import scripts.misc as misc
try:
    from evaluation_plots import arange_room_nos_list
except ModuleNotFoundError:
    from scripts.evaluation_plots import arange_room_nos_list


def calc_stay_search(inhabit_v, mor_v, ip, split_large_dwell):
    # Merge dataframes on common columns

    move_out_gen_v = pd.merge(inhabit_v, mor_v, on=ip["to_group_col"])

    # stay = inhabits-mor*inhabits, move = mor*inhabits
    move_out_gen_v["stay"] = move_out_gen_v[ip["col_weights"]] * (
        1 - move_out_gen_v[ip["col_move_per_pop"]]
    )
    move_out_gen_v["search"] = (
        move_out_gen_v[ip["col_weights"]] * move_out_gen_v[ip["col_move_per_pop"]]
    )
    move_out_gen_v["search"] = move_out_gen_v["search"].round(decimals=0)
    # calculate marginal distribution for the households to get hh_search
    # these are all people-configurations that move out
    hh_search_v = (
        move_out_gen_v.groupby(by=ip["cols_hh"])["search"]
        .sum(numeric_only=True)
        .reset_index()
    )
    hh_search_v.set_index(ip["cols_hh"], inplace=True)
    # calculate marginal distribution for the dwellings to get dwell_free
    # these are the building-configurations that will be free due to people moving out
    dwell_free_v = (
        move_out_gen_v.groupby(by=ip["cols_dwell"])["search"]
        .sum(numeric_only=True)
        .reset_index()
    )

    dwell_free_v.rename(columns={"search": "free_dwell"}, inplace=True)

    if split_large_dwell != "dont_split":
        # split dwellings with large rooms. Either divide or leave them
        large_dwells = split_large_dwell.split("_")
        rooms = large_dwells[0]
        dwells = large_dwells[2]
        new_dwells = {}  # rooms: [new dwell size, new dwell size, ...]
        for i in range(int(rooms), ip["max_rooms"] + 1):
            new_key = str(i)
            if i == ip["max_rooms"]:
                new_key = f"{i}+"
            new_dwells[new_key] = []
            subtract = math.trunc(i / int(dwells))  # e.g. 3 for 6 rooms and 2 dwells
            room_tmp = i
            for j in range(int(dwells)):
                new_dwells[new_key].append(subtract)
                room_tmp -= subtract
            if room_tmp > 0:
                new_dwells[new_key][-1] += room_tmp

        # only divide SFHs and only take 30%, leave 70% at dwell_free
        for k in new_dwells.keys():
            # copy and only take 30%
            dwell_free_tmp = dwell_free_v.copy().loc[
                (dwell_free_v["building_type"] == "SFH") & (dwell_free_v["rooms"] == k)
            ]
            # leave 70% at original
            dwell_free_v.loc[
                (dwell_free_v["building_type"] == "SFH") & (dwell_free_v["rooms"] == k),
                "free_dwell",
            ] *= 0.7
            dwell_free_tmp["free_dwell"] *= 0.3

            # add the 30% to configurations with v rooms
            for v in new_dwells[k]:
                dwell_free_new_rooms = dwell_free_tmp.copy()
                dwell_free_new_rooms["rooms"] = str(v)
                dwell_free_v = pd.concat(
                    [dwell_free_v, dwell_free_new_rooms], ignore_index=True
                )

    dwell_free_v = (
        dwell_free_v.groupby(by=ip["cols_dwell"])["free_dwell"]
        .sum(numeric_only=True)
        .reset_index()
    )

    print(f"vacant dwellings: {dwell_free_v['free_dwell'].sum()}")

    dwell_free_v.set_index(ip["cols_dwell"], inplace=True)
    # only take complete dwellings into account
    dwell_free_v = dwell_free_v.round(decimals=0)
    # stay matrix for creation of next-year inhabit matrix
    stay_v = move_out_gen_v["stay"]
    # only take complete people into account
    stay_v = stay_v.round(decimals=0)
    # people that search a new flat, that move out
    # search_v = move_out_gen_v["search"]

    return stay_v, hh_search_v, dwell_free_v


def pref_current_quintile(inhabit_v, ip):
    """Create preference matrix from inhabit vector that focuses on the same quintile.
    The whole household-configuration stays the same. The people want to move to the
    same places as they already live.

    :param inhabit_v: inhabit vektor
    :type inhabit_v: dataframe
    :param ip: inputs
    :type ip: dict
    :return: preference matrix
    :rtype: dataframe
    """
    preferences_m = inhabit_v.unstack(ip["cols_dwell"])
    # take inhabit matrix and divide the number of households in each cell
    # by the total number of households in that row
    preferences_m = preferences_m.div(preferences_m.sum(axis="columns"), axis="index")
    preferences_v = preferences_m.stack(ip["cols_dwell"], future_stack=True)
    preferences_v = preferences_v.rename(
        columns={ip["col_weights"]: f"{ip['col_weights']}_pref"}
    )
    preferences_v.fillna(0, inplace=True)
    return preferences_v


def pref_quintile_above(inhabit_v, ip):
    """
    Function that calculates the preference matrix for the preference
    setting 'quintile above'
    Steps:
        1. Call the calc_shares function, that calculates the historic preferences.
        2. go through the quintiles, starting from the second highest,
            and copy preferences from the quintile above to it
            use .xs method of multiindex to access the quintile preferences
    Args:
        inhabit:
        cols_dwell:

    Returns:
        preference matrix with dimensions of the inhabit matrix

    """
    pref_base_v = pref_current_quintile(inhabit_v, ip)
    pref_base_m = pref_base_v.unstack(ip["cols_dwell"])
    preference_distribution_hh = pref_base_m.sum(axis=1)

    preferences_v = pref_base_v.copy()
    for quintile in range(1, 5):
        # print(f"processing quintile {quintile}")
        current_quintile_pref_hh_distribution = preference_distribution_hh.xs(
            f"q{quintile}", level=3
        )
        above_quintile_pref_hh_distribution = preference_distribution_hh.xs(
            f"q{quintile+1}", level=3
        )

        quintile_above_prefs_v = pref_base_v.xs(
            f"q{quintile + 1}", level=3
        )  # filter for entries of the above quintile

        # iterate over all entries, generate index tuple and access index via
        # .loc[index] to write new preferences
        for i, value in enumerate(quintile_above_prefs_v["weights_pref"]):
            # get index tuple of entry as list; list is used because quintile has to be
            # inserted at the right place
            index = list(quintile_above_prefs_v.index[i])
            if not (
                current_quintile_pref_hh_distribution[tuple(index[:-5])] == 1
                and above_quintile_pref_hh_distribution[tuple(index[:-5])] == 0
            ):
                index.insert(3, f"q{quintile}")  # insert quintile to index list
                preferences_v.loc[tuple(index)] = value

    return preferences_v


def pref_q4_aspiration(inhabit_v, ip):
    """
    Function that calculates the preference matrix for the preference
    setting 'quitile 4 aspiration', that is a mix of "current quintile" for all except
    from q4 that has the preference quintile above
    Steps:
        1. Call the calc_shares function, that calculates the historic preferences.
        2. go through the quintiles, starting from the second highest,
            and copy preferences from the quintile above to it
            use .xs method of multiindex to access the quintile preferences
    Args:
        inhabit:
        cols_dwell:

    Returns:
        preference matrix with dimensions of the inhabit matrix

    """
    pref_base_v = pref_current_quintile(inhabit_v, ip)
    pref_base_m = pref_base_v.unstack(ip["cols_dwell"])
    preference_distribution_hh = pref_base_m.sum(axis=1)

    preferences_v = pref_base_v.copy()
    for quintile in [4]:
        # print(f"processing quintile {quintile}")
        current_quintile_pref_hh_distribution = preference_distribution_hh.xs(
            f"q{quintile}", level=3
        )
        above_quintile_pref_hh_distribution = preference_distribution_hh.xs(
            f"q{quintile+1}", level=3
        )

        quintile_above_prefs_v = pref_base_v.xs(
            f"q{quintile + 1}", level=3
        )  # filter for entries of the above quintile

        # iterate over all entries, generate index tuple and access index via
        # .loc[index] to write new preferences
        for i, value in enumerate(quintile_above_prefs_v["weights_pref"]):
            # get index tuple of entry as list; list is used because quintile has to be
            # inserted at the right place
            index = list(quintile_above_prefs_v.index[i])
            if not (
                current_quintile_pref_hh_distribution[tuple(index[:-5])] == 1
                and above_quintile_pref_hh_distribution[tuple(index[:-5])] == 0
            ):
                index.insert(3, f"q{quintile}")  # insert quintile to index list
                preferences_v.loc[tuple(index)] = value

    return preferences_v


def pref_no_underoccupation(inhabit_v, ip):
    """
    Function that calculates the preference matrix for the preference
    setting 'no_preferred_underoccupation', where the preferences "current_quintile" are
    manipulated, so that househods want prefere dwellings with a maximum number of
    rooms of hh_hize+1

        1. Call the calc_shares function, that calculates the historic preferences.
        2. go trough the household sizes
            go trough the room sizes larger than hh_size+1
            for all entries add the preference factor to the dwelling size without
            underoccupation. And Overwrite the entry with 0.
    Args:
        inhabit:
        cols_dwell:

    Returns:
        preference matrix with dimensions of the inhabit matrix

    """
    pref_base_v = pref_current_quintile(inhabit_v, ip)
    preferences_v = pref_base_v.copy()

    def get_next_higher_room_size(hh_size):
        higher_size = int(hh_size[0]) + 1
        if higher_size == int(room_numbers[-1][0]):
            return f"{higher_size}+"
        else:
            return str(higher_size)

    hh_sizes = [f"{x}" for x in range(1, ip["max_hh_size"])]
    hh_sizes.append(f"{ip['max_hh_size']}+")
    room_numbers = arange_room_nos_list(ip["max_rooms"])
    pref_base_v_nonzero = pref_base_v[pref_base_v["weights_pref"] != 0]

    # iterate over hh sizes
    for hh_size_index, hh_size in enumerate(hh_sizes):
        # print(
        #    f'Current hhsize: {hh_size}, max dwelling size without underoccupation:
        #    {get_next_higher_room_size(hh_size)}')

        # get all entries from hh_size from enumeration
        # dangerous to work with .xs - once the order changes or new disaggs appear,
        # one has to rework on this piece of code to keep it working. Probably change
        # this in future.
        hh_size_extract = pref_base_v_nonzero.xs(hh_size, level=1)

        # iterate over dwelling sizes
        hh_size_index_plus = hh_size_index + 2
        for room_number in room_numbers[hh_size_index_plus:]:
            room_number_extract = hh_size_extract.xs(room_number, level=7)
            for index_number, index_entry in enumerate(room_number_extract.index):
                # convert index tuple to list that it can be manipulated
                index_as_list = list(index_entry)
                # insert hh_size and maximum dwelling_size without underoccupation to
                #  index list
                index_as_list.insert(1, hh_size)
                index_as_list.insert(8, get_next_higher_room_size(hh_size))
                # add factor of higher room number to entry withput underoccupation
                preferences_v.loc[tuple(index_as_list)] += room_number_extract.iloc[
                    index_number
                ]
                # overwrite high room number entry with zero
                index_as_list.pop(8)
                index_as_list.insert(8, room_number)
                preferences_v.loc[tuple(index_as_list)] = 0

        hh_size_index_minus = hh_size_index - 1
        if hh_size_index_minus >= 0:
            hh_size_extract = pref_base_v_nonzero.xs(hh_size, level=1)

            for room_number in room_numbers[:hh_size_index_minus]:

                try:
                    room_number_extract = hh_size_extract.xs(room_number, level=7)
                    for index_number, index_entry in enumerate(
                        room_number_extract.index
                    ):
                        # convert index tuple to list that it can be manipulated
                        index_as_list = list(index_entry)
                        # insert hh_size and maximum dwelling_size without
                        #   underoccupation to index list
                        index_as_list.insert(1, hh_size)
                        index_as_list.insert(8, get_next_higher_room_size(hh_size))

                        preferences_v.loc[
                            tuple(index_as_list)
                        ] += room_number_extract.iloc[index_number]
                        # overwrite high room number entry with zero
                        index_as_list.pop(8)
                        index_as_list.insert(8, room_number)
                        preferences_v.loc[tuple(index_as_list)] = 0
                except KeyError:
                    pass

    # preferences_v.loc[preferences_v["weights_pref"] > 0].to_csv("pref.csv")
    # exit(1)
    return preferences_v


def other_handler(option1, option2, changed_dwellings):
    if option1 in changed_dwellings:
        return [option2 if x == option1 else x for x in changed_dwellings]
    else:
        return [option1 if x == option2 else x for x in changed_dwellings]


def standard_handler(check_value, check_list, changed_dwellings):
    return [(check_value if x in check_list else x) for x in changed_dwellings]


def get_osciallating_vals(changed_dwellings, dwell_order, ip):
    """Get next osciallating value for rooms.

    :param changed_dwellings: Dwelling parameters to be changed
    :type changed_dwellings: list
    :param osci_i: rising number. Declare how far will be oscillated
    :type osci_i: int
    :param dwell_order: input parameter. Either oscillate_larger, or ..._lower
    :type dwell_order: string
    :return: arg1 and arg2 for other handler. Arg1 is to be replaced by arg2.
    :rtype: string
    """
    # calculate all possibilities and return as list
    possible_rooms = [f"{x}" for x in range(1, ip["max_rooms"])]
    max_room = f"{ip['max_rooms']}+"
    possible_rooms.append(max_room)
    current_room, current_original = 0, 0
    for room in possible_rooms:
        if room in changed_dwellings:
            current_room = room
            current_original = room
            break
    if current_room == max_room:
        current_room = ip["max_rooms"]
    current_room = int(current_room)
    add_sub = None
    if dwell_order == "oscillate_larger":
        add_sub = [[x, -x] for x in range(1, ip["max_rooms"])]
    elif dwell_order == "oscillate_lower":
        add_sub = [[-x, x] for x in range(1, ip["max_rooms"])]
    all_combinations = []
    for ad_su in add_sub:
        for a_s in ad_su:
            tmp_room = current_room + a_s
            if tmp_room == ip["max_rooms"]:
                all_combinations.append([f"{current_original}", max_room])
            elif tmp_room < ip["max_rooms"] and tmp_room > 0:
                all_combinations.append([f"{current_original}", f"{tmp_room}"])
    return all_combinations


def get_next_growth_type(changed_dwellings, grower):
    if grower == 0:
        if "shrinking" in changed_dwellings or "growing" in changed_dwellings:
            return "neither"
        else:
            return "growing"
    else:
        if "shrinking" in changed_dwellings:
            return "growing"
        else:
            return "shrinking"


def get_alloc_dwell_order(
    ip, needed_dwellings, dwell_free_v, hh_configuration, alloc_limiter
):
    all_cases = {
        "condition": {
            "other": {
                "handler": other_handler,
                "arg1": "renovated",
                "arg2": "not renovated",
            },
            "default": {
                "handler": standard_handler,
                "arg1": -1,
                "arg2": ["renovated", "not renovated"],
            },
        },
        "rooms": {
            "oscillate_larger": {"handler": other_handler, "arg1": None, "arg2": None},
            "oscillate_lower": {"handler": other_handler, "arg1": None, "arg2": None},
        },
        "ownership": {
            "other": {
                "handler": other_handler,
                "arg1": "private owner",
                "arg2": "private tenant",
            },
            "default": {
                "handler": standard_handler,
                "arg1": None,
                "arg2": -2,
            },
        },
        "building_type": {
            "other": {"handler": other_handler, "arg1": "SFH", "arg2": "MFH"},
            "default": {
                "handler": standard_handler,
                "arg1": -1,
                "arg2": ["SFH", "MFH"],
            },
        },
    }
    region_type_cases = {
        "region_type": {
            "other": {"handler": other_handler, "arg1": "rural", "arg2": "urban"},
            "default": {
                "handler": standard_handler,
                "arg1": -1,
                "arg2": ["rural", "urban"],
            },
        }
    }
    growth_type_cases = {
        "growth_type": {
            "next": {
                "handler": standard_handler,
                "arg1": None,  # special case, see below
                "arg2": ["growing", "neither", "shrinking"],
            },
            "default": {
                "handler": standard_handler,
                "arg1": -1,
                "arg2": ["neither", "shrinking", "growing"],
            },
        }
    }
    if ip["growth_type"]:
        all_cases |= growth_type_cases  # requires python 3.9, appends dicts
    if ip["region_type"]:
        all_cases |= region_type_cases

    # parameters
    room_all_args = None
    osci_i = 0
    grower = 0
    growth = False
    oscillator = False  # for the oscillating case in rooms
    ownership = False
    own_i = 0
    changed_dwellings = []
    all_changed_dwellings = [needed_dwellings]
    all_checked_dwellings = []
    large_uo_option = -1
    use_large_uo = False
    i = 1
    while True:
        # check for entry. If no entry in input file exists, break out of while loop
        try:
            # shorter names for better code reading
            dwell_var = ip[f"alloc_dwell_prio_{i}_feature"]
            dwell_order = ip[f"alloc_dwell_prio_{i}_feature_order"]
        except KeyError:
            break

        # continue if region type or growth type are not set
        if dwell_var == "region_type" and not ip["region_type"]:
            i += 1
            continue
        elif dwell_var == "growth_type" and not ip["growth_type"]:
            i += 1
            continue

        changed_dwellings = needed_dwellings

        try:
            cases = all_cases[dwell_var][dwell_order]
        except KeyError:  # key not available - use default entry
            cases = all_cases[dwell_var]["default"]
        if cases["arg1"] == -1:
            cases["arg1"] = dwell_order
        if cases["arg2"] == -2:
            cases["arg2"] = dwell_order.split(", ")
        handler = cases["handler"]
        arg1 = cases["arg1"]
        arg2 = cases["arg2"]
        # handle special cases
        if ip["growth_type"] and dwell_var == "growth_type" and dwell_order == "next":
            growth = True
            arg1 = get_next_growth_type(changed_dwellings, grower)
        elif dwell_var == "ownership" and ip["dwelling_ownership_short"] == "false":
            ownership = True
            arg1 = dwell_order.split(", ")[own_i]
        elif dwell_var == "rooms":
            oscillator = True
            if room_all_args is None:
                room_all_args = get_osciallating_vals(
                    changed_dwellings, dwell_order, ip
                )
            arg1, arg2 = room_all_args[osci_i]

        # calling the handler with the arguments
        changed_dwellings = tuple(handler(arg1, arg2, changed_dwellings))
        # now check for free space and else change config
        # found free space in the new configuration. Return to allocate it
        # check for underoccupation to be able to return
        underocc = get_underoccupation(ip, changed_dwellings, hh_configuration)
        if float(dwell_free_v.loc[changed_dwellings].values[0]) > 0:
            if underocc <= alloc_limiter:
                # TODO: update unhappyness
                unhappyness = misc.get_unhappyness(
                    ip, needed_dwellings, changed_dwellings
                )
                return changed_dwellings, unhappyness, use_large_uo
            elif large_uo_option == -1:
                large_uo_option = changed_dwellings
        else:
            # append changed dwellings and test them
            for idx in range(len(all_changed_dwellings)):
                appender = tuple(handler(arg1, arg2, all_changed_dwellings[idx]))
                if appender not in all_changed_dwellings:
                    all_changed_dwellings.append(appender)

            # iterating combinations of changed_dwellings. if one has space free: use it
            for combination in all_changed_dwellings:
                if combination not in all_checked_dwellings:
                    if float(dwell_free_v.loc[combination].values[0]) > 0:
                        # check for underoccupation to be able to return
                        underocc = get_underoccupation(
                            ip, combination, hh_configuration
                        )
                        if underocc <= alloc_limiter:
                            unhappyness = misc.get_unhappyness(
                                ip, needed_dwellings, combination
                            )
                            return combination, unhappyness, use_large_uo
                        elif large_uo_option == -1:
                            large_uo_option = combination
                    else:
                        all_checked_dwellings.append(combination)

        # no combination has been found. Change settings/next category
        # oscillate for varying rooms
        if oscillator:
            osci_i += 1
            # after all possible rounds continue to next iteration
            if osci_i >= ip["max_rooms"] - 1:
                i += 1
                oscillator = False
        # ownership variations
        elif ownership:
            max_ownership = 3
            own_i += 1
            if own_i >= max_ownership:  # only 2/3 variants - continue to next iteration
                i += 1
                ownership = False
        elif growth:
            grower += 1
            if grower >= 2:
                i += 1
                growth = False
        # continue to next iteration
        else:
            i += 1

    # If this point is reached, underoccupation regulations made many people homeless
    # return -1, -1, use_large_uo
    # allow large underoccupation before sending people on the street
    if large_uo_option == -1:
        unhappyness = -1
    else:
        unhappyness = misc.get_unhappyness(ip, needed_dwellings, large_uo_option)
        use_large_uo = True

    return large_uo_option, unhappyness, use_large_uo


def get_underoccupation(ip, needed_dwellings, hh_configuration):
    all_rooms = [str(x) for x in range(ip["max_rooms"])]
    all_rooms.append(f'{ip["max_rooms"]}+')
    all_hh_size = [str(x) for x in range(ip["max_hh_size"])]
    all_hh_size.append(f'{ip["max_hh_size"]}+')
    # calculate underoccupation and check if it is in allowed range
    # take onliest element from list ([0]) and only first digit ([0]),
    # to ignore the + if it occurs. Transform to integer for calculation
    # of underoccupation
    room = int([x for x in all_rooms if x in needed_dwellings][0][0])
    hh_size = int([x for x in all_hh_size if x in hh_configuration][0][0])
    return room - hh_size


@misc.timer_func
def allocation_loop(
    alloc_rate_it, move_in_want_v, ip, dwell_free_v, move_in_v, u_r, alloc_limiter
):
    # {config: [#moved_people, unhappyness], [...], ...}
    # unhappyness = 0 means match according to preferences
    unhappy_index = {}
    unhappy_final = {}
    len_hh = len(ip["cols_hh"])
    homeless = 0
    homeless_hh_configs = []
    use_large_uo = False
    forced_move_in_v = move_in_v.copy()
    for ix in range(len(alloc_rate_it)):
        # iterate row by row to check for each config if dwell_free still available.
        # if it is available: delete from dwell_free, add to move_in
        # TODO: improve performance, avoid for loop!!
        for row in move_in_want_v.itertuples():
            searchers = int(getattr(row, f"weights_pref_{str(ix)}"))
            # the household that wants to move. Add in the following dwelling type
            hh_configuration = row[0][:len_hh]

            # allocate searchers to configuration
            needed_dwellings = row[0][len_hh:]
            unhappyness = 0
            while searchers > 0:
                free_dwellings = int(dwell_free_v.loc[needed_dwellings].values[0])
                underocc = get_underoccupation(ip, needed_dwellings, hh_configuration)
                # number households that move into the final_configuration
                final_move_in = 0
                if free_dwellings > 0 and ((underocc <= alloc_limiter) or use_large_uo):
                    final_configuration = needed_dwellings + hh_configuration
                    if (free_dwellings - searchers) >= 0:
                        dwell_free_v.loc[needed_dwellings, ["free_dwell"]] -= searchers
                        final_move_in = searchers
                        searchers = 0
                    else:
                        searchers -= free_dwellings
                        dwell_free_v.loc[needed_dwellings] = 0
                        final_move_in = free_dwellings
                    # add to existing entries.
                    move_in_v.loc[
                        final_configuration, [ip["col_weights"]]
                    ] += final_move_in
                    if use_large_uo:
                        forced_move_in_v.loc[
                            final_configuration, [ip["col_weights"]]
                        ] += final_move_in
                    use_large_uo = False

                    if final_configuration not in unhappy_index.keys():
                        unhappy_index[final_configuration] = [
                            [
                                final_move_in,
                                unhappyness,
                            ]
                        ]
                    else:
                        unhappy_index[final_configuration].append(
                            [final_move_in, unhappyness]
                        )
                    unhappyness = 0
                elif dwell_free_v["free_dwell"].sum() > 0:
                    # check alloc_dwell_order for free dwellings with new conf.
                    needed_dwellings, unhappyness, use_large_uo = get_alloc_dwell_order(
                        ip,
                        needed_dwellings,
                        dwell_free_v,
                        hh_configuration,
                        alloc_limiter,
                    )
                    if needed_dwellings == -1 and unhappyness == -1:
                        homeless += searchers
                        searchers = 0
                        homeless_hh_configs.append(hh_configuration)
                else:
                    homeless += searchers
                    searchers = 0
                    homeless_hh_configs.append(hh_configuration)

        # add unhappyness percentwise together per configuration
        unhappy_final = misc.add_unhappyness(unhappy_final, unhappy_index)
    homeless_msg = f"{homeless} people found no new place to stay in {u_r} area."
    misc.debug_messages(homeless_msg, ip)
    print(homeless_msg)
    if dwell_free_v["free_dwell"].sum() > 0:
        if homeless > 0:
            print(
                f'{homeless} people are homeless while \
                {dwell_free_v["free_dwell"].sum()} dwellings are still free.\
                This comes due to strict underoccupation prohibition.'
            )
            print("homeless configurations")
            print(homeless_hh_configs)

        print(f'leftover free dwellings: {dwell_free_v["free_dwell"].sum()}')
        print(dwell_free_v.loc[dwell_free_v["free_dwell"] > 0])

        # print("Exiting. No way that people are homeless while dwellings are free.")
        # raise

    return unhappy_final, move_in_v, forced_move_in_v


def get_regtype(df, regtyp, to_group, hh_dwell="dwell"):
    df = df.reset_index()
    df = df.loc[df[f"region_type_{hh_dwell}"] == regtyp]
    # df = df.sort_index()
    df = df.set_index(to_group)
    return df


@misc.timer_func
def allocation(ip, inhabit_v, mor_v, year):
    """Allocate searching households to free dwellings."""
    # the dict's key is the variants name, its value is the function that calculates the
    # variant
    save_path = misc.get_save_path(ip["output"], ip["alloc_output_path"])
    pref_variants = {
        "current_quintile": pref_current_quintile,
        "quintile_above": pref_quintile_above,
        "q4_aspiration": pref_q4_aspiration,
        "no_preferred_underoccupation": pref_no_underoccupation,
    }

    # default settings before scenario starts
    scenario_inputs = [
        "preferences_variant",
        "split_large_dwellings",
        "prohibit_underoccupation",
    ]
    if year >= ip["scenario_start_year"]:
        scenario_inputs = [f"scenario_{x}" for x in scenario_inputs]

    pref_v = pref_variants[ip[scenario_inputs[0]]](inhabit_v, ip)
    pref_v_save = pref_v.copy()
    pref_v_save = pref_v_save.loc[
        pref_v_save["weights_pref"] > 0
        ]
    pref_v_save.to_csv(
        os.path.join(
            ip["output"],
            ip["alloc_output_path"],
            f"preferences_{year}.csv",
        )
    )

    split_large_dwell = ip[scenario_inputs[1]]

    alloc_limiter = 1000
    if ip[scenario_inputs[2]] != "no_prohibition":
        alloc_limiter = int(ip[scenario_inputs[2]][1:])

    # calculate stay and search vectors as well als marginal distriubtions
    stay_v, hh_search_v, dwell_free_v = calc_stay_search(
        inhabit_v, mor_v, ip, split_large_dwell
    )

    # BSI calculation
    # get_bsi(pref_v.copy(), ip, hh_search_v, dwell_free_v, year)

    # Allocation
    pref_m = pref_v.unstack(ip["cols_dwell"])
    move_in_want_m = pref_m.mul(hh_search_v["search"], axis="index")
    move_in_want_v = move_in_want_m.stack(ip["cols_dwell"], future_stack=True)

    # only focus on the entries where people want to move
    move_in_want_v = move_in_want_v.loc[move_in_want_v["weights_pref"] > 0]

    # e.g. [20, 20, 20, 20, 20], percentage to process move_in_want_v iteration-wise
    alloc_rate_it = [int(x) for x in ip["alloc_rate_iteration"].split(", ")]
    for ix, percentile in enumerate(alloc_rate_it):
        move_in_want_v[f"weights_pref_{str(ix)}"] = (
            move_in_want_v["weights_pref"] * percentile / 100
        )
        # if weights_pref smaller 0 take original value to the first round and set
        # the other rounds to 0
        if ix > 0:
            move_in_want_v.loc[
                move_in_want_v[f"weights_pref_{str(ix)}"] < 0.5,
                f"weights_pref_{str(ix)}",
            ] = 0
            move_in_want_v.loc[
                move_in_want_v["weights_pref_0"] < 0.5, "weights_pref_0"
            ] = move_in_want_v.loc[
                move_in_want_v["weights_pref_0"] < 0.5, "weights_pref"
            ]
    # only take complete households into account
    move_in_want_v = move_in_want_v.round(decimals=0)
    # now add or sub at the first round some decimals that the sum of all weights_prefs
    # fit to the total weights_pref of the line
    move_in_want_v["sum_weights"] = 0
    for ix in range(len(alloc_rate_it)):
        move_in_want_v["sum_weights"] += move_in_want_v[f"weights_pref_{str(ix)}"]
    move_in_want_v["sum_weights"] -= move_in_want_v["weights_pref"]
    move_in_want_v["weights_pref_0"] -= move_in_want_v["sum_weights"]
    move_in_want_v = move_in_want_v.drop(["sum_weights"], axis=1)
    move_in_want_v = move_in_want_v.loc[move_in_want_v["weights_pref"] > 0]

    # no need for additional columns in the saved version
    move_in_want_v_save = move_in_want_v.copy()

    move_in_v = inhabit_v.copy()
    move_in_v[ip["col_weights"]] = 0.0

    # order move in want to order specified in inputs.csv
    move_in_want_v = misc.order_move_in_want(move_in_want_v, ip)

    # parallelize when people cannot leave urban/rural areas. If they can, don't
    if ip["region_type"]:
        # @TODO: fix bug
        unhappy_final, move_in_v = allocation_loop(
            alloc_rate_it,
            move_in_want_v,
            ip,
            dwell_free_v,
            move_in_v,
            "rural + urban",
            alloc_limiter,
        )
    else:
        move_in_want_rur_v = get_regtype(
            move_in_want_v, "rural", [*ip["cols_hh"], *ip["cols_dwell"]]
        )
        move_in_want_urb_v = get_regtype(
            move_in_want_v, "urban", [*ip["cols_hh"], *ip["cols_dwell"]]
        )
        # order move in want to order specified in inputs.csv
        move_in_want_rur_v = misc.order_move_in_want(move_in_want_rur_v, ip)
        move_in_want_urb_v = misc.order_move_in_want(move_in_want_urb_v, ip)

        dwell_free_rur_v = get_regtype(dwell_free_v, "rural", ip["cols_dwell"])
        dwell_free_urb_v = get_regtype(dwell_free_v, "urban", ip["cols_dwell"])
        move_in_rur_v = get_regtype(move_in_v, "rural", ip["to_group_col"])
        move_in_urb_v = get_regtype(move_in_v, "urban", ip["to_group_col"])

        # print("______analytics_______")
        # print("____urban_____")
        sum_miw_u = sum(
            [
                move_in_want_urb_v[f"weights_pref_{alloc}"].sum()
                for alloc in range(len(alloc_rate_it))
            ]
        )
        sum_df_u = dwell_free_urb_v["free_dwell"].sum()
        # print(f"sum(move_int_want) = {sum_miw_u}")
        # print(f"sum(dwell_free) = {sum_df_u}")
        # print(f"difference = {sum_miw_u - sum_df_u}(>0: homeless,<0: free dwellings)")
        # print("____rural_____")
        sum_miw_r = sum(
            [
                move_in_want_rur_v[f"weights_pref_{alloc}"].sum()
                for alloc in range(len(alloc_rate_it))
            ]
        )
        sum_df_r = dwell_free_rur_v["free_dwell"].sum()
        # print(f"sum(move_int_want) = {sum_miw_r}")
        # print(f"sum(dwell_free) = {sum_df_r}")
        # print(f"difference = {sum_miw_r - sum_df_r}(>0: homeless,<0: free dwellings)")
        # print("____together_____")
        sum_miw = move_in_want_v["weights_pref"].sum()
        sum_df = dwell_free_v["free_dwell"].sum()
        # print(f"sum(move_int_want) = {sum_miw}")
        # print(f"sum(dwell_free) = {sum_df}")
        # print(f"difference = {sum_miw - sum_df} (>0: homeless, <0: free dwellings)")
        misc.debug_messages("______analytics_______", ip)
        misc.debug_messages("____urban_____", ip)
        misc.debug_messages(f"sum(move_int_want) = {sum_miw_u}", ip)
        misc.debug_messages(f"sum(dwell_free) = {sum_df_u}", ip)
        misc.debug_messages(
            f"difference = {sum_miw_u - sum_df_u} (>0: homeless, <0: free dwellings)",
            ip,
        )
        misc.debug_messages("____rural_____", ip)
        misc.debug_messages(f"sum(move_int_want) = {sum_miw_r}", ip)
        misc.debug_messages(f"sum(dwell_free) = {sum_df_r}", ip)
        misc.debug_messages(
            f"difference = {sum_miw_r - sum_df_r} (>0: homeless, <0: free dwellings)",
            ip,
        )
        misc.debug_messages("____together_____", ip)
        misc.debug_messages(f"sum(move_int_want) = {sum_miw}", ip)
        misc.debug_messages(f"sum(dwell_free) = {sum_df}", ip)
        misc.debug_messages(
            f"difference = {sum_miw - sum_df} (>0: homeless, <0: free dwellings)", ip
        )

        with concurrent.futures.ThreadPoolExecutor() as executor:

            alloc_rural = executor.submit(
                allocation_loop,
                alloc_rate_it,
                move_in_want_rur_v,
                ip,
                dwell_free_rur_v,
                move_in_rur_v,
                "rural",
                alloc_limiter,
            )

            alloc_urban = executor.submit(
                allocation_loop,
                alloc_rate_it,
                move_in_want_urb_v,
                ip,
                dwell_free_urb_v,
                move_in_urb_v,
                "urban",
                alloc_limiter,
            )
        unhappy_rur, move_in_rur_v, forced_move_in_rur_v = alloc_rural.result()
        unhappy_urb, move_in_urb_v, forced_move_in_urb_v = alloc_urban.result()
        unhappy_final = unhappy_urb | unhappy_rur
        move_in_v = pd.concat([move_in_rur_v, move_in_urb_v])
        forced_move_in_v = pd.concat([forced_move_in_rur_v, forced_move_in_urb_v])
        forced_move_in_v = forced_move_in_v.loc[forced_move_in_v["weights"] > 0,]
        if len(forced_move_in_v) > 0:
            misc.heatmap(
                forced_move_in_v,
                year,
                save_path,
                ip,
                heat_name=f"forced_move_in in {year}",
            )
            forced_move_in_v.to_csv(
                os.path.join(save_path, f"forced_move_in_{year}.csv")
            )
    # add unhappy final together to get final unhappyness for each config:
    unhappy = misc.add_unhappyness({}, unhappy_final)
    unhappy = {key: value[0] for key, value in unhappy.items()}
    unhappy = {
        "index": list(unhappy.keys()),
        "columns": ["distributed_people", "unhappyness"],
        "data": list(unhappy.values()),
        "index_names": ip["to_group_col"],
        "column_names": ["unhappy_index"],
    }
    unhappy_v = pd.DataFrame.from_dict(data=unhappy, orient="tight")
    unhappy_v = pd.DataFrame(unhappy_v["unhappyness"])

    # merge move_in_v with stay_v to get new inhabit matrix
    inhabit_new_v = pd.DataFrame(stay_v).merge(move_in_v, on=ip["to_group_col"])
    inhabit_new_v[ip["col_weights"]] += inhabit_new_v["stay"]
    inhabit_new_v = inhabit_new_v.drop(["stay"], axis=1)
    if ip["reduce_save"]:
        move_in_v = move_in_v.loc[move_in_v[ip["col_weights"]] > 0]
        stay_v = stay_v.loc[stay_v > 0]
    stay_v.rename(ip["col_weights"], inplace=True)
    move_in_v.to_csv(os.path.join(save_path, f"move_in_created_{year}.csv"))
    stay_v.to_csv(os.path.join(save_path, f"stay_created_{year}.csv"))

    move_in_want_v_save[ip["col_weights"]] = move_in_want_v_save["weights_pref"]
    # move_in_want_v_save = move_in_want_v_save[ip["col_weights"]]
    move_in_want_v_save.to_csv(
        os.path.join(save_path, f"move_in_want_created_{year}.csv")
    )

    return inhabit_new_v, unhappy_v
