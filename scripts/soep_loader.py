"""Load soep data"""

import os
import numpy as np
import pandas as pd

try:
    import scripts.misc as misc
except ModuleNotFoundError:
    import misc
# List of all variables we use as a source to fill wum1
WUM1_NEW_MOVED_2021 = ["wum1_hbrutt", "hlf0154_h", "housing1", "wum1_updated"]


# data: hhgr for 2021: interviewer dataset
# data: cov_brutto hhgr for 2020


def attachVariable(
    soep_path, add_vars, merge_from, merge_to, match_keys, new_names=None
):
    """Merge add_vars+match_keys from merge_from to merge_to by match_keys.

    Parameters:
        - soep_path: path to soep folder respectively the data folder
        - add_vars: list of variable names to merge to Dataframe
        - merge_from: Dataframe to be loaded, name of the dataset to merge from
        - merge_to: Dataframe, base dataset to merge variables to (e.g. df)
        - match_keys: list of variable names to use as keys for merging datasets
        - new_names: directly rename variables before merging them

    Returns:
        - updated dataset "add_to" (e.g. df) with new variables

    Attention: Should be run only once.
    """
    data_new = pd.read_csv(
        os.path.join(soep_path, f"{merge_from}.csv"),
        usecols=add_vars + match_keys,
        keep_default_na=False,
        na_values=[""],
    )
    if new_names:
        for x, y in zip(add_vars, new_names):
            data_new.rename({x: y}, axis=1, inplace=True)

    merged = pd.merge(merge_to, data_new, on=match_keys, how="left")
    return merged


# @TODO: update all of this
@misc.timer_func
def update_wum1(df, soep_path, wum_path):
    """Calculate wum1 for years 2019, 2020 and 2021.

    1. Use the information from hbrutt (new households) and hl (moved hh)
    and housing2021 (2021) to fill the NaN values in wum1.
    2. For old households, use the value from the previous year for the same hid.
    3. Save the updated values in output/wum1_updated.csv
    """
    # @TODO: check wum1 whether we really use them correctly
    # attach combined building type variable valid until 2018
    df = attachVariable(
        soep_path=soep_path,
        add_vars=["wum1"],
        merge_from="hbrutto",
        merge_to=df,
        match_keys=["hid", "syear"],
        new_names=[WUM1_NEW_MOVED_2021[3]],  # "wum1_updated"]
    )

    # attach building type variable for new households
    df = attachVariable(
        soep_path=soep_path,
        add_vars=["wum1"],
        merge_from="hbrutt",
        merge_to=df,
        match_keys=["hid", "syear"],
        new_names=[WUM1_NEW_MOVED_2021[0]],  # "wum1_hbrutt"],
    )

    # attach building type variable for moved households
    df = attachVariable(
        soep_path=soep_path,
        add_vars=["hlf0154_h"],
        merge_from="hl",
        merge_to=df,
        match_keys=["hid", "syear"],
        new_names=[WUM1_NEW_MOVED_2021[1]],  # "hlf0154_h"],
    )

    # attach building type variable for 2021
    df = attachVariable(
        soep_path=soep_path,
        add_vars=["housing1"],
        merge_from="raw/housing2021",
        merge_to=df,
        match_keys=["hid", "syear"],
        new_names=[WUM1_NEW_MOVED_2021[2]],  # "housing1"],
    )

    negatives = misc.get_negative_dict()
    negatives.update({i: i for i in range(1, 9)})

    # Sort the DataFrame by 'syear' in ascending order
    df = df.sort_values(by="syear", kind="mergesort")

    for col in WUM1_NEW_MOVED_2021:
        df.replace({col: negatives}, inplace=True)

    # Initialize a dict to store the previous year's 'wum1' values for each 'hid'
    prev_year_wum1 = {}
    prev_year_wum1_temp = {}

    # prioritize non-NaN values from wum1_new_moved_2021
    def prioritize_values(row):
        for col in [
            el for el in WUM1_NEW_MOVED_2021
        ]:  # if el != WUM1_NEW_MOVED_2021[3]]:
            if not pd.isna(row[col]):
                return row[col]
        return np.nan  # If all columns are NaN, return NaN

    cur_yr = 2018
    counter = 0
    # Iterate through each row in the DataFrame
    for index, row in df.iterrows():
        # Get the 'hid' and 'syear' values for the current row
        hid = row["hid"]
        syear = row["syear"]

        # Update 'wum1_updated' with the prioritized values from wum1_new_moved_2021
        df.at[index, WUM1_NEW_MOVED_2021[3]] = prioritize_values(row)
        # Fill prev_year dictionary with values from starting year 2018 for 2019
        if syear == 2018:
            prev_year_wum1_temp[hid] = row[WUM1_NEW_MOVED_2021[3]]

        # 2019 is the first year where we fill the values in wum1
        elif syear >= 2019:
            # first switch in years
            if syear > cur_yr:
                # print for prev year
                print(f"values in {cur_yr} reused from {cur_yr-1}: {counter}")
                counter = 0
                cur_yr = syear
                # copy temp dict
                prev_year_wum1 = prev_year_wum1_temp.copy()
                prev_year_wum1_temp = {}

            # If 'wum1_updated' is still NaN:
            # fill it with the previous year's 'wum1' for the same 'hid'
            if pd.isna(df.at[index, WUM1_NEW_MOVED_2021[3]]):
                # Check if the 'hid' has a previous year's 'wum1' value in the dict
                if hid in prev_year_wum1:
                    counter += 1
                    df.at[index, WUM1_NEW_MOVED_2021[3]] = prev_year_wum1[hid]

            else:
                # Update the previous year's 'wum1' value for the current 'hid'
                # for the next iteration
                prev_year_wum1_temp[hid] = row[WUM1_NEW_MOVED_2021[3]]

    print(f"values in {syear} reused from {syear-1}: {counter}")
    df = df[["hid", "syear", WUM1_NEW_MOVED_2021[3]]]
    df = misc.clean_nan(df)

    # save results to CSV, so it's not necessary to calculate the updated wum1 again
    df.to_csv(wum_path, index=False)


@misc.timer_func
def csv_gen_households(ip):
    """Generate main df to save to disk from different csv's.
    Merge of all needed variables to only load the data that is needed. Households only.
    Merge from entspricht der csv datei
    Args:
        - ip: dataframe, contains user-input from csv and in-program added vars
        - hh_path: path and name of file to be saved
    """
    soep_path = ip["soep"]
    df = pd.read_csv(
        os.path.join(soep_path, "ppathl.csv"),
        usecols=[
            "pid",  # Never Changing Person ID
            "hid",  # household ID
            "syear",  # survery year
            "gebjahr",  # birth year, 4 digit
        ],
    )
    # variables for households
    col_type = "hh_type" if "hh_type" in ip["hh"].keys() else np.nan
    col_sizeclass = "hh_size" if "hh_size" in ip["hh"].keys() else np.nan
    col_weights = ip["col_weights"]

    # Attach from hpathl:
    df = attachVariable(
        soep_path=soep_path,
        add_vars=["hnetto"],
        merge_from="hpathl",
        merge_to=df,
        match_keys=["hid", "syear"],
    )
    df = attachVariable(
        soep_path=soep_path,
        add_vars=["hgtyp1hh"],
        merge_from="hgen",
        merge_to=df,
        match_keys=["hid", "syear"],
        new_names=[col_type],
    )
    df = attachVariable(
        soep_path=soep_path,
        add_vars=["hhgr"],
        merge_from="hbrutto",
        merge_to=df,
        match_keys=["hid", "syear"],
        new_names=[col_sizeclass],
    )
    df = attachVariable(
        soep_path=soep_path,
        add_vars=["hghinc"],
        merge_from="hgen",
        merge_to=df,
        match_keys=["hid", "syear"],
    )
    # Attach from hpathl:
    # hhrf: weigthing factor by household
    df = attachVariable(
        soep_path=soep_path,
        add_vars=["hhrf"],
        merge_from="hpathl",
        merge_to=df,
        match_keys=["hid", "syear"],
        new_names=[col_weights],
    )
    # same as in dwelling for household-region type as well
    col_regio = "region_type"
    df = attachVariable(
        soep_path=soep_path,
        add_vars=["regtyp"],
        merge_from="regionl",
        merge_to=df,
        match_keys=["hid", "syear"],
        new_names=[col_regio],
    )
    # call and save the movers for each year
    move_path = os.path.join(ip["soep_composita_path"], ip["move"]["csv_move"])
    if not os.path.exists(move_path):
        calc_will_move(ip, df)

    df = attachVariable(
        soep_path=ip["soep_composita_path"],
        add_vars=[ip["move"]["col_will_move"]],
        merge_from="will_move",
        merge_to=df,
        match_keys=["pid", "syear"],
        new_names=[ip["move"]["col_will_move"]],
    )
    df.loc[df[ip["move"]["col_will_move"]] != 1, ip["move"]["col_will_move"]] = 0

    return df


@misc.timer_func
def csv_gen_dwellings(ip):
    """Generate main df to save to disk from different csv's.
    Merge of all needed variables to only load the data that is needed. Dwellings and
    Wum only. Calls update_wum1 to generate also the wum-csv.
    Args:
        - ip: dataframe, contains user-input from csv and in-program added vars
        - dwelling_path: path and name of file to be saved
    """
    soep_path = ip["soep"]
    composita_path = ip["soep_composita_path"]
    df = pd.read_csv(
        os.path.join(soep_path, "hgen.csv"),
        usecols=[
            "hid",
            "syear",
            # "cid",  # Original Houshold Number, Case ID
            # "hgcnstyrmax",   # Latest Possible Construction Year of Dwelling
            # "hgcnstyrmin",   # Earliest Possible Construction Year of Dwelling
            "hgcondit",  # Condition of house. 1: good condition ... 4: dilapidated
            "hgowner",  # 1: Owner, 2: Main Tenant, 3: Sub T, 4: Tenant, 5: sharedacc
            "hgroom",  # Number of rooms larger than 6sqm
            # "hgsize",   # size of housing unit in sqm
            # "hgmoveyr"  # year moved into dwelling
            # "hgtyp1hh",   # Household Typology, 1 digit
            # "hgtyp2hh",   # Household Typology, 2 digit
        ],
    )

    df_2015 = df.loc[df["syear"] == 2015, ["hid", "syear", "hgcondit"]]
    df_2019 = df.loc[df["syear"] == 2019, ["hid", "syear", "hgcondit"]]

    for i in [2016, 2017, 2018, 2020, 2021]:
        df_201x = df_2015.copy()
        df_201y = df_2019.copy()
        df_201x["syear"] = i
        df_201y["syear"] = i
        xsuffix = f"_{i}x"
        ysuffix = f"_{i}y"
        df = df.merge(
            df_201y, on=["hid", "syear"], how="left", suffixes=[None, ysuffix]
        )
        if i in [2016, 2017, 2018]:
            df = df.merge(
                df_201x, on=["hid", "syear"], how="left", suffixes=[None, xsuffix]
            )

            # replace with preference 2015, thats why 2019 first then 2015
            loc_to = (
                (df["syear"] == i)
                & (df["hgcondit"] != 1)
                & (df["hgcondit"] != 2)
                & (df["hgcondit"] != 3)
                & (df["hgcondit"] != 4)
            )
            df.loc[loc_to, "hgcondit"] = df.loc[loc_to, f"hgcondit{ysuffix}"]

            loc_to = (
                (df["syear"] == i)
                & (df["hgcondit"] != 1)
                & (df["hgcondit"] != 2)
                & (df["hgcondit"] != 3)
                & (df["hgcondit"] != 4)
            )
            df.loc[loc_to, "hgcondit"] = df.loc[loc_to, f"hgcondit{xsuffix}"]

            # remove unwanted columns
            df = df.drop([f"hgcondit{xsuffix}", f"hgcondit{ysuffix}"], axis=1)
        else:
            loc_to = (
                (df["syear"] == i)
                & (df["hgcondit"] != 1)
                & (df["hgcondit"] != 2)
                & (df["hgcondit"] != 3)
                & (df["hgcondit"] != 4)
            )
            df.loc[df["syear"] == i, "hgcondit"] = df.loc[loc_to, f"hgcondit{ysuffix}"]
            # remove unwanted columns
            df = df.drop([f"hgcondit{ysuffix}"], axis=1)

        # print(i, len(df.loc[df["syear"] == i, "hgcondit"]))

    col_bt = "building_type"
    col_regio = "region_type"

    # attach ownership variable
    if ip["dwelling_ownership_short"]:
        df = attachVariable(
            soep_path=soep_path,
            add_vars=["hlf0013_h"],
            merge_from="hl",
            merge_to=df,
            match_keys=["hid", "syear"],
            new_names=["hlf0013_h"],
        )

    df = attachVariable(
        soep_path=soep_path,
        add_vars=["regtyp"],
        merge_from="regionl",
        merge_to=df,
        match_keys=["hid", "syear"],
        new_names=[col_regio],
    )

    wum_path = os.path.join(composita_path, f"{WUM1_NEW_MOVED_2021[3]}.csv")
    if not os.path.exists(wum_path):
        update_wum1(df, soep_path, wum_path)

    df = attachVariable(
        soep_path=composita_path,
        add_vars=["wum1_updated"],
        merge_from="wum1_updated",
        merge_to=df,
        match_keys=["hid", "syear"],
        new_names=[col_bt],
    )

    return df


@misc.timer_func
def calc_will_move(ip, df):
    """calculates the will_move.csv for faster loading during calculations.
    Args:
    - ip: dataframe, contains user-input from csv and in-program added vars.
    - df: Dataframe, loaded soep csv.
    """
    df = attachVariable(
        soep_path=ip["soep"],
        add_vars=["resmove"],
        merge_from="raw/movedist",
        merge_to=df,
        match_keys=["pid", "syear"],  # match to pid, not hid
        new_names=[ip["move"]["col_moved"]],
    )

    # filter all pid and syear that have a moved=1 entry
    moved = df.loc[df[ip["move"]["col_moved"]] == 1, ["pid", "syear"]]
    # save the year before to have the pid + syear-1 combination
    moved["syear"] = moved["syear"] - 1
    # all entries of "moved" will move in the current year. Mark this with 1
    moved[ip["move"]["col_will_move"]] = 1

    save_path = os.path.join(ip["soep_composita_path"], ip["move"]["csv_move"])
    moved.to_csv(save_path, index=False)


def movers(df, ip):
    """Add variable for residential move to hids."""
    df = attachVariable(
        soep_path=ip["soep_composita_path"],
        add_vars=[ip["move"]["col_will_move"]],
        merge_from=ip["move"]["col_will_move"],
        merge_to=df,
        match_keys=["pid", "syear"],  # match to pid, not hid
    )
    df[ip["move"]["col_will_move"]] = df[ip["move"]["col_will_move"]].fillna(0)
    return df
